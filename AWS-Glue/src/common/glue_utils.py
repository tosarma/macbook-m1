LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os
import sys
import boto3

from common.constants import GLUE_CATALOG_SETTINGS

from common.names import (
    get_prod_vs_nonprod,
    get_glue_common_databasename,
    get_bucketname_glue_raw_input,
    get_bucketname_glue_processed_input,
    get_bucketname_glue_final_input,
)


### ======================================================================================
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ======================================================================================

# this returns the folder
def get_glue_catalog_item( ENV :str, branch :str, glue_catalog_entry_name: str, gluecatalog_item_dbname: str = None) -> dict:
    """Look up entry in AWS-Glue Catalog and return that Glue-Catalog-ENTRY as a JSON.
    """
    if ( gluecatalog_item_dbname == None ):
        gluecatalog_item_dbname = get_glue_common_databasename( ENV, branch )

    if ( os.environ.get('is_GFE_Laptop') ):
        print( f"!! WARNING !! Running on GFE/MacOS/MacBook, so ..  MOCKING the LOOKUP into AWS-Glue-Catalog for item '{glue_catalog_entry_name}' in database '{gluecatalog_item_dbname}' !!" )
        ### TODO: TBD - figure out HOW to Remove this BLOCK of IF; Retain only ELSE code.  GFEs do _NOT_ have access to AWS-Credentials!!
        return {
            "Name": GLUE_CATALOG_SETTINGS[ "glue_catalog_entry_name" ],
            "Database": "nccrcommonstacknonprod-rawdatalake-datalakebucket-ff0b21fa",
            "Location": "s3://nccrcommonstacknonprod-rawdatalake-datalakebucket-ff0b21fa/synth_data_12_08_2023",
            "Classification": "CSV",
        }
    else:
        try:
            glue_client: boto3.client = boto3.client('glue')
            response = glue_client.get_table(  ### https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glue/client/get_table.html
                # OPTIONAL -> CatalogId='aws_account_id', ### ALl branches, DEV and TEST use the same Catalog in "NonProd" AWS-Account!
                DatabaseName=gluecatalog_item_dbname,
                Name=glue_catalog_entry_name,
            )
            # print( f"DEBUG: get_glue_catalog_item(): response inside file {__file__} = {response}" )
            return response['Table']

        except Exception as e:
            print(f"ERROR!! Item '{glue_catalog_entry_name}' not found in Glue catalog for database named {gluecatalog_item_dbname} !!")
            print(f"Error details: {e}")
            raise

### ----------------------------------------------------------------------------

def get_glue_catalog_location( ENV :str, branch :str, glue_catalog_entry_name: str, gluecatalog_item_dbname: str ) -> str:
    """Look up entry in AWS-Glue Catalog and return its "Location" field.
    """
    j = get_glue_catalog_item( ENV, glue_catalog_entry_name, gluecatalog_item_dbname );
    print( f"DEBUG: get_glue_catalog_location(): j inside file {__file__} = {j}" )
    return j['Location']


### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

### S3 bucket & DataLake

def download_file( ENV :str, branch :str, glue_catalog_entry_name: str, gluecatalog_item_dbname: str, local_filename: str ):
    """ Download file from S3 LOCATION (as specified within Glue-Catalog) to local directory
    """
    fulls3path = get_glue_catalog_location( ENV=ENV, glue_catalog_entry_name=glue_catalog_entry_name, gluecatalog_item_dbname=gluecatalog_item_dbname ); ### 'nccrcommonstacknonprod-rawdatalake-datalakebucket-ff0b21fa',
    print( f"DEBUG: fulls3path = '{fulls3path}'" )
    fulls3path = fulls3path.replace('s3://', '')
    print( f"DEBUG: fulls3path (w/o protocol) = '{fulls3path}'" )
    s3_bucket_name   = fulls3path.split('/')[0];
    s3_bucket_prefix = '/'.join( fulls3path.split('/')[1:] ) +'/'+ local_filename;
    print( f"DEBUG: s3_bucket_name   = '{s3_bucket_name}'" )
    print( f"DEBUG: s3_bucket_prefix = '{s3_bucket_prefix}'" )

    if ( os.environ.get('is_GFE_Laptop') ):
        print( f"!! WARNING !! Running on GFE/MacOS/MacBook, so ..  _SKIPPING_ the DOWNLOAD of file '{local_filename}' !!" )
    else:
        # TODO test the boto3-code below inside Data-pipeline
        s3_client :boto3.client = boto3.client('s3')
        s3_client.meta.client.download_file(                            ### https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3/client/download_file.html
            Bucket = s3_bucket_name,
            Key    = s3_bucket_prefix,
            Filename = local_filename,
        )

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

### The following functions deal with AWS-GLUE job's "default-arguments"
### READ: https://docs.aws.amazon.com/glue/latest/dg/aws-glue-api-crawler-pyspark-extensions-get-resolved-options.html

sample_retval_getResolvedOptions = """
{
    "CLI-argument-NAME": "<<CLI-argument-VALUE>>",
    "job_bookmark_option": "job-bookmark-disable",
    "job_bookmark_from": None,
    "job_bookmark_to": None,
    "JOB_ID": None,
    "JOB_RUN_ID": None,
    "SECURITY_CONFIGURATION": None,
    "encryption_type": None,
    "enable_data_lineage": None,
    "RedshiftTempDir": None,
    "TempDir": None
}
"""

### -----------------------------------

def get_glue_cli_arg( sys, cli_arg_name :str ) -> str:

    # print( "!!!!!!!!!!!!!!!!! running inside AWS-Glue !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" )
    import awsglue  ### NOTE: No need to pip-install!! It's __AUTOMATIC__ if you run this __INSIDE__ AWS-GLUE.
    from awsglue.utils import getResolvedOptions

    if ('--{}'.format( cli_arg_name ) in sys.argv):
        argval :object = awsglue.utils.getResolvedOptions(sys.argv, [ cli_arg_name])
            ### Note: The response is JSON. See example below.
        return argval[ cli_arg_name ];

### -----------------------------------

def parse_all_glue_cli_args ( sys, cli_arg_names :list) -> dict:
    glue_args = awsglue.utils.getResolvedOptions( sys.argv, cli_arg_names )
    return glue_args
    ### glue_args is a dict!
    ### glue_args["key"]
    ### NOTE: the args should be as:  key1 <space> value1 <space> key2 <space> value2 <space> ...
    ### _NOT_ k=v format _NOT_ !!!

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

if __name__ == "__main__":
    print( "!! ERROR !! This file should Not be invoked directly via command line: "+ __file__ )
    sys.exit(11)

### EoF
