LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os
import sys
import pathlib
import argparse ### https://docs.python.org/3/library/argparse.html

from common.glue_utils import get_glue_cli_arg

### ----------------------------------------------------------------------------
### WARNING: Globals in Python are global to a module, not across all modules !!!
plain_python_cli_parser = argparse.ArgumentParser(
            prog=pathlib.Path(__file__).stem,
            description='Plain Python script',
            epilog='FYI: This python-script should be tested inside AWS-Glue also')

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

def get_cli_arg( sys, cli_arg_name :str ) -> str:
    """ get value of CLI-arg -- by dynamically determining ..
        .. whether we're running INSIDE AWS-GLUE, ..
        .. or whether this is a PLAIN-Python program.
    """
    if ( os.environ.get('runningInsideAWSGlue') ):
        return get_glue_cli_arg( sys, cli_arg_name )
    else:
        # parsed_args = globals()['parsed_args']
        # print ( parsed_args )
        return get_argparse_cli_arg( cli_arg_name, get_argparse_parsed_args() )

### -----------------------------------

def get_argparse_cli_arg( cli_arg_name :str, parsed_args ) -> str:
    """ get the value of a CLI-arg using PLAIN-Python's `argparse`.
    """
    if hasattr( parsed_args, cli_arg_name ):
        return getattr( parsed_args, cli_arg_name )
    else:
        return None

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

def get_argparse_parsed_args() -> argparse.Namespace:
    global parsed_args  ### Explicitly declare this as a global variable.
    return parsed_args

def process_all_argparse_cli_args( args ) -> dict:
    """ Process the CLI-args using PLAIN-Python's `argparse`.
        REF: https://docs.python.org/3/library/argparse.html
    """
    # print( "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ___argparse__ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" )
    global plain_python_cli_parser ### Explicitly declare this as a global variable.
    # plain_python_cli_parser = globals()["plain_python_cli_parser"]
    plain_python_cli_parser.add_argument( '--ENV', required=True )
    plain_python_cli_parser.add_argument( '--branch', required=False, default='NoSuchGitBranch' )
    plain_python_cli_parser.add_argument( '--runningInsideAWSGlue', required=False, default=False )
    plain_python_cli_parser.add_argument( '--enableMetrics', required=False, default=False )
    plain_python_cli_parser.add_argument( '--tempDir', required=False, default="invalid-tempDir" )

    plain_python_cli_parser.add_argument( '--glueCatalogEntryName', required=True )
    plain_python_cli_parser.add_argument( '--commonDatabaseName', required=True )
    plain_python_cli_parser.add_argument( '--rawBucket', required=True )
    plain_python_cli_parser.add_argument( '--processedBucket', required=True )
    plain_python_cli_parser.add_argument( '--finalBucket', required=True )
    global parsed_args  ### Explicitly declare this as a global variable.
    parsed_args = plain_python_cli_parser.parse_args( args )
    ### https://docs.python.org/3/library/argparse.html#partial-parsing
    ### Does NOT produce an error when EXTRA/UNKNOWN args are passed via cli.
    # parsed_args, leftovers = plain_python_cli_parser.parse_known_args( args )

    # ================================================
    ### Do NOT touch / modify these 10 lines !!!!!!!!!!!!!!!
    ### Do NOT touch / modify these 10 lines !!!!!!!!!!!!!!!
    if get_argparse_cli_arg( 'runningInsideAWSGlue', parsed_args ):
        # print( '?'*80 )
        # print( "DEBUG: Running inside AWS-Glue, per Default-Arguments passed to spark-submit -- within process_all_argparse_cli_args(): "+ __file__ )
        os.environ.setdefault( 'runningInsideAWSGlue', 'true' )
        # print( '?'*80 )
    if get_argparse_cli_arg( 'enableMetrics', parsed_args ):
        os.environ.setdefault( 'enableMetrics', 'true' )

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

def process_std_glue_cli_args(args) -> dict:
    """ Process the standard Glue CLI-args, and set the corresponding ENV-vars.
        REF: https://docs.aws.amazon.com/glue/latest/dg/aws-glue-programming-etl-glue-arguments.html
    """
    print( '-'*80 )
    # print( "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! GLUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" )

    if ( os.environ.get('runningInsideAWSGlue') ):
        print( "!!!!!!!!!!!!!!!!! running inside AWS-Glue !!!!!!!!!!!!!!!!!!!!!!!" )
        import awsglue  ### NOTE: No need to pip-install!! It's __AUTOMATIC__ if you run this __INSIDE__ AWS-GLUE.
        from awsglue.utils import getResolvedOptions

    # global runningInsideAWSGlue
    global ENV
    global branch
    global tempDir

    global data_dictionary_path
    global commonDatabaseName

    global input_dir_path
    global rawBucket
    global processedBucket
    global finalBucket

    # runningInsideAWSGlue = False
    ENV = '<uninitialized-variable>'
    branch = '<uninitialized-variable>'
    tempDir = '<uninitialized-variable>'

    # data_dictionary_path = '<uninitialized-variable>'
    # input_dir_path = '<uninitialized-variable>'

    ### ---------------------------------------

    ### SAMPLE Code!!!
    ### SAMPLE Code!!!

    glue_args = getResolvedOptions(sys.argv, [
        'ENV',
        'branch',

        'glueCatalogEntryName',
        'commonDatabaseName',
        # 'input',
        'rawBucket',
        'processedBucket',
        'finalBucket',

        'runningInsideAWSGlue',     ### Only required for use when running __INSIDE__ AWS-Glue !!!
        'tempDir',                  ### Only required for use when running __INSIDE__ AWS-Glue !!!
        'enableMetrics',            ### Only required for use when running __INSIDE__ AWS-Glue !!!
    ])

    # ================================================
    ### Do NOT touch / modify these 10 lines !!!!!!!!!!!!!!!
    ### Do NOT touch / modify these 10 lines !!!!!!!!!!!!!!!
    if glue_args['runningInsideAWSGlue']:
        # print( '?'*80 )
        # print( "DEBUG: Running inside AWS-Glue, per Default-Arguments passed to spark-submit -- within process_std_glue_cli_args(): "+ __file__ )
        os.environ.setdefault( 'runningInsideAWSGlue', 'true' )
        # print( '?'*80 )
    if glue_args['enableMetrics']:
        os.environ.setdefault( 'enableMetrics', 'true' )

    # ================================================
    ### Debug print
    print( f"ENV='{glue_args['ENV']}'" )
    print( f"branch='{glue_args['branch']}'" )

    print( f"data Dictionary'{glue_args['glueCatalogEntryName']}'" )
    print( f"commonDatabaseName='{glue_args['commonDatabaseName']}'" )
    print( f"rawBucket='{glue_args['rawBucket']}'" )
    print( f"processedBucket='{glue_args['processedBucket']}'" )
    print( f"finalBucket='{glue_args['finalBucket']}'" )

    print( "runningInsideAWSGlue="+ os.environ["runningInsideAWSGlue"] )
    print( f"tempDir='{glue_args['tempDir']}'" )
    print( f"enableMetrics='{glue_args['enableMetrics']}'" )

    return glue_args

### EoF
