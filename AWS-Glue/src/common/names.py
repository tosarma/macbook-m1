LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

### This file contains functions that provide a layer of IN-DIRECTION to the various CONSTANTS in `./constants.py`
###     REF: https://en.wikipedia.org/wiki/Indirection

import boto3
from common.constants import GLUE_CATALOG_SETTINGS

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### Generic

def get_prod_vs_nonprod( ENV : str ) -> str:
    """ Return either 'prod' or 'nonprod' depending on the ENV parameter."""
    if ENV.lower() == "prod":
        return "prod"
    else:
        return "nonprod"

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### S3/Datalake related

def get_bucketname_glue_raw_input( ENV : str, branch :str ) -> str:
    """ Unlike Processed and Final, Raw bucket is shared across _ALL_ Git-branches."""
    return f"my-raw-datalake-bucket-{ENV}"

def get_bucketname_glue_processed_input( ENV :str, branch : str ) -> str:
    """ Processed datalake-bucket is NOT only ENV-specific, but also Git-branch specific."""
    return f"my-processed-datalake-bucket-{ENV}-{branch}"
def get_bucketname_glue_final_input( ENV :str, branch : str ) -> str:
    """ FINAL datalake-bucket is NOT only ENV-specific, but also Git-branch specific."""
    return f"my-final-datalake-bucket-{ENV}-{branch}"

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### Glue related

### ----------------------
def get_glue_catalog_entry_name( ENV :str, branch :str ) -> str:
    """ Value of the "Name" column of Glue-Catalog entries."""
    return GLUE_CATALOG_SETTINGS[ "glue_catalog_entry_name" ];

### ----------------------
def get_glue_common_databasename(  ENV : str, branch :str ) -> str:
    """ Value of the "DatabaseName" column of Glue-Catalog entries."""
    return f"glue_common_{ENV}"


###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### EoF
