### Warning: These 10 lines below must be the 1st lines of code in this file !!!!!
# from common.glue_utils import (  )
from common.cli_utils import ( get_cli_arg, process_std_glue_cli_args, process_all_argparse_cli_args )
import sys, os
process_all_argparse_cli_args( args = sys.argv[1:] )
if ( os.environ.get('runningInsideAWSGlue') ):
    global glue_args
    glue_args :dict = process_std_glue_cli_args( args = sys.argv )
### WARNING: Must process the cli-args BEFORE any other "import", so os.environ() can be set properly.
print( '-'*80 ); print( '-'*80 )

LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

import os

### ----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ----------------------------------------------------------------------------

def main():

    ### Recommended way of getting CLI-args -- so that it will work both inside AWS-Glue and outside as a PLAIN-python-code.
    ENV = get_cli_arg( sys, 'ENV' )
    print( f"GLUE-SDK's parse_glue_cli_arg( sys, 'ENV' ) returned .. '{ENV}'" )

    ### If you'd like to EXPLICTLY assume you are running in glue, then the following IF-block is recommended.
    if ( os.environ.get('runningInsideAWSGlue') ):
        print( "!!!!!!!!!!!!!!!!! running inside AWS-Glue !!!!!!!!!!!!!!!!!!!!!!!" )
        global glue_args
        ENV= glue_args['ENV']
        print( f"GLUE-SDK's glue_args['ENV'] returned .. '{ENV}'" )


### ----------------------------------------------------------------------------

if __name__ == "__main__":
    main()
    print( '-' * 80 )


### EoF
