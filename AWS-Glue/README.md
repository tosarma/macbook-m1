# About

On a MacBook-M1 laptop, this README has step-by-step details on -> [HOW-TO](#locally-on-macbook-m1----simple-how-to) run your Python-code inside AWS-Glue (locally on that MacBook-M1).

In addition, this README has step-by-step details on -> [HOW-TO](#run-as-plain-python-program) _ ALSO _ run your Python-code as a SIMPLE python-program (via `python3` cli-command).

# REFERENCES.

REFERENCE: AWS Official Docs: https://docs.aws.amazon.com/glue/latest/dg/aws-glue-programming-etl-libraries.html

REFERENCE: Blog = https://aws.amazon.com/blogs/big-data/develop-and-test-aws-glue-version-3-0-jobs-locally-using-a-docker-container/

SAMPLES: https://github.com/aws-samples/aws-glue-samples

<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# Pre-Requisites

1.  You are an expert in using MacOS CLI.  I will not support you re: this.
1.  You are an expert in using BASH-Shell.  I will not support you re: this.
1.  You are an expert in using Python3.  I will not support you re: this.<BR/>Python 2.x is NOT supported.
1.  Only MacBook-M1 is supported.<BR/>Do __ NOT __ try this anywhere else, and ask me for support.
1.  Your Python-code file should be in `./src` folder only.
    *  Do _NOT_ put it inside a sub-folder of it, or anywhere else.   I will not support you re: this.
1.  Run all the commands (shown here) from the ROOT-Folder of this Git-project.

# Locally on MacBook-M1 -- Simple HOW-TO:

Run any Python script as a Glue-Job (inside Docker) on your MacBook-M1/Laptop as:

## Template CLI command
```bash
cd ./src
../bin/run-glue-job-LOCALLY.sh  --cleanup   <Python-Script-NAME>.py   arg1 arg2 .. ..
```

## Actual EXAMPLE

Yes, you can copy-n-paste the following and try it out!

```bash
cd ./src
../bin/run-glue-job-LOCALLY.sh  --cleanup sample-glue-job.py
```

ADVANCED users ONLY: See also [Appendix tip](#appendix---running-a-file-inside-common-subfolder)

<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# Run as PLAIN Python-program

EXAMPLE:

NOTE: My python-code in `sample-glue-job.py` expects the following 6 CLI-argument (with their values).

If you do _ NOT _ like this list of CLI-args, see next section titled [Change the CLI-args](#change-the-cli-arguments).

```bash
python3 sample-glue-job.py --ENV sandbox --commonDatabaseName MyCOMMONDATABASENAME --glueCatalogEntryName MyDICT --rawBucket MyRAWBUCKET --processedBucket MyPROCESSEDBUCKET  --finalBucket MyFINALBUCKET
```

<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# Change the CLI-arguments

If you want to change what the CLI-args are:
1.  Edit the files [./src/common/cli_utils.py](./src/common/cli_utils.py)
1.  look inside `process_all_argparse_cli_args()` and make changes in there.
1.  look inside `process_std_glue_cli_args()` and make changes in there.

Example:

*   You would like to passing a new cli-arg `--JOB_NAME  123_ABC`
*   Insert a new line at (say) line # 78 for `--JOB_NAME`<BR/>This will allow you to run your python-code as a PLAIN python-command.
*   Insert a new line at (say) line # 125 for `JOB_NAME`<BR/>This will ensure your code will get the value `123_ABC` when running __ INSIDE __ AWS-Glue.

<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# Code that detects its running on MacBook-M1 and NOT in AWS

__Question__: Why would you need this ability?
__Answer__: When running on MacBook-M1, if you do _ NOT _ have AWS-credentials, then you must "short-circuit" code that invokes AWS-APIs for S3-buckets or for AWS-Glue-Catalog.

## How-To "short-circuit"

```python

if ( os.environ.get('running_on_LAPTOP') ):
    print( "!!!!!!!!!!!!!!!!! running on laptop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" )
    .. ### assume S3-get is already done and file is available in current-directory
    .. ### assume Glue-Catalog-Query is already done and the "JSON-Response" is available in current-directory as a JSON-file
    ..
else:
    ..
    ..
```


<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# LICENSE

Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# APPENDIX -- Python-CODE - dependencies

`import awsglue`

ATTENTION:<BR/>
*   NOTE: No need to pip-install `awsglue` !!
*   It's __AUTOMATIC__ if you run this __INSIDE__ AWS-GLUE.

## Detecting if code running on AWS -versus- on LAPTOP/MacBook

*   Since MacBook is running Linux-docker container (for the Glue-Engine) ..<BR/>
        you can _NOT_ detect MacBook-M1.
*   Instead, use ENV-VAR named "`running_on_LAPTOP`", which is set by my tool [run-glue-job-LOCALLY.sh](./bin/run-glue-job-LOCALLY.sh)
    * Use as:  `if ( os.environ['running_on_LAPTOP'] ) then:`

<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# Glue-Spark CLI configuration

*       `--conf <key>=<value>` CLI-arg to `spark-submit`.

<HR/>
<HR/>
<HR/>
<HR/>
<HR/>

# UNDER THE HOOD !

## 1-time setup

```bash
AWSPROFILE=".."

DOCKER_IMAGE_NAME="aws-glue-libs:glue_libs_4.0.0_image_01"

docker pull "public.ecr.aws/glue/${DOCKER_IMAGE_NAME}"
```

<HR/>
<HR/>

## Run LOCALLY - Simple stand-ALONE python-script

**!! ATTTENTION !!**: The python-script should be a SIMPLE __ STANDALONE __ script.<BR/>
See SUB-section, if you have a LARGE python code-base

SUMMARY: run an AWS Glue job script by running the `spark-submit` command into the container.

```bash
WORKSPACE_LOCATION="$(pwd)"
SCRIPT_FILE_NAME=".. ?? ?? .."

PORT1="4040"
PORT2="18080"

PY_SCRIPT_CLI_ARGS=( --arg1 value1 --arg222 value222 )

docker run -it      \
        -v ~/.aws:/home/glue_user/.aws -v $WORKSPACE_LOCATION:/home/glue_user/workspace/  \
        -e AWS_PROFILE=$AWSPROFILE -e DISABLE_SSL=true ${DOCKERFLAGS} -p "${PORT1}:${PORT1}" -p "${PORT2}:${PORT2}"   \
        --name   "${DOCKER_CONT_NAME}"     "amazon/${DOCKER_IMAGE_NAME}"    \
        spark-submit --py-files "${DOCKER_ZIP_FILE_PATH}/${ZIP_FILE_NAME}"  /home/glue_user/workspace/$GLUESCRIPT_FILENAME      \
        ${PY_SCRIPT_CLI_ARGS[@]}

```

<HR/>


### Running _LARGE_ Python codebase (folder-heirarchy) inside Glue Spark

REF: https://spark.apache.org/docs/1.0.2/submitting-applications.html#bundling-your-applications-dependencies

NOTE:

*   the `spark-submit` command is a Java-binary.
*   It does _ NOT _ support `PYTHON_PATH`.
*   ONLY one Solution-OPTION = <BR/>
Zip up entire codebase as a zip-file/egg file, and use `--py-files` CLI-arg to `spark-submit`
*   Bottomline: See [run-glue-job-LOCALLY.sh](./bin/run-glue-job-LOCALLY.sh)

<HR/>

| PYTHON SPECIFIC CONFIGURATIONS	|   DESCRIPTION |
| --- | --- |
| –py-files	|   Use --py-files to add .py, .zip or .egg files. |
| –config spark.executor.pyspark.memory	|   The amount of memory to be used by PySpark for each executor. |
| –config spark.pyspark.driver.python	|   Python binary executable to use for PySpark in driver. |
| –config spark.pyspark.python	|   Python binary executable to use for PySpark in both driver and executors. |

<HR/>
<HR/>
<HR/>
<HR/>

# APPENDIX

## pyTEST (Testing)

```bash
docker run -it -v ~/.aws:/home/glue_user/.aws -v $WORKSPACE_LOCATION:/home/glue_user/workspace/ -e AWS_PROFILE=$AWSPROFILE -e DISABLE_SSL=true --rm -p "${PORT1}:${PORT1}" -p "${PORT2}:${PORT2}" \
        --name  glue_pytest       "amazon/${DOCKER_IMAGE_NAME}" \
        -c "python3 -m pytest"
```

<HR/>
<HR/>

## Jupyter-LAB

```bash
docker run -it -v ~/.aws:/home/glue_user/.aws -v $JUPYTER_WORKSPACE_LOCATION:/home/glue_user/workspace/jupyter_workspace/ -e AWS_PROFILE=$AWSPROFILE -e DISABLE_SSL=true --rm -p "${PORT1}:${PORT1}" -p "${PORT2}:${PORT2}"   \
        --name  glue_jupyter_lab   "amazon/${DOCKER_IMAGE_NAME}" \
        /home/glue_user/jupyter/jupyter_start.sh
```

Open http://127.0.0.1:8888/lab ..<BR/>
in your web browser in your __ LOCAL __ machine ..<BR/>
to access the JupyterLab UI.

<HR/>
<HR/>

## REPL-shell

```bash
docker run -it -v ~/.aws:/home/glue_user/.aws -e AWS_PROFILE=$AWSPROFILE -e DISABLE_SSL=true --rm -p "${PORT1}:${PORT1}" -p "${PORT2}:${PORT2}" \
        --name   glue_pyspark      "amazon/${DOCKER_IMAGE_NAME}"   \
         pyspark
```

<HR/>
<HR/>
<HR/>

# Appendix - running a file inside `common/` subfolder

Example: If you'd like to run the file `common/glue_utils.py`, run it as:

```bash
cd ./src
( export PYTHONPATH="$(pwd)"; cd python3 common/glue_utils.py )
```

<HR/>
<HR/>
<HR/>

# EoF
