LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import os
import sys
import stat
import time
import pathlib
import argparse     ### https://docs.python.org/3/library/argparse.html
import secrets

import docker       ### https://docker-py.readthedocs.io/en/stable/

ALL_FILE_MODES = {
    33188: '-rw-r--r--',
    33252: '-rwxr--r--',
    33261: '-rwxr-xr-x',
    33279: '-rwxrwxrwx',
}

def linux_cmd_ls( file ):
    file_path = pathlib.Path( file )
    file_info = file_path.stat()
    rwx = ALL_FILE_MODES.get( file_info.st_mode, f'unknown-file-mode: {file_info.st_mode}' )
    return f'{file_path.name}: Mode: {rwx}, Size: {file_info.st_size} bytes, Last Modified: {time.ctime(file_info.st_mtime)}'

### -----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------------------------------------------

class MyScript:

    @property
    def argv(self):
        return self.__argv

    @property
    def parsed_args(self):
        return self.__parsed_args

    def __init__(self, _argv):
        self.__argv = _argv

        self.main()

    ### -------------------------------------------------------------------------------------------
    def basicChecks(self):

        if os.environ.get('BUILDPLATFORM') is None:
            print( "!! Attention !! BUILDPLATFORM is NOT defined. setting it to defaults." )
            os.environ['BUILDPLATFORM'] = 'linux/aarch64'
            time.sleep(5)

        if os.environ.get('DOCKER_DEFAULT_PLATFORM') is None:
            os.environ['DOCKER_DEFAULT_PLATFORM'] = os.environ['BUILDPLATFORM']

        if os.environ.get('TARGETPLATFORM') is None:
            os.environ['TARGETPLATFORM'] = os.environ['BUILDPLATFORM']

        for key, value in os.environ.items():
            if 'PLATFORM' in key:
                print(f'{key}: {value}')

        if self.parsed_args.glue_script is None:
            print( "ERROR: Missing file-path to Glue-Script as the last CLI-arg." )
            sys.exit(11)

    ### -------------------------------------------------------------------------------------------
    def staticVariables(self):
        self.ENV = self.parsed_args.env or "sandbox"
        self.AWSPROFILE="default"

        ### https://docs.aws.amazon.com/glue/latest/dg/aws-glue-programming-etl-libraries.html#develop-local-docker-image
        self.DOCKER_IMAGE_NAME="aws-glue-libs:glue_libs_4.0.0_image_01"
        # self.DOCKER_IMAGE_URL="amazon/{DOCKER_IMAGE_NAME}"
        self.DOCKER_IMAGE_URL=f"public.ecr.aws/glue/{self.DOCKER_IMAGE_NAME}"

        self.PORT1="4040"
        self.PORT2="18080"


    ### -------------------------------------------------------------------------------------------
    def deriveVariables(self):

        self.SCRIPTFOLDER = pathlib.Path(__file__).parent.absolute()
        self.SCRIPTNAME = pathlib.Path(__file__).name
        self.CWD = pathlib.Path.cwd()

        self.TMPDIR=f"/tmp/{self.SCRIPTNAME}"
        pathlib.Path.mkdir( pathlib.Path(self.TMPDIR), parents=True, exist_ok=True )
        self.CACHE_FILE=f"{self.TMPDIR}/cache-{self.ENV}.py"

        if self.parsed_args.cleanup:
            self.DOCKERFLAGS="--rm"
        else:
            self.DOCKERFLAGS=""

        p = pathlib.Path(self.parsed_args.glue_script)
        if not p.exists():
            print( f"\n\n!! ERROR !! Script file not found: '{self.parsed_args.glue_script}' in folder '{self.CWD}'" )
            sys.exit(81)
        self.WORKSPACE_LOCATION=p.resolve().absolute().parent            ### PARENT folder of glue-script.
        print( f"WORKSPACE ROOT-Folder = '{self.WORKSPACE_LOCATION}'" )

        self.GLUESCRIPT_FILENAME = p.resolve().absolute().name
        print( f"GLUESCRIPT_FILENAME = '{self.GLUESCRIPT_FILENAME}'" )

        self.DOCKER_CONT_NAME = p.stem +"-"+ secrets.token_hex(16)
        print( f"DOCKER_CONTAINER's NAME: '{self.DOCKER_CONT_NAME}'" )

        self.PY_FILES_LIST=pathlib.Path.cwd().glob( '*.py' )
        print( self.PY_FILES_LIST )

        self.ZIP_FILE_PATH=self.TMPDIR                  ### !! NOTE !! This location is __OUTSIDE__ the Docker-filesystem, that is .. in this MacBook-M1 Laptop's filesystem.
        self.DOCKER_ZIP_FILE_PATH="/tmp/macbook-tmp"    ### !! WARNING !! This is __INSIDE__ the Docker-Image's filesystem.
        self.ZIP_FILE_NAME="py_files_list.zip"

        self.DOCKER_LOGGING_FLDR="/home/glue_user/spark/logs"      ### !! WARNING !! This is __INSIDE__ the Docker-Image's filesystem.
        self.LOCALDISK_LOGGING_FLDR=f"{self.TMPDIR}/python_logs_for_{self.SCRIPTNAME}"  ### NOTE: This is on the MacBook-M1 Laptop's filesystem.

        branch = "NoSuchGitBranch"

        ###---------------------------------------------------------------

        print( pathlib.Path.cwd() )


        path2cachefile = pathlib.Path(self.CACHE_FILE)
        if path2cachefile.exists():
            print( f"Loading from cache file: '{self.CACHE_FILE}' .. .." )
            import importlib    ### https://docs.python.org/3/library/importlib.html#importing-a-source-file-directly
                                ### https://docs.python.org/3/library/importlib.html#module-importlib.util
            spec = importlib.util.spec_from_file_location( path2cachefile.stem, str(path2cachefile) ) ### https://docs.python.org/3/library/importlib.html#importlib.util.spec_from_file_location
            modref = importlib.util.module_from_spec(spec)      ### https://docs.python.org/3/library/importlib.html#importlib.util.module_from_spec
            sys.modules[ path2cachefile.stem ] = modref
            spec.loader.exec_module(modref)
            for name, val in modref.__dict__.items():
                if callable(val):
                    assert False, f"Unexpected function inside '{self.CACHE_FILE}' -> name={name} val={val}"
                else:
                    print( f"loading from {self.CACHE_FILE}: {name} = {val}" )
                    self.__dict__[name] = val
            print( f"\t.. confirming: glueCatalogEntryName = '{self.glueCatalogEntryName}'" )
            print( f"\t.. confirming: common_DatabaseName = '{self.common_DatabaseName}'" )
            print( f"\t.. confirming: bucketname_glue_raw_input = '{self.bucketname_glue_raw_input}'" )
            print( f"\t.. confirming: bucketname_glue_processed_input = '{self.bucketname_glue_processed_input}'" )
            print( f"\t.. confirming: bucketname_glue_final = '{self.bucketname_glue_final}'" )

        else:

            modulefilepath = self.CWD / "./common/names.py"
            sys.path.append( str(self.CWD) )
            sys.path.append( str(self.WORKSPACE_LOCATION) )
            print( f"DEBUG: sys.path={sys.path}" )

            import importlib
            print( f"DEBUG: modulefilepath = '{modulefilepath}'" )
            if not modulefilepath.exists():
                print( f"\n\n!! FATAL-ERROR!! File missing -> '{modulefilepath}'" )
                print( f"\tTypically, this happens if ONE of the following two issues occurs:" )
                print( f"\t\t(1) your script is NOT in current-working-dir={self.CWD} .." )
                print( f"\t\t(2) you FORGOT to copy the 'src' subfolder in here (from the git-repo)!\n")
                sys.exit(84)
            spec = importlib.util.spec_from_file_location( modulefilepath.stem, str( modulefilepath ) )
            modref = importlib.util.module_from_spec(spec)
            sys.modules[ path2cachefile.stem ] = modref
            spec.loader.exec_module(modref)

            # from common.names import (
            #     get_glue_catalog_entry_name,
            #     get_glue_common_databasename,
            #     get_bucketname_glue_raw_input,
            #     get_bucketname_glue_processed_input,
            #     get_bucketname_glue_final_input,
            # )
            self.glueCatalogEntryName = getattr( modref, "get_glue_catalog_entry_name" )(self.ENV,branch)
            self.common_DatabaseName  = getattr( modref, "get_glue_common_databasename" )(self.ENV,branch)
            self.bucketname_glue_raw_input = getattr( modref, "get_bucketname_glue_raw_input" )(self.ENV,branch)
            self.bucketname_glue_processed_input = getattr( modref, "get_bucketname_glue_processed_input" )(self.ENV,branch)
            self.bucketname_glue_final = getattr( modref, "get_bucketname_glue_final_input" )(self.ENV,branch)

            print( f"creating a CACHE-file {self.CACHE_FILE} .. to speed up this command the next time." )
            with open( path2cachefile, 'w' ) as file:
                file.write( f"glueCatalogEntryName='{self.glueCatalogEntryName}'\n" )
                file.write( f"common_DatabaseName='{self.common_DatabaseName}'\n" )
                file.write( f"bucketname_glue_raw_input='{self.bucketname_glue_raw_input}'\n" )
                file.write( f"bucketname_glue_processed_input='{self.bucketname_glue_processed_input}'\n" )
                file.write( f"bucketname_glue_final='{self.bucketname_glue_final}'\n" )

            print( linux_cmd_ls(self.CACHE_FILE) )

        print( '-'*80 )

        ### Warning: Must follow above code-block re: CACHE_FILE.
        self.STANDARD_GLUE_CLI_ARGS=[
                # f"'{self.parsed_args.glue_script}'",
                "--ENV",        f"'{self.ENV}'",
                "--branch",     f"'{branch}'",
                "--tempDir",    f"'{self.TMPDIR}'",
                "--runningInsideAWSGlue",  "'Y'",
                "--enableMetrics",         "'y'",
                "--glueCatalogEntryName",   f"'{self.glueCatalogEntryName}'",
                "--commonDatabaseName",     f"'{self.common_DatabaseName}'",
                "--rawBucket",          f"'{self.bucketname_glue_raw_input}'",
                "--processedBucket",    f"'{self.bucketname_glue_processed_input}'",
                "--finalBucket",        f"'{self.bucketname_glue_final}'",

                # "--source_BucketName", self.bucketname_glue_raw_input,
                # "--target_BucketName", self.bucketname_glue_processed_input,
                # "--TempDir", f"s3://{script_bucketname}/{ENV}/tempdir/",
        ]

    ### -------------------------------------------------------------------------------------------
    def sanityChecks(self):

        if not pathlib.Path(self.WORKSPACE_LOCATION / self.GLUESCRIPT_FILENAME ).exists():
            print( f"\n\n!! ERROR !! Script file not found: '{self.GLUESCRIPT_FILENAME}' in folder '{self.WORKSPACE_LOCATION}'" )
            sys.exit(81)

    ### -------------------------------------------------------------------------------------------
    def do_docker_setup(self):

        print( '-'*80 ); print( '-'*80 );
        print( f"starting at {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}" )
        self.STARTED_AT = time.time()

        docker_client = docker.from_env()
        try:
            docker_client.ping()
        except docker.errors.APIError:
            print( "ERROR: Docker Daemon is NOT running!!" )
            sys.exit(60)

        ### https://gallery.ecr.aws/codebuild/local-builds
        # echo Running .. \
        # docker image ls -q "{CODEBUILD_AGENT_IMAGE}"
        def pull_docker_image( docker_client, docker_image ):
            try:
                print( f"docker image ls -q {docker_image} .. .. ", end="", flush=True )
                docker_client.images.get( docker_image )
                print( "Done!")
            except docker.errors.ImageNotFound:
                try:
                    print( f"docker image pull {docker_image} .. .. ", end="", flush=True )
                    docker_client.images.pull( docker_image )
                    print( "Done!")
                except docker.errors.ImageNotFound:
                    print( f"\n\n!! FATAL ERROR !! docker pull {docker_image} failed !!\n" )
                    sys.exit(68)

        pull_docker_image( docker_client=docker_client, docker_image=self.DOCKER_IMAGE_URL )

        print( '-'*80 )

    ### -------------------------------------------------------------------------------------------
    def prep(self):

        pathlib.Path.mkdir( pathlib.Path(self.LOCALDISK_LOGGING_FLDR), parents=True, exist_ok=True )
        zfnm = self.ZIP_FILE_PATH +'/'+ self.ZIP_FILE_NAME
        pathlib.Path.unlink( zfnm, missing_ok=True )
        pathlib.Path.unlink( zfnm +'.log', missing_ok=True )

        import zipfile
        with zipfile.ZipFile( zfnm, "w" ) as myzipfile:
            for f in self.PY_FILES_LIST:
                print( f"adding {f} to {zfnm}")
                myzipfile.write( f )

        print( '-'*80 )

    ### -------------------------------------------------------------------------------------------
    def do(self):

        # print( '-'*80 ); print( '-'*80 );
        print( f"ENDED at {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}" )
        self.ENDED_AT = time.time()
        print( f"TOTAL TIME: {self.ENDED_AT - self.STARTED_AT} seconds" )

        print( "\n\nPlease run the following command:\n")
        print( f"""docker run -it \\
            -v ~/.aws:/home/glue_user/.aws -v {self.WORKSPACE_LOCATION}:/home/glue_user/workspace/  -v '{self.ZIP_FILE_PATH}:{self.DOCKER_ZIP_FILE_PATH}'  -v '{self.LOCALDISK_LOGGING_FLDR}:{self.DOCKER_LOGGING_FLDR}' \\
            -e AWS_PROFILE={self.AWSPROFILE} -e DISABLE_SSL=true {self.DOCKERFLAGS} -e running_on_LAPTOP=true  \\
            -p '{self.PORT1}:{self.PORT1}' -p '{self.PORT2}:{self.PORT2}'  \\
            --name   '{self.DOCKER_CONT_NAME}'     '{self.DOCKER_IMAGE_URL}'  \\
            spark-submit --py-files '{self.DOCKER_ZIP_FILE_PATH}/{self.ZIP_FILE_NAME}'  /home/glue_user/workspace/{self.GLUESCRIPT_FILENAME} \\
        """, end="", flush=True
            # {self.STANDARD_GLUE_CLI_ARGS}
            # {self.parsed_args.}
        )
        for a in self.STANDARD_GLUE_CLI_ARGS:
            print( a, end=" ", flush=True )
        if hasattr( self.parsed_args, 'remaining_sys_argv'):
            for a in self.parsed_args.remaining_sys_argv:
                print( f"'{a}'", end=" ", flush=True )
        print( "\n\n" )

    ### -------------------------------------------------------------------------------------------
    def main(self):
        parser = argparse.ArgumentParser(       ### https://docs.python.org/3/library/argparse.html#argumentparser-objects
            description='Python re-write of AWS-CodeBuild/LOCAL-aws-codebuild-runner.sh',
            prog=pathlib.Path(__file__).stem,
            # usage=
        )

        ### ----------------------------------
        parser.add_argument(                    ### https://docs.python.org/3/library/argparse.html#the-add-argument-method
            '--cleanup', help='cleanup the docker CONTAINERS when done',
            required=False,
            action='store_const', const=True,
        )

        parser.add_argument(                    ### https://docs.python.org/3/library/argparse.html#the-add-argument-method
            '--env', help='dev|test|stage|prod|<git-branch>|<GitHub-PR#>',
            required=True,
        )

        parser.add_argument(                    ### https://docs.python.org/3/library/argparse.html#the-add-argument-method
            'glue_script', help='Relative or Absolute path to a file.',
            nargs="?",
        )

        ### ----------------------------------
        # self.__parsed_args = parser.parse_args( self.argv ) ### https://docs.python.org/3/library/argparse.html#the-parse-args-method

        ### parse_known_args() method can be useful. It works much like parse_args() except that it does not produce an error when extra arguments are present.
        ### https://docs.python.org/3/library/argparse.html#argparse.ArgumentParser.parse_known_args
        [ self.__parsed_args, self.remaining_sys_argv ] = \
                parser.parse_known_args( self.argv ) ### https://docs.python.org/3/library/argparse.html#the-parse-args-method


        return self  ### always a good practice, to help with chaining.

### -----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------------------------------------------

if __name__ == '__main__':
    my_script = MyScript(_argv=sys.argv[1:])  ### Note: the "1:"

    my_script.basicChecks()
    my_script.staticVariables()
    my_script.deriveVariables()
    my_script.sanityChecks()
    my_script.do_docker_setup()
    my_script.prep()
    my_script.do()

### EoF
