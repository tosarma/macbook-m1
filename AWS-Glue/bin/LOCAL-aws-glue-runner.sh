#!/bin/bash -f

# Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


if [ "$1" == "--cleanup" ]; then
    DOCKERFLAGS="--rm"
    shift
fi

if [ $# -le 0 ]; then
    echo "Usage: $0 (--cleanup)?  <GLUESCRIPT_FILENAME> <Optional: PY_SCRIPT_CLI_ARGS> .. .."
    echo '          `--cleanup` CLI-flag will DESTROY the Docker-Container after it exits.'
    exit 1
fi

GLUESCRIPT_FILENAME="$1"
shift







### PY_SCRIPT_CLI_ARGS=( .. --arg1 value1 .. )
### FYI: In pythion-code, glue_args is a dict! To be used as:  glue_args["key"]
### NOTE: the args should be as:  key1 <space> value1 <space> key2 <space> value2 <space> ...
### _NOT_ k=v format _NOT_ !!!
PY_SCRIPT_CLI_ARGS=$@






###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname $0)"
SCRIPT_NAME="$(basename $0)"
CWD="$(pwd)"

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### SECTION: STATIC Configuration

if [ -d .git ]; then
    branch="$(git symbolic-ref --short HEAD)"
else
    branch="unknown-git-branch"
fi
ENV="sandbox"

AWSPROFILE="default"

### https://docs.aws.amazon.com/glue/latest/dg/aws-glue-programming-etl-libraries.html#develop-local-docker-image
DOCKER_IMAGE_NAME="aws-glue-libs:glue_libs_4.0.0_image_01"
# DOCKER_IMAGE_URL="amazon/${DOCKER_IMAGE_NAME}"
DOCKER_IMAGE_URL="public.ecr.aws/glue/${DOCKER_IMAGE_NAME}"

PORT1="4040"
PORT2="18080"

TMPDIR="/tmp/${SCRIPT_NAME}"
mkdir -p "${TMPDIR}"
CACHE_FILE="${TMPDIR}/${ENV}-${branch}.txt"

DOCKER_LOGGING_FLDR="/home/glue_user/spark/logs"      ### !! WARNING !! This is __INSIDE__ the Docker-Image's filesystem.
LOCALDISK_LOGGING_FLDR="${TMPDIR}/python_logs_for_${SCRIPT_NAME}"  ### NOTE: This is on the MacBook-M1 Laptop's filesystem.

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### SECTION: Dynamically generated variables/configuration

# WORKSPACE_LOCATION="$( cd $SCRIPT_FOLDER && cd ../src && pwd )"   ### PARENT folder of ./bin
WORKSPACE_LOCATION="$( dirname ${GLUESCRIPT_FILENAME} )"            ### PARENT folder of ./bin
echo "WORKSPACE ROOT-Folder = '$WORKSPACE_LOCATION'"
if [ "${WORKSPACE_LOCATION}" == "." ]; then
    WORKSPACE_LOCATION="$(pwd)"
    echo "WORKSPACE ROOT-Folder = '$WORKSPACE_LOCATION'"
fi

DOCKER_CONT_NAME="$(basename -s .py ${GLUESCRIPT_FILENAME})-$(openssl rand -hex 8)"
echo "DOCKER_CONTAINER's NAME: '$DOCKER_CONT_NAME'"

###---------------------------------------------------------------

PY_FILES_LIST=$( find . -name '*.py' )

ZIP_FILE_PATH="${TMPDIR}"  ### !! NOTE !! This location is __OUTSIDE__ the Docker-filesystem, that is .. in this MacBook-M1 Laptop's filesystem.
DOCKER_ZIP_FILE_PATH="/tmp/macbook-tmp"  ### !! WARNING !! This is __INSIDE__ the Docker-Image's filesystem.
ZIP_FILE_NAME="py_files_list.zip"

###---------------------------------------------------------------

pwd

if [ -f "${CACHE_FILE}" ]; then
    echo "Loading from cache file: '${CACHE_FILE}' .. .."
    . "${CACHE_FILE}"
else
    CODESTR="from common.names import get_glue_catalog_entry_name;            print(get_glue_catalog_entry_name('${ENV}','${branch}'))"
    glueCatalogEntryName=$( python3 -c "$CODESTR" )
    CODESTR="from common.names import get_glue_common_databasename;        print(get_glue_common_databasename('${ENV}','${branch}'))"
    common_DatabaseName=$( python3 -c "$CODESTR" )
    CODESTR="from common.names import get_bucketname_glue_raw_input;       print(get_bucketname_glue_raw_input('${ENV}','${branch}'))"
    bucketname_glue_raw_input=$( python3 -c "$CODESTR" )
    CODESTR="from common.names import get_bucketname_glue_processed_input; print(get_bucketname_glue_processed_input('${ENV}','${branch}'))"
    bucketname_glue_processed_input=$( python3 -c "$CODESTR" )
    CODESTR="from common.names import get_bucketname_glue_final_input;     print(get_bucketname_glue_final_input('${ENV}','${branch}'))"
    bucketname_glue_final=$( python3 -c "$CODESTR" )

    touch "${CACHE_FILE}"
    echo "creating a CACHE-file ${CACHE_FILE} .. to speed up this command the next time."
    echo "glueCatalogEntryName='$glueCatalogEntryName'" >> "${CACHE_FILE}"
    echo "common_DatabaseName='$common_DatabaseName'"     >> "${CACHE_FILE}"
    echo "bucketname_glue_raw_input='$bucketname_glue_raw_input'"             >> "${CACHE_FILE}"
    echo "bucketname_glue_processed_input='$bucketname_glue_processed_input'" >> "${CACHE_FILE}"
    echo "bucketname_glue_final='$bucketname_glue_final'" >> "${CACHE_FILE}"

    printf "%0.s-" {1..80}; echo ""
    cat "${CACHE_FILE}"
    printf "%0.s-" {1..80}; echo ""

fi

### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

STANDARD_GLUE_CLI_ARGS=(
        "${MY_PYTHON_SCRIPT_PATH}"
        "--ENV" "${ENV}"
        "--branch" "${branch}"
        "--tempDir" "${TMPDIR}"
        "--runningInsideAWSGlue"  "Y"
        "--enableMetrics"         "y"
        "--glueCatalogEntryName" "${glueCatalogEntryName}"
        "--commonDatabaseName" "${common_DatabaseName}"
        "--rawBucket" "${bucketname_glue_raw_input}"
        "--processedBucket" "${bucketname_glue_processed_input}"
        "--finalBucket" "${bucketname_glue_final}"

        # "--source_BucketName" "${bucketname_glue_raw_input}"
        # "--target_BucketName" "${bucketname_glue_processed_input}"
        # "--TempDir" f"s3://{script_bucketname}/{ENV}/tempdir/",
)

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### SECTION: Sanity Checks

if [ ! -f "${WORKSPACE_LOCATION}/${GLUESCRIPT_FILENAME}" ]; then
    echo "ERROR: Script file not found: '${WORKSPACE_LOCATION}/${GLUESCRIPT_FILENAME}'"
    exit 81
fi

if [ -z "${BUILDPLATFORM+x}" ]; then
    echo '';  echo '!!!!!!!!!!!!!!!!! BUILDPLATFORM is _NOT_ set !!!!!!!!!!!!!!!!!'; echo '';
    exit 83
fi

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### SECTION: Prep  --- for the actual running of Glue-job

echo "LOGGING-Folder: '${LOCALDISK_LOGGING_FLDR}/'"
mkdir -p "${LOCALDISK_LOGGING_FLDR}"
ls -la "$LOCALDISK_LOGGING_FLDR"

rm -f "${ZIP_FILE_PATH}/${ZIP_FILE_NAME}"
rm -f "${ZIP_FILE_PATH}/${ZIP_FILE_NAME}.log"

set -x
zip "${ZIP_FILE_PATH}/${ZIP_FILE_NAME}" ${PY_FILES_LIST[@]} >> "${ZIP_FILE_PATH}/${ZIP_FILE_NAME}.log"
set +x

ls -la "${ZIP_FILE_PATH}/${ZIP_FILE_NAME}"

###---------------------------------------------------------------

docker image inspect "${DOCKER_IMAGE_URL}" >& /dev/null
if [ $? -ne 0 ]; then
    echo "Attention: Docker-image is missing, and will be downloaded: '${DOCKER_IMAGE_URL}'"
    set -x
    docker pull "${DOCKER_IMAGE_URL}"
    set +x
fi

###-----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###-----------------------------------------------------------------------------------------------

### SECTION: "Do"

### Use of `--py-files` CLI-args for `spark-submit` command.
### REF: https://spark.apache.org/docs/1.0.2/submitting-applications.html#bundling-your-applications-dependencies

echo ''; echo '----------------------------------------------------------'
set -x

docker run -it      \
        -v ~/.aws:/home/glue_user/.aws -v $WORKSPACE_LOCATION:/home/glue_user/workspace/  -v "${ZIP_FILE_PATH}:${DOCKER_ZIP_FILE_PATH}"  -v "${LOCALDISK_LOGGING_FLDR}:${DOCKER_LOGGING_FLDR}"  \
        -e AWS_PROFILE=$AWSPROFILE -e DISABLE_SSL=true ${DOCKERFLAGS} -e running_on_LAPTOP=true  \
        -p "${PORT1}:${PORT1}" -p "${PORT2}:${PORT2}"   \
        --name   "${DOCKER_CONT_NAME}"     "${DOCKER_IMAGE_URL}"    \
        spark-submit --py-files "${DOCKER_ZIP_FILE_PATH}/${ZIP_FILE_NAME}"  /home/glue_user/workspace/$GLUESCRIPT_FILENAME      \
        ${STANDARD_GLUE_CLI_ARGS[@]}                    \
        ${PY_SCRIPT_CLI_ARGS[@]}

set +x

# EoScript
