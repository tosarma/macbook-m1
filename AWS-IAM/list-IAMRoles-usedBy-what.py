import sys
import boto3
import os
import pathlib
import json
import time
import regex
from typing import Sequence
from datetime import datetime, timedelta
import traceback

from generic_aws_cli_script import ( GenericAWSCLIScript )
from list_orphan_ENIs_for_SG import ( find_orphan_enis_for_sg )

APP_NAME = "nccr"
THIS_SCRIPT_DATA = "IAMRoles"
DEBUG = False

class ListOrphanIAMRoles(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        stack_name_suffix :str,
        aws_profile :str,
        tier :str,
        purpose :str = THIS_SCRIPT_DATA,
        debug :bool = False,
    ) -> None:
        super().__init__(
            appl_name=appl_name,
            purpose=purpose,
            aws_profile=aws_profile,
            tier=tier,
            debug=debug,
        )

        # # get list of Roles named f"{self.app_name}-{self.tier}-{}"
        # if self.tier.startswith(self.appl_name):
        #     self.role_name = f"{self.tier}-{stack_name_suffix}"
        # else:
        #     self.role_name = f"{self.appl_name}-{self.tier}-{stack_name_suffix}"
        # if self.debug: print(self.role_name)

        iam_roles_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'iam',
            api_method_name = "list_roles",
            response_key = 'Roles',
            json_output_filepath = self.json_output_filepath,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 2: print(iam_roles_list)

        ### ---------------------
        self.json_output_filepath_lambdas = self.gen_name_of_json_outp_file( "lambdas", suffix="-all" )

        lambda_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'lambda',
            api_method_name = "list_functions",
            response_key = 'Functions',
            json_output_filepath = self.json_output_filepath_lambdas,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 2: print(lambda_list)

        ### ---------------------
        self.json_output_filepath_gluejobs = self.gen_name_of_json_outp_file( "GlueJobs", suffix="-all" )

        gluejob_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'glue',
            api_method_name = "get_jobs",
            response_key = 'Jobs',
            json_output_filepath = self.json_output_filepath_gluejobs,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 2: print(gluejob_list)

        ### ---------------------
        for role in iam_roles_list:
            rolename = role['RoleName']
            print( '.', end="", flush=True)
            if self.debug: print(rolename +'/'+ role['RoleLastUsed'], end=".. ", flush=True)
            rolearn = role['Arn']

            for function in lambda_list:
                if 'Role' in function and function['Role'] == rolearn:
                    print(f"👉🏾 Role '{rolename}' is in use by 𝜆-fn '{function['FunctionName']}'")

            for gluejob in gluejob_list:
                if 'Role' in gluejob and (gluejob['Role'] == rolearn or gluejob['Role'] in rolearn):
                    print(f"👉🏾 Role '{rolename}' is in use by GLUE-JOB '{gluejob['Name']}'")

### ####################################################################################################

# if invoked via cli
if __name__ == "__main__":
    if len(sys.argv) >= 2:
        aws_profile = sys.argv[1]
        # tier = sys.argv[2]
        # stack_name_suffix = sys.argv[3]
        scr = ListOrphanIAMRoles(
            appl_name=APP_NAME,
            stack_name_suffix=None, ### stack_name_suffix,
            aws_profile=aws_profile,
            tier=None, ### tier,
            purpose=THIS_SCRIPT_DATA,
            debug=DEBUG,
        )
    else:
        print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> " )
        print( f"EXAMPLE: python {sys.argv[0]} NCCR-nonprod" )
        # print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> <TIER> <STACK_NAME_SUFFIX>" )
        # print( f"EXAMPLE: python {sys.argv[0]} NCCR-nonprod  1854  vpc" )

# EoScript
