### 1. List of all IAM-Roles in entire AWS-Account .. .. and then .. ..
### 2. Filter OUT those that are .. do NOT belong to the App or allowed-TIERS.
### 3. Count total # of IAM-Roles (pre and post-filtered)
### 4. Count total # of IAM-Roles for each UNIQUE Lambda across Tiers (post-filtered).
### 5. Dump the IAM-Policies (by above grouping)

import sys
import boto3
import os
import pathlib
import json
import time
import regex
from datetime import datetime, timedelta
import traceback

from aws_api_invoker import (
    InvokeAWSApi,
    MyException,
)

if len(sys.argv) != 3:
    print("Usage: python script.py <AWS_PROFILE> <AWS_ENV>")
    sys.exit(1)

aws_profile = sys.argv[1]
env         = sys.argv[2]

### ----------------------------------------------------------------------
### Mnually configurable constants.

loggrps_older_than_in_days = 2 ### filter for all log-groups OLDER thah these many adays

appl_name = 'MyApp'

cache_no_older_than = 7     ### maximum _ days old before invoking SDK-APIs to refresh the json_output_filepath_roles

all_tiers = [
    regex.regex.Regex("dev"),
    regex.regex.Regex("test"),
    regex.regex.Regex("uat"),
    regex.regex.Regex("prod"),
    regex.regex.Regex("nccr-[a-z0-9A-Z-]+"),
]
lambdaname_extract_regex = regex.regex.Regex("nccr-[a-z0-9A-Z-]+-stateless-(.*)-[a-z0-9A-Z]+")

aws_managed_policies :list = [
    "arn:aws:iam::aws:policy/ReadOnlyAccess",
    "arn:aws:iam::aws:policy/AWSSupportAccess",
    "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
    "arn:aws:iam::aws:policy/CloudWatchLambdaInsightsExecutionRolePolicy",
    "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole",
    "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs",
]

lambdas_to_ignore = [
    "BucketNotificationsH",
    "LogRetention",
]
policies_to_ignore = [
    "[]",
    '[{"Version": "2012-10-17", "Statement": [{"Effect": "Allow", "Action": ["logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"], "Resource": "*"}]}]',
]

DEBUG = False

TMPDIR    = '/tmp'

### ----------------------------------------------------------------------
### Section: Derived variables and constants

json_output_filepath_roles = f"{TMPDIR}/all-iam-roles.json"
json_output_filepath_roles = pathlib.Path(json_output_filepath_roles) ### convert a string into a Path object.

json_output_filepath_Policies = f"{TMPDIR}/all-iam-policies.json"
json_output_filepath_Policies = pathlib.Path(json_output_filepath_Policies) ### convert a string into a Path object.

json_output_filepath_RoleAssociatedPolicies = f"{TMPDIR}/all-iam-DERIVED-rolepolicies.json"
json_output_filepath_RoleAssociatedPolicies = pathlib.Path(json_output_filepath_RoleAssociatedPolicies) ### convert a string into a Path object.

### ----------------------------------------------------------------------
awsapi_invoker = InvokeAWSApi(debug=DEBUG)

session = awsapi_invoker.sanity_check_awsprofile(aws_profile=aws_profile)

client = session.client('iam')

complete_response_iam_roles = awsapi_invoker.list_iam_roles(
    json_output_filepath=json_output_filepath_roles,
    aws_profile=aws_profile,
    cache_no_older_than=cache_no_older_than,
)

complete_response_iam_Policies = awsapi_invoker.list_iam_policies(
    json_output_filepath=json_output_filepath_Policies,
    aws_profile=aws_profile,
    cache_no_older_than=cache_no_older_than,
)

### ----------------------------------------------------------------------
### iam-P O L I C I E S

### produce STATS for human consumption
### Also, creata a new "lookup" dict using PolicyArn as dict-Key

print("Loading Cache for DERIVED-Info re: Policies-ASSOCIATED-with-Roles.. ..", end="")
iam_Policy_lookup = {}
iam_role_associated_inline_policy_lookup = awsapi_invoker.load_role_associated_inline_policy_cache(
    json_output_filepath=json_output_filepath_RoleAssociatedPolicies,
)
print(".. Done")

total_rows = 0
total_filtered_rows = 0
other_count = 0

num_of_proj_iam_Policies = {}
for patt in all_tiers:
    num_of_proj_iam_Policies[patt] = 0

# Iterate over the IAM-POLICIES
for iamPolicy in complete_response_iam_Policies:
    total_rows += 1
    iamPolicy_name = iamPolicy['PolicyName']
    creation_datetime = iamPolicy['CreateDate']
    policy_arn = iamPolicy.get('Arn')
    if DEBUG: print(f"{iamPolicy_name}:\t{creation_datetime}\t{policy_arn}")

    total_rows += 1
    is_proj_iam_Policy = False
    lambda_name = None
    for patt in all_tiers:
        if patt.match(iamPolicy_name):
            num_of_proj_iam_Policies[patt] += 1
            break

    iam_Policy_lookup[policy_arn] = iamPolicy
    print("_", end="")

    # filter
    # if not is_proj_iam_Policy:
    #     continue
    #         is_proj_iam_Policy = True

    # client = session.client('iam')
    # response = client.get_policy( PolicyArn=policy_arn )

    # if match := lambdaname_extract_regex.match(iamPolicy_name):
    #     lambda_name = match.group(1)
    #     print(f"Extracted lambda-name '{lambda_name}'")
    # else:
    #     print(f"Skipping '{iamPolicy_name}' .. ", end="")
    #     # raise MyException(f"!!ERROR - FAILED!! regex to EXTRACT lambda-name from '{iamPolicy_name}'")


    # print(f"{iamPolicy_name}: {retention_setting} {loggrpSizeInBytes} {creation_datetime.astimezone()}")
print("\nProject-specific IAM-Policies:\n", '_'*80, "\n")
for patt in all_tiers:
    print(f"{patt.pattern}:\t\t{num_of_proj_iam_Policies[patt]}")

print("\nSUMMARY COUNTS:\n", '_'*80, "\n")
print(f"# of IAM-Policies (Not one of the RegExp): {total_rows}")
print(f"# of PROJECT-Specific IAM-Policies: {total_filtered_rows}\n\n")

### ----------------------------------------------------------------------
### iam-R O L E S

### produce STATS for human consumption
total_rows = 0
total_filtered_rows = 0
other_count = 0

num_of_proj_iam_roles = {}
for patt in all_tiers:
    num_of_proj_iam_roles[patt] = 0

lambda_specific_policies_inline = {}
lambda_specific_policies_mgd = {}

# Iterate over the IAM-Roles
for iamrole in complete_response_iam_roles:
    total_rows += 1
    iamrole_name = iamrole['RoleName']
    creation_datetime = iamrole['CreateDate']
    # assumeRolepolicydocu = iamrole.get('AssumeRolePolicyDocument')
    if DEBUG: print(f"{iamrole_name}: {creation_datetime}")

    total_rows += 1
    is_proj_iam_role = False
    lambda_name = None
    for patt in all_tiers:
        if patt.match(iamrole_name):
            num_of_proj_iam_roles[patt] += 1
            break

    match = lambdaname_extract_regex.match(iamrole_name)
    if not match:
        if DEBUG: print(f"DEBUG: IGNORING IAM-Role '{iamrole_name}' .. ")
        print("▷", end="")
        continue

    is_proj_iam_role = True
    lambda_name = match.group(1)
    if DEBUG: print(f"DEBUG: Extracted lambda-name '{lambda_name}'")

    for lambda_name_snippet in lambdas_to_ignore:
        if lambda_name_snippet in lambda_name:
            if DEBUG: print(f"DEBUG: IGNORING IAM-Role '{iamrole_name}' as it belong to an IGNORABLE-𝜆 {lambda_name} .. ")
            print("𝜆", end="")
            continue

    # Get --ALL-- the Policy Statements associated with this IAM-Role
    # Get inline policies
    inline_policies = []
    try:
        if iamrole_name in iam_role_associated_inline_policy_lookup:
            response = iam_role_associated_inline_policy_lookup[iamrole_name]
        else:
            print(f"!!!!!! Policy IN-MEMORY Lookup = None.  Invoking AWS-API list_role_policies() for {iamrole_name} !!!!!!")
            response = client.list_role_policies(RoleName=iamrole_name)
            iam_role_associated_inline_policy_lookup[iamrole_name] = response
            # print(response)

        for policy_name in response['PolicyNames']:
            print("!", end="")
            if DEBUG: print(f"DEBUG: Role:\t{iamrole_name}\thas a INLINE-Policy: {policy_name}")
            policy_arn = f"arn:aws:iam::{iamrole_name}:policy/{policy_name}"
            if policy_arn in iam_role_associated_inline_policy_lookup:
                policy_details = iam_role_associated_inline_policy_lookup[policy_arn]
            else:
                print(f"!!!!!! Policy IN-MEMORY Lookup = None.  Invoking AWS-API get_role_policy() for {policy_arn} !!!!!!")
                policy = client.get_role_policy(RoleName=iamrole_name, PolicyName=policy_name)
                policy_details = policy['PolicyDocument']
                iam_role_associated_inline_policy_lookup[policy_arn] = policy_details
            inline_policies.append( policy_details)

    except Exception as e:
        traceback.print_exc()
        print(f"Error getting inline policies: {str(e)}")
        print(f"Error getting inline-associated policies for\t{iamrole_name}")
        time.sleep(2)

    # Get managed policies
    managed_policies = []
    policy_arn = "define it OUTSIDE try-catch to be able to print useful-details inside 'except'"
    try:
        if iamrole_name+"-listatt" in iam_role_associated_inline_policy_lookup:
            response = iam_role_associated_inline_policy_lookup[iamrole_name+"-listatt"]
        else:
            print(f"!!!!!! Policy IN-MEMORY Lookup = None.  Invoking AWS-API list_attached_role_policies() for {iamrole_name} !!!!!!")
            response = client.list_attached_role_policies(RoleName=iamrole_name)
            iam_role_associated_inline_policy_lookup[iamrole_name+"-listatt"] = response
            # print(response)

        for policy in response['AttachedPolicies']:
            policy_name = policy['PolicyName']
            policy_arn = policy['PolicyArn']
            if DEBUG: print(f"DEBUG: Role:\t{iamrole_name}\thas a ?? Managed-Policy: {policy_name}\t{policy_arn}", end="")

            if aws_managed_policies.count(policy_arn) > 0:
                print(".", end="")
                if DEBUG: print(f"\tSkipping AWS-Managed Policy")
                # print(f"\tSkipping AWS-Managed Policy {policy_arn}")
                continue

            if DEBUG: print(f"Role:\t{iamrole_name}\thas a Customer-Managed-Policy: {policy_name}\t{policy_arn}")
            if policy_arn in iam_Policy_lookup:
                print("+", end="")
                policy_details = iam_Policy_lookup[policy_arn]
            else:
                print(f"!!!!!! Policy Lookup Failed. Invoking AWS-API for {policy_arn} !!!!!!")
                policy_details= client.get_policy(PolicyArn=policy_arn)
                policy_details = policy_details['Policy']

            # if DEBUG: print(policy_details)
            policy_version = policy_details['DefaultVersionId']
            if policy_arn+"-latest" in iam_role_associated_inline_policy_lookup:
                policy_document = iam_role_associated_inline_policy_lookup[policy_arn+"-latest"]
            else:
                print(f"!!!!!! Policy IN-MEMORY Lookup = None.  Invoking AWS-API get_policy_version() for {policy_arn} !!!!!!")
                response = client.get_policy_version(PolicyArn=policy_arn, VersionId=policy_version)
                policy_document = response['PolicyVersion']['Document']
                iam_role_associated_inline_policy_lookup[policy_arn+"-latest"] = policy_document
                # print(response)

            managed_policies.append( policy_document )

    except Exception as e:
        traceback.print_exc()
        print(f"Error getting managed policies: {str(e)}")
        print(f"Error getting managed policies for\t{iamrole_name}\t{policy_name}\t{policy_arn}")
        time.sleep(2)

    if lambda_name not in lambda_specific_policies_inline:
        lambda_specific_policies_inline[lambda_name] = []
    if lambda_name not in lambda_specific_policies_mgd:
        lambda_specific_policies_mgd[lambda_name] = []
    lambda_specific_policies_inline[lambda_name].append(inline_policies)
    lambda_specific_policies_mgd[lambda_name].append(managed_policies)

    # print(f"{iamrole_name}: {retention_setting} {loggrpSizeInBytes} {creation_datetime.astimezone()}")

### -----------------------------------------------------------------------------------------------------------

awsapi_invoker.save_role_associated_inline_policy_cache(
    json_output_filepath=json_output_filepath_RoleAssociatedPolicies,
    all_role_associated_inline_policies=iam_role_associated_inline_policy_lookup,
)

print("\nProject-specific IAM-Roles:\n", '_'*80, "\n")
for patt in all_tiers:
    print(f"{patt.pattern}:\t\t{num_of_proj_iam_roles[patt]}")

print("\nPOLICIES-INLINE for each Lambda:\n", '_'*80, "\n")
for lambda_name, policies in lambda_specific_policies_inline.items():
    print('.'*20, end=""); print( lambda_name, end="" ); print('.'*20);
    for policy in policies:
        if policies_to_ignore.count(json.dumps(policy)) <= 0:
            print(f"\t{policy}")

print("\nPOLICIES CUSTOMER-Managed (Not AWS-Mgd) for each Lambda:\n", '_'*80, "\n")
for lambda_name, policies in lambda_specific_policies_mgd.items():
    print('.'*20, end=""); print( lambda_name, end="" ); print('.'*20);
    for policy in policies:
        if policies_to_ignore.count(json.dumps(policy)) <= 0:
            print(f"\t{policy}")

print("\nSUMMARY COUNTS:\n", '_'*80, "\n")
print(f"# of IAM-Roles (Not one of the RegExp): {total_rows}")
print(f"# of PROJECT-Specific IAM-Roles: {total_filtered_rows}\n\n")

### EoScript
