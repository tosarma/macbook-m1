import sys
import boto3
import os
import pathlib
import json
import time
import regex
from typing import Sequence
from datetime import datetime, timedelta
import traceback

from generic_aws_cli_script import ( GenericAWSCLIScript )

APP_NAME = "nccr"
THIS_SCRIPT_DATA = "roles"
DEBUG = False


class GetAllPoliciesAssociatedWithIAMRole(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        role_name_suffix :str,
        aws_profile :str,
        tier :str,
        purpose :str = THIS_SCRIPT_DATA,
        debug :bool = False,
    ) -> None:
        super().__init__(
            appl_name=appl_name,
            aws_profile=aws_profile,
            tier=tier,
            purpose=purpose,
            debug=debug,
        )

        # get list of stacks named f"{self.app_name}-{self.tier}-{}"
        if self.tier.startswith(self.appl_name):
            self.role_name_regex = f"{self.tier}-{role_name_suffix}"
        else:
            self.role_name_regex = f"{self.appl_name}-{self.tier}-{role_name_suffix}"
        if self.debug: print(self.role_name_regex)
        # stack_list = self.get_stack_list()

        self.role_name_regex = f"{self.role_name_regex}.*"
        # self.role_name_regex = f"{self.role_name_regex}.{0,20}"
        # self.role_name_regex = f"{self.role_name_regex}" ### exact match

        iam_client = self.session.client("iam")

        iamrole_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'iam',
            api_method_name = "list_roles",
            response_key = 'Roles',
            json_output_filepath = self.json_output_filepath,
            additional_params={},
            # additional_params={ "?????StackStatusFilter": ALL_ACTIVE_STACK_STATUSES },
            # additional_param_name="StackStatusFilter",
            # additional_param_value=ALL_ACTIVE_STACK_STATUSES,
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 3: print(iamrole_list)

        for aRole in iamrole_list:
            ### Sample aRole = {
            ###     "RoleName": "aws-control-tower-set-account-password-role",
            ###     "Arn": "arn:aws:iam::519794227637:role/aws-control-tower-set-account-password-role",
            ###     "AssumeRolePolicyDocument": {
            ###         "Version": "2012-10-17",
            ###         "Statement": [
            ###             {
            ###                 "Effect": "Allow",
            ###                 "Principal": {
            ###                     "Service": "lambda.amazonaws.com"
            ###                 },
            ###                 "Action": "sts:AssumeRole"
            ###             }
            ###         ]
            ###     },
            ###     "Description": "",
            ###     "CreateDate": "2024-01-30 14:09:07+00:00",
            ###     "RoleId": "AROAXSBRGPG26WRAPL3BG",
            ###     "Path": "/",
            ###     "MaxSessionDuration": 3600
            ### },
            aRoleName = aRole['RoleName']
            print( '.', end="", flush=True)
            if self.debug: print(aRoleName , end=".. ", flush=True)
            if regex.match( pattern=self.role_name_regex, string=aRoleName ):

                if self.debug: print(f"REGEX-Matches the Role = '{aRoleName}' !!!  ")
                if "InlinePolicyList" in aRole:
                    policy_names = aRole['InlinePolicyList'] ### load from cache.
                else:
                    policy_names = iam_client.list_role_policies( RoleName=aRoleName )['PolicyNames']
                    aRole["InlinePolicyList"] = policy_names ### to be Saved into cache.
                if self.debug > 1: print(json.dumps(policy_names, indent=4, default=str))
                print( f'{{ "{aRoleName}": [' )

                for policy_name in policy_names:
                    if policy_name in aRole:
                        policy_docu = aRole[policy_name] ### load from cache.
                    else:
                        policy_docu = iam_client.get_role_policy( RoleName=aRoleName, PolicyName=policy_name )['PolicyDocument']
                        aRole[policy_name] = policy_docu ### to be Saved into cache.
                    if self.debug > 1: print(json.dumps(policy_docu, indent=4, default=str))
                        ### Sample policy_docu = { "Version": "2012-10-17",
                        ###    "Statement": [
                        ###        {   "Effect": "Allow", "Resource": "*",
                        ###            "Action": "s3:PutBucketNotification",
                        ###        },
                        ###        {   "Effect": "Allow", "Resource": "*",
                        ###            "Action": "s3:GetBucketNotification",
                        ### }]}

                    print( f'\t{{ "{policy_name}": ', end="", flush=True )
                    print( json.dumps(policy_docu["Statement"], indent=4, default=str).replace("\n", "\n\t\t") )
                    print( f'}}' )

                    # if self.debug:
                    #     print("Orphan ENIs tobe manually destroyed:")
                    #     print(json.dumps(enis, indent=4, default=str))
                    #     print("\n\n")
                    # # delete each "eni" within the list "enis"
                    # for eni in enis:
                    #     eni_id = eni['NetworkInterfaceId']
                    #     print(f"Deleting ENI {eni_id} ..", end="")
                    #     resp = iam_client.delete_network_interface(NetworkInterfaceId=eni_id)
                    #     if debug: print(json.dumps(resp, indent=4, default=str))
                    #     print("Done")

                print( f']}}' )

        self.awsapi_invoker.update_diskfile_cache(
            json_output_filepath = self.json_output_filepath,
            inmemory_cache = iamrole_list,
        )

### ####################################################################################################

# if invoked via cli
if __name__ == "__main__":
    if len(sys.argv) >= 4:
        aws_profile = sys.argv[1]
        tier = sys.argv[2]
        role_name_suffix = sys.argv[3]
        scr = GetAllPoliciesAssociatedWithIAMRole(
            appl_name=APP_NAME,
            purpose=THIS_SCRIPT_DATA,
            role_name_suffix=role_name_suffix,
            debug=DEBUG,
        )
    else:
        print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> <TIER> <STACK_NAME_SUFFIX>" )
        print( f"EXAMPLE: python {sys.argv[0]} NCCR-nonprod  1854  vpc" )

# EoScript
