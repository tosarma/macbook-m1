#!/bin/bash -f

### From the list of all EXISTING IAM-Roles, extract the tier (the prefix) from Role's name.
### From the tier, derive the git-branch's name and Ticket #
### If the Ticket # is closed or the git-branch NO longer exists, then the IAM-Role is an Orphan!
### Delete that Orphan IAM-Role

###---------------------------------------------------------------

GIT_BRANCHID_PATTERN=".*"

# IAM_ROLE_PATTERN="stateless-privateapiprivaterestapiCloudWat"
IAM_ROLE_PATTERN=".*"

###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
TOPMOST_SCRIPT_FOLDER="$(cd ${SCRIPT_FOLDER}/.. ; pwd)"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"

###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${TOPMOST_SCRIPT_FOLDER}/settings.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### ---------- get list of all 900 IAM-Role NAMES.
aws iam list-roles --output json ${AWSPROFILEREGION} > /tmp/json
jq '.Roles[].RoleName' /tmp/json --raw-output > /tmp/o

### ---------- get the “nccr-???”  git-branch IDs (for the IAM-Roles that contain the name [stateless-privateapiprivaterestapiCloudWat]
BRANCH_IDs=$( grep "${PROJECTID}-${GIT_BRANCHID_PATTERN}-${IAM_ROLE_PATTERN}" /tmp/o | cut -d '-' -f 2 )
echo "BRANCH_IDs="
# echo "${BRANCH_IDs[@]}"
echo "BRANCH_IDs(simplified)="
BRANCH_IDs=$( echo ${BRANCH_IDs[@]} | sed -e 's| |\n|g' | sort -u )
# echo "${BRANCH_IDs[@]}"

### --------------- make sure you are in a git-cloned folder !!!!!!! ----------------
git checkout main
git fetch --all
git status

### --------------- LOOP for each Git-Branch
for brid in ${BRANCH_IDs[@]}; do

    ### --------------- for each Git-Branch, check to see if it is ACTIVE
    exists_in_remote=$( git ls-remote --heads origin "${PROJECTID}-${brid}" )
    # echo "exists_in_remote='${exists_in_remote}'"
    if [[ "${exists_in_remote}" == "" ]]; then

        echo " ### --- ${PROJECTID}-$brid ---";
        ### ----------- if --NOT-- an ACTIVE branch .. then ECHO AWS commands to delete the IAM-Roles.
        ROLES=$( grep "${PROJECTID}-${brid}-${IAM_ROLE_PATTERN}" /tmp/o )
        for ROLE in ${ROLES[@]}; do
            echo "aws iam delete-role --role-name ${ROLE} ${AWSPROFILEREGION}"
        done
    else
        echo " ### .. skipping ${PROJECTID}-${brid} .. .. as that git-BRANCH is still ACTIVE !!!"
    fi
    sleep 1
done

### EoF
