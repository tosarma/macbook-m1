import sys
import boto3
import os
import pathlib
import json
import time
import regex
from typing import Sequence
from datetime import datetime, timedelta
import traceback

from generic_aws_cli_script import ( GenericAWSCLIScript )
from list_orphan_ENIs_for_SG import ( find_orphan_enis_for_sg )

APP_NAME = "nccr"
THIS_SCRIPT_DATA = "IAMRoles"
DEBUG = False

class ListOrphanIAMRoles(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        stack_name_suffix :str,
        aws_profile :str,
        tier :str,
        purpose :str = THIS_SCRIPT_DATA,
        debug :bool = False,
    ) -> None:
        super().__init__(
            appl_name=appl_name,
            purpose=purpose,
            aws_profile=aws_profile,
            tier=tier,
            debug=debug,
        )

        # # get list of Roles named f"{self.appl_name}-{self.tier}-{}"
        # if self.tier.startswith(self.appl_name):
        #     self.role_name = f"{self.tier}-{stack_name_suffix}"
        # else:
        #     self.role_name = f"{self.appl_name}-{self.tier}-{stack_name_suffix}"
        # if self.debug: print(self.role_name)

        iam_roles_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'iam',
            api_method_name = "list_roles",
            response_key = 'Roles',
            json_output_filepath = self.json_output_filepath,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 2: print(iam_roles_list)

        ### ---------------------
        self.json_output_filepath_lambdas = self.gen_name_of_json_outp_file( "lambdas", suffix="-all" )

        lambda_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'lambda',
            api_method_name = "list_functions",
            response_key = 'Functions',
            json_output_filepath = self.json_output_filepath_lambdas,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 2: print(lambda_list)

        ### ---------------------
        self.json_output_filepath_gluejobs = self.gen_name_of_json_outp_file( "GlueJobs", suffix="-all" )

        gluejob_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'glue',
            api_method_name = "get_jobs",
            response_key = 'Jobs',
            json_output_filepath = self.json_output_filepath_gluejobs,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 2: print(gluejob_list)

        ### ---------------------
        self.json_output_filepath_apigw = self.gen_name_of_json_outp_file( "APIGW", suffix="-all" )

        api_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'apigateway',
            api_method_name = "get_rest_apis",
            response_key = 'items',
            json_output_filepath = self.json_output_filepath_apigw,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        apigw_client = self.session.client('apigateway')
        for api in api_list:
            restApiId = api['id']
            ### Get all details about this api
            if 'Resources' not in api:
                print("↓", end="", flush=True)
                api_details :dict = apigw_client.get_resources( restApiId = restApiId )
                api['Resources'] = api_details['items']
                for resource in api['Resources']:
                    if 'resourceMethods' in resource:
                        for method_name, method in resource['resourceMethods'].items():
                            # Get detailed method information
                            print("↓", end="", flush=True)
                            method_details = apigw_client.get_method(
                                restApiId=api['id'],
                                resourceId=resource['id'],
                                httpMethod=method_name
                            )
                            if 'credentials' in method_details.get('methodIntegration', {}):
                                method_role_arn = method_details.get('methodIntegration')['credentials'];
                                if self.debug > 1: print(f" .. in API '{api['name']}' ({api['id']}) for method {method_name} on resource {resource['path']} Role is {method_role_arn}" )
                                print(f" .. in API '{api['name']}' ({api['id']}) for method {method_name} on resource {resource['path']} Role is {method_role_arn}" )
                                ### arn:aws:iam::123456789012:role/nccr-1189-3-stateless-privateapiprivaterestapiCloud-Kydl4BsdHLkq

            if 'Stages' not in api:
                ### https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/apigateway/client/get_stages.html
                stages = apigw_client.get_stages( restApiId = restApiId )
                api['Stages'] = stages['item']
            if 'AcctLevelCWRoleArn' not in api:
                account_settings = apigw_client.get_account()
                api['AcctLevelCWRoleArn'] = account_settings.get('cloudwatchRoleArn',None)
        self.awsapi_invoker.update_diskfile_cache( self.json_output_filepath_apigw, api_list )
        if self.debug > 2: print(api_list)

        ### ---------------------
        self.json_output_filepath_StepFns = self.gen_name_of_json_outp_file( "StepFns", suffix="-all" )

        stepFn_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'stepfunctions',
            api_method_name = "list_state_machines",
            response_key = 'stateMachines',
            json_output_filepath = self.json_output_filepath_StepFns,
            additional_params={},
            cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        sfn_client = self.session.client('stepfunctions')
        for sfn in stepFn_list:
            ### Get all details about this sfn
            ### https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/stepfunctions/client/describe_state_machine.html
            if 'roleArn' not in sfn:
                sfn_details :dict = sfn_client.describe_state_machine(
                    stateMachineArn=sfn['stateMachineArn'],
                    includedData='ALL_DATA',
                )
                for k in sfn_details.keys():
                    sfn[k] = sfn_details[k]
        self.awsapi_invoker.update_diskfile_cache( self.json_output_filepath_StepFns, stepFn_list )
        if self.debug > 2: print(stepFn_list)

        # ### ---------------------
        # self.json_output_filepath_cognitoUserPools = self.gen_name_of_json_outp_file( "GlueJobs", suffix="-all" )

        # cognito_userpools_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
        #     aws_client_type = 'cognito-idp',
        #     api_method_name = "list_user_pools",
        #     response_key = 'UserPools',
        #     json_output_filepath = self.json_output_filepath_cognitoUserPools,
        #     additional_params={},
        #     cache_no_older_than = 5, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        # )
        # if self.debug > 2: print(cognito_userpools_list)

        ### ---------------------
        unused_roles_list = []
        for role in iam_roles_list:
            rolename :str = str(role['RoleName'])
            # confirm that the type of rolename is str
            if not isinstance(rolename, str):
                raise Exception(f"Error: rolename is not a string: {rolename}")
            role_last_used = role['RoleLastUsed'] if 'RoleLastUsed' in role else None
            if not regex.match(f"{self.appl_name}-.*", rolename):
                if self.debug: print(f"👉🏾 Role '{rolename}' is NOT matching the regex '{self.appl_name}-.*'")
                continue
            sss : list[regex.regex.Pattern]= [
                regex.compile( '.*nccr-cdk-hnb659fds-.*' ),
                regex.compile( '.*nccr-CommonStack-nonprod.*' ),
                regex.compile( '.*identit?y?-p?o?o?l?-?cognitoembeddingQS.*' ),
                regex.compile( '.*identit?y?-p?o?o?l?-?cognitomyIdentityPool.*' ),
                regex.compile( '.*identit?y?-p?o?o?l?-?cognitoDataupload.*' ),
                regex.compile( '.*StaticStack-nccrrumidentitypool.*' ),
                regex.compile( '.*data-lake-clean_crawler-crawler.*' ),

                regex.compile( '.*codeb?u?i?l?d?-autotesting.*' ),
                # regex.compile( '.*privateapiprivaterestapiClou.*' ),
                regex.compile( '.*statel?e?s?s?-privateapirestapiCl?o?u?d?W?a?.*' ),
                regex.compile( '.*statel?e?s?s?-privateapiprivaterestapiCl?o?u?d?W?a?.*' ),
            ]
            # sss : list[str]= [
            #     'nccr-cdk-hnb659fds-',
            #     'nccr-CommonStack-nonprod',
            #     'identity-pool-cognitoembeddingQS',
            #     'identit-pool-cognitoembeddingQS',
            #     'identi-pool-cognitoembeddingQS',
            #     'identity-pool-cognitomyIdentityPool',
            #     'identit-pool-cognitomyIdentityPool',
            #     'stateless-privateapirestapiCloudWa',
            #     'stateles-privateapirestapiCloudWa',
            # ]
            patt_match = False
            for r in sss:
                # ## r represents a regular expression, but could be a sub-string of rolename. Match all such scenarios
                # print( f"rolename = '{rolename}' and r = '{r}'")
                if r.match( rolename ):
                # ### Confirm r is a substring of rolename
                # if r.casefold() in rolename.casefold():
                    if self.debug: print(f"👉🏾 Role '{rolename}' has '{s}' in its name!  So, skipping")
                    patt_match = True
            if patt_match:
                continue

            print( '.', end="", flush=True)
            if self.debug: print(rolename +' | Created: '+ role['CreateDate'] +' | Last-Used: '+ role_last_used, end=".. ", flush=True)
            rolearn = role['Arn']

            is_role_in_use = False
            for function in lambda_list:
                if 'Role' in function and function['Role'] == rolearn:
                    if self.debug > 1: print(f"👉🏾 Role '{rolename}' is in use by 𝜆-fn '{function['FunctionName']}'")
                    is_role_in_use = True

            for gluejob in gluejob_list:
                if 'Role' in gluejob and (gluejob['Role'] == rolearn or gluejob['Role'] in rolearn):
                    if self.debug > 1: print(f"👉🏾 Role '{rolename}' is in use by GLUE-JOB '{gluejob['Name']}'")
                    is_role_in_use = True

            for stepFn in stepFn_list:
                if 'roleArn' in stepFn and stepFn['roleArn'] == rolearn:
                    if self.debug > 1: print(f"👉🏾 Role '{rolename}' is in use by StepFunc '{stepFn['name']}'")
                    is_role_in_use = True

            for api in api_list:
                if 'AcctLevelCWRoleArn' in api:
                    if self.debug > 5: print( f"    comparing .. {rolearn} vs. {api['AcctLevelCWRoleArn']}")
                if 'AcctLevelCWRoleArn' in api and api['AcctLevelCWRoleArn'] == rolearn:
                    if self.debug > 1: print(f"👉🏾 Role '{rolename}' is in use by APIGW's 'AccountLevel-CWRole")
                    is_role_in_use = True
                    break
                for res in api['Resources']:
                    # Check resource methods
                    if 'resourceMethods' in res:
                        for methodName, method_details in res['resourceMethods'].items():
                            if 'credentials' in method_details.get('methodIntegration', {}):
                                method_role_arn = method_details.get('methodIntegration')['credentials'];
                                if method_role_arn == rolearn:
                                    if self.debug > 1: print(f"👉🏾 Role '{rolename}' is in use by APIGW-RestApi '{api['id']} - {methodName}'")
                                    print(f"👉🏾 Role '{rolename}' is in use by APIGW-RestApi '{api['id']} - {methodName}'")
                                    is_role_in_use = True


            ### ------------------
            if not is_role_in_use:
                s = 'Not been used in 30+ days!!!' if role_last_used and (role_last_used < (datetime.now() - timedelta(days=30))) else ''
                print(f"\nRole '{rolename}' is NOT in use by any 𝜆-fn/ GLUE-JOB/ SFn /RestAPI  {s}")
                unused_roles_list.append(rolename)

### ####################################################################################################

# if invoked via cli
if __name__ == "__main__":
    if len(sys.argv) >= 2:
        aws_profile = sys.argv[1]
        # tier = sys.argv[2]
        # stack_name_suffix = sys.argv[3]
        scr = ListOrphanIAMRoles(
            appl_name=APP_NAME,
            stack_name_suffix=None, ### stack_name_suffix,
            aws_profile=aws_profile,
            tier=None, ### tier,
            purpose=THIS_SCRIPT_DATA,
            debug=DEBUG,
        )
    else:
        print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> " )
        print( f"EXAMPLE: python {sys.argv[0]} NCCR-nonprod" )
        # print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> <TIER> <STACK_NAME_SUFFIX>" )
        # print( f"EXAMPLE: python {sys.argv[0]} NCCR-nonprod  1854  vpc" )

# EoScript
