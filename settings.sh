#!/bin/false

### Attention: This file must ONLY be sourced from other bash-scripts.

### Full instructions to switch between Accounts, is in `README-switch-client.md``
### if [ ! -e common.sh ]; then echo '!ERROR! common.sh is MISSING !!'; else if [ -L common.sh ]; then rm common.sh; ln -s common-PersonalAWS.sh common.sh; eval ls -la "common*"; else echo '!ERROR! common.sh is a REGULAR FILE!'; fi fi

if [ -z ${TIER+x} ]; then
    export TIER="dev"
    export ENV=${ENV}
else
    export ENV="${TIER}"
fi

export env="${ENV}"
ENVIRONMENT="${ENV}"

export AWSREGION="us-east-1"
export REGIONNAME="Virgina"

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

VERSION="v1.9.8"

# component="backend"
# component="frontend"
component="base"

PRODUCT="factrial"
application="emfact"
branch="$(git symbolic-ref --short HEAD)"

### --------------------------------------------------------

### DEVOPS Variables

USER="seangchan-ryu-nih"
USER="sarma-nci-nih-essex"

ORG="CBIIT"

OWNER="IODC"
OWNER="NCI-DCCPS"

# PROJECTNAME="nccr-dp"
# PROJECTID="nccr"
PROJECTNAME="${application}"
PROJECTID="${PROJECTNAME}"
REPONAME="iodc-bento-sandbox"
REPONAME="nccr-dataplatform"

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------
###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

if [ -z ${AWSPROFILE+x} ]; then
    export AWSPROFILE="UNDEFINED!!!"
    if [ "${ENV}" == "dev" ]; then
        export AWSPROFILE="DEVINT"
    fi
    if [ "${ENV}" == "int" ]; then
        export AWSPROFILE="DEVINT"
    fi
    if [ "${ENV}" == "uat" ]; then
        export AWSPROFILE="UAT"
    fi
    if [ "${ENV}" == "prod" ]; then
        export AWSPROFILE="PROD"
    fi
fi

AWSPROFILEREGION="--profile ${AWSPROFILE} --region ${AWSREGION}"
AWSPROFILEREGION="--region ${AWSREGION}"
AWSREGIONPROFILE="${AWSPROFILEREGION}"
AWS_PROFILE_REGION="${AWSPROFILEREGION}"

export PLATFORMID="CRRI"  ### In case the Acct# belongs to a COLLECTION of Environments

export PROJECTNAME="FACTrial"
export PROJECTID="${PLATFORMID}-${PROJECTNAME}"

export EMAILADDRESS="sarma.seetamraju@nih.gov"
export CONTACT="${EMAILADDRESS}"


# aws sts get-caller-identity --query Account --output text ${AWSPROFILEREGION} >& /dev/null
export AWSACCOUNTID=$( aws sts get-caller-identity --query Account --output text ${AWSPROFILEREGION} )
if [ $? -ne 0 ]; then
   echo "!! ATTENTION !! PRE-REQUISITE: You MUST login successfully -- VIA CLI -- into CloudBoost."
   echo "!! ATTENTION !! Then .. .. SUCCESSFULLY run a TEST-command with _NO_ errors:->  aws s3 ls ${AWSPROFILEREGION}"
   exit 1
fi

### --------------------------------------------------------

echo "AWS_ACCOUNT_ID='${AWS_ACCOUNT_ID}'"
echo "AWSPROFILEREGION='${AWSPROFILEREGION}'"
sleep 2

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

BOUNDARYPOLICY_NAME="my-default-PermissionsBoundary"     ### <----------------------- !! SWITCH !! <--------------------------
BOUNDARYPOLICY="arn:aws:iam::${AWSACCOUNTID}:policy/${BOUNDARYPOLICY_NAME}"


###--------------------------------------------------------

export SHAREDS3BUCKET="generic-bucket-${AWSACCOUNTID}"
### Make sure to align this with CFT.yaml's MAPPING-§  that defines "SharedDevOpsS3Bucket".
SHAREDS3BUCKET_ROOT="s3://${SHAREDS3BUCKET}/devops"

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

export DateTimeStamp=$(date +"%Y-%m-%dT%H:%M")

TAGS_ARR=(
    PRODUCT="${PRODUCT}"
    VERSION="${VERSION}"
    application="${application}"
    component="${component}"
    env="${env}"
    ENVIRONMENT="${ENVIRONMENT}"
    branch="${branch}"
)

# echo ${TAGS_ARR[@]}

###------------------------------
TAGS=(
    Key="PRODUCT",Value="${PRODUCT}"
    Key="VERSION",Value="${VERSION}"
    Key="ENVIRONMENT",Value="${ENVIRONMENT}"
    Key="application",Value="${application}"
    Key="component",Value="${component}"
    Key="env",Value="${env}"
    Key="branch",Value="${branch}"
)

# echo ${TAGS[@]}

###--------------------------------------------------------

ENVIRONMENTS=( "dev" "int" "uat" )
ENV="dev"

### --------------------------------------------------------

### Common -TEMPORARY- Variables

OPENSSL_CONF="/etc/ssl/openssl.cnf"
if [ ! -f "${OPENSSL_CONF}" ]; then
    echo "ERROR: ${OPENSSL_CONF} does not exist."
    exit 91
fi
###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------
###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

### Define the temporary files (to save the output of JQ commands below)
###------------ SCRATCH-VARIABLES & FOLDERS ----------

# TMPROOTDIR="/tmp/${MyCompanyName}"
TMPROOTDIR="/tmp/devops"
if [ -z ${SCRIPTFOLDER+x}          ]; then SCRIPTFOLDER=$(dirname -- "$0");                   fi
if [ -z ${SCRIPTNAME+x}            ]; then SCRIPTNAME=$(basename -- "$0");                    fi
if [ -z ${SCRIPTFOLDER_FULLPATH+x} ]; then SCRIPTFOLDER_FULLPATH="$(pwd)/${SCRIPTFOLDER}";    fi
# echo "${SCRIPTFOLDER_FULLPATH}"
if [ -z ${TMPDIR+x}                ]; then TMPDIR="${TMPROOTDIR}/DevOps/${PROJECTID}/${SCRIPTNAME}"; fi

mkdir -p "${TMPROOTDIR}"
mkdir -p "${TMPDIR}"

touch ${TMPDIR}/junk ### To ensure rm commands (under noglob; see many lines below) always work.

TMPFILE11=${TMPDIR}/tmp1.txt
TMPFILE22=${TMPDIR}/tmp22.txt
TMPFILE333=${TMPDIR}/tmp333.txt

rm -rf "${TMPFILE11}" "${TMPFILE22}" "${TMPFILE333}"

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

### EoScript
