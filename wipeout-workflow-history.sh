#!/bin/bash -f

### Using "gh api" commands, DESTROY all the RUNS of EVERY workflow.

if [ $# -eq 0 ]; then
    echo "USAGE: must provide WORKFLOW-name's PREFIX as cli-arg !!"
    exit 0
fi

WORKFLOW_NAME_PREFIX="nccr-27"
WORKFLOW_NAME_PREFIX="$1"

###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------

PROJECT_HOME="$( cd ${SCRIPT_FOLDER}/..; pwd )"
# echo $PROJECT_HOME

###---------------------------------------------------------------

TMPROOTDIR="/tmp"
TMPDIR="${TMPROOTDIR}/DevOps/Github/${SCRIPT_NAME}"
TMPFILE11="${TMPDIR}/tmp1.txt"
TMPFILE22="${TMPDIR}/tmp2.txt"
ITEMS_FILE="${TMPDIR}/gh_wkflo_runs_to_delete.txt"

mkdir -p ${TMPROOTDIR}
mkdir -p ${TMPDIR}
rm -rf "${TMPFILE11}" "${TMPFILE22}" "${ITEMS_FILE}"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

#__ gh api --method GET -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
#__         repos/$YOUR_USER/$YOUR_REPO/actions/workflows

echo \
gh run --repo "${OWNER}/${REPONAME}" list --limit 1000 --json name,workflowName,displayTitle,databaseId,workflowDatabaseId,number,status,headBranch,startedAt,headSha  \
        --jq ".[] | select(.displayTitle | startswith(\"${WORKFLOW_NAME_PREFIX}\") ) "
gh run --repo "${OWNER}/${REPONAME}" list --limit 1000 --json name,workflowName,displayTitle,databaseId,workflowDatabaseId,number,status,headBranch,startedAt,headSha  \
        --jq ".[] | select(.displayTitle | startswith(\"${WORKFLOW_NAME_PREFIX}\") ) "          \
        > "${TMPFILE11}"

ls -la "${TMPFILE11}"
if [ ! -s "${TMPFILE11}" ]; then
    echo "No runs found for ${OWNER}/${REPONAME};  Quietly quitting."
    exit 0
else
    printf "\n# of Workflow-RUNS found = $( wc -l < ${TMPFILE11})\n\n"
fi

jq '.databaseId'        < "${TMPFILE11}"   | sort -g  > "${ITEMS_FILE}"
### OLD-stuff# awk -F '\t' '{print $7}'        < "${TMPFILE11}"    > "${ITEMS_FILE}"

ls -la  "${ITEMS_FILE}"
cat     "${ITEMS_FILE}"
read -p "Start deleting? >> " ANSWER;

### Copied from https://github.com/orgs/community/discussions/26256#discussioncomment-5854220
for run in $(cat "${ITEMS_FILE}" ); do
    echo "delete run $run";
    echo \
    gh api --method DELETE -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
        repos/${OWNER}/${REPONAME}/actions/runs/$run ;
        ### repos/${OWNER}/${REPONAME}/actions/workflows/${WorkflowID}/runs   <-- specific to ONE workflow.
    sleep 1;
    gh api --method DELETE -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
        repos/${OWNER}/${REPONAME}/actions/runs/$run ;
    #__ curl -L -X DELETE -H "Accept: application/vnd.github+json" -H "Authorization: Bearer $GH_TOKEN" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/$OWNER/$REPO/actions/runs/$run ;
done


### EoScript
