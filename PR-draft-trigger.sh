#!/bin/bash -f

if [ $# -eq 0 ]; then
    echo "USAGE: must provide PR-number as cli-arg !!"
    exit 0
fi
PULLNUMBER="$1"

###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

if [ "$1" == "--all" ]; then
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''
    sleep 60
    WIPE_ALL="true"
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### 1st .. Get status of the PR -- for debugging purposes.
### https://cli.github.com/manual/gh_pr_status

date

set -x
gh pr status --repo NCI-DCCPS/nccr-dataplatform --json headRefName,id,number,isDraft,state,mergeStateStatus,mergeable,closed,title | jq ".createdBy[] | select( .number == ${PULLNUMBER} )"
set +x

### https://cli.github.com/manual/gh_pr_ready

set -x
gh pr ready --repo ${OWNER}/${REPONAME}  "${PULLNUMBER}" --undo
set +x

### Does NOT work.
### https://cli.github.com/manual/gh_api   <--- how to send a BODY inline.
### https://github.com/orgs/community/discussions/45174
# echo \
# gh api --method PATCH -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
#     --raw-field body='{"state": "draft"}'                     \
#     "repos/${OWNER}/${REPONAME}/pulls/${PULLNUMBER}"          \
#     > "${TMPFILE11}"

### https://cli.github.com/manual/gh_pr_ready
set -x
sleep 5
gh pr ready --repo ${OWNER}/${REPONAME}  "${PULLNUMBER}"
set +x

date

### EoF
