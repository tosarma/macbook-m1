import sys
import boto3
import os
import pathlib
import json
import time
import regex
from typing import Sequence
from datetime import datetime, timedelta
import traceback

from generic_aws_cli_script import ( GenericAWSCLIScript )
from list_orphan_ENIs_for_SG import ( find_orphan_enis_for_sg )

APP_NAME = "nccr"
THIS_SCRIPT_DATA = "stacks"
DEBUG = False

ALL_STACK_STATUSES=[
    'CREATE_IN_PROGRESS', 'CREATE_FAILED', 'CREATE_COMPLETE', 'ROLLBACK_IN_PROGRESS', 'ROLLBACK_FAILED', 'ROLLBACK_COMPLETE', 'DELETE_IN_PROGRESS', 'DELETE_FAILED', 'DELETE_COMPLETE', 'UPDATE_IN_PROGRESS', 'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_COMPLETE', 'UPDATE_FAILED', 'UPDATE_ROLLBACK_IN_PROGRESS', 'UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE', 'REVIEW_IN_PROGRESS', 'IMPORT_IN_PROGRESS', 'IMPORT_COMPLETE', 'IMPORT_ROLLBACK_IN_PROGRESS', 'IMPORT_ROLLBACK_FAILED', 'IMPORT_ROLLBACK_COMPLETE',
]
ALL_ACTIVE_STACK_STATUSES = ALL_STACK_STATUSES.copy()
ALL_ACTIVE_STACK_STATUSES.remove('DELETE_COMPLETE')

class CleanupFailedStacks(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        stack_name_suffix :str,
        aws_profile :str,
        tier :str,
        purpose :str = THIS_SCRIPT_DATA,
        debug :bool = False,
    ) -> None:
        super().__init__(
            appl_name=appl_name,
            purpose=purpose,
            aws_profile=aws_profile,
            tier=tier,
            debug=debug,
        )

        # get list of stacks named f"{self.app_name}-{self.tier}-{}"
        if self.tier.startswith(self.appl_name):
            self.stack_name = f"{self.tier}-{stack_name_suffix}"
        else:
            self.stack_name = f"{self.appl_name}-{self.tier}-{stack_name_suffix}"
        if self.debug: print(self.stack_name)
        # stack_list = self.get_stack_list()

        stkname_regex = f"{self.stack_name}" ### exact match
        # stkname_regex = f"{self.stack_name}.{0,20}"

        ec2_client = self.session.client("ec2")

        stks_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 'cloudformation',
            api_method_name = "list_stacks",
            response_key = 'StackSummaries',
            json_output_filepath = self.json_output_filepath,
            additional_params={ "StackStatusFilter": ALL_ACTIVE_STACK_STATUSES },
            # additional_param_name="StackStatusFilter",
            # additional_param_value=ALL_ACTIVE_STACK_STATUSES,
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 1: print(stks_list)

        for stk in stks_list:
            stknm = stk['StackName']
            print( '.', end="", flush=True)
            if self.debug: print(stknm +'/'+ stk['StackStatus'], end=".. ", flush=True)
            if regex.match( pattern=stkname_regex, string=stknm ):
            # if stk['StackName'].startswith(self.stack_name):
                if stk['StackStatus'] == 'DELETE_IN_PROGRESS':
                    print(f"\n\n\n⚠️⚠️ 🛑 Found stack that is still BEING deleted: {stknm}\n\n")
                    time.sleep(15)
                    continue
                if stk['StackStatus'] == 'DELETE_FAILED':
                    if self.debug: print(f"Found stack that FAILED to delete: {stknm}")
                    cfn_client = self.session.client("cloudformation")
                    # cfn_client.delete_stack(stknm)
                    # list all the resources that are Not deleted in this stack
                    resources = cfn_client.list_stack_resources(StackName=stknm)['StackResourceSummaries']
                    for resource in resources:
                        sample_resource = {
                            "LogicalResourceId": "vpcsglambda0A8D6252",
                            "PhysicalResourceId": "sg-08f4678762dd7bace",
                            "ResourceType": "AWS::EC2::SecurityGroup",
                            "LastUpdatedTimestamp": "2024-10-01 16:13:21.666000+00:00",
                            "ResourceStatus": "DELETE_FAILED",
                            "ResourceStatusReason": "resource sg-08f4678762dd7bace has a dependent object (Service: Ec2, Status Code: 400, Request ID: 9af89c76-8e9d-4234-80bd-25ed0341672d)",
                            "DriftInformation": { "StackResourceDriftStatus": "NOT_CHECKED" }
                        }
                        if resource['ResourceStatus'] == 'DELETE_FAILED':
                            sg_id = resource['PhysicalResourceId']
                            print(f".. Resource '{sg_id}' in stack '{stknm}' is in state '{resource['ResourceStatus']}'")
                            if self.debug: print(json.dumps(resource, indent=4, default=str))
                            enis = find_orphan_enis_for_sg(
                                security_group_id=sg_id,
                                boto3_client=ec2_client
                            )
                            if self.debug:
                                print("Orphan ENIs tobe manually destroyed:")
                                print(json.dumps(enis, indent=4, default=str))
                                print("\n\n")
                            # delete each "eni" within the list "enis"
                            for eni in enis:
                                eni_id = eni['NetworkInterfaceId']
                                print(f"Deleting ENI {eni_id} ..", end="")
                                resp = ec2_client.delete_network_interface(NetworkInterfaceId=eni_id)
                                if debug: print(json.dumps(resp, indent=4, default=str))
                                print("Done")

### ####################################################################################################

# if invoked via cli
if __name__ == "__main__":
    if len(sys.argv) >= 4:
        aws_profile = sys.argv[1]
        tier = sys.argv[2]
        stack_name_suffix = sys.argv[3]
        scr = CleanupFailedStacks(
            appl_name=APP_NAME,
            stack_name_suffix=stack_name_suffix,
            aws_profile=aws_profile,
            tier=tier,
            purpose=THIS_SCRIPT_DATA,
            debug=DEBUG,
        )
    else:
        print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> <TIER> <STACK_NAME_SUFFIX>" )
        print( f"EXAMPLE: python {sys.argv[0]} NCCR-nonprod  1854  vpc" )

# EoScript
