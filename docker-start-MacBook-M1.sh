#!/bin/bash

if [ "$1" != "ShutUpAndRun" ]; then
    ### PROMPT the developer to ensure Docker-Desktop is __LATEST__ version.
    ### Then, start Docker-Desktop and _WAIT_ for it to FINISH-starting-UP.

    ### ----------------------------------------------------
    ### Important: Failure to __LOCALLY__ do a CodeBuild-Project's StartBuild on MacBook-M1 is PRIMARILY due-to OLD-VERSION of Docker-Desktop (on MacBook-M1) .. that is, is _NOT_ up-to-date.
    echo ''; echo -n '!!!!! Did you UPGRADE to _LATEST_ Docker-Desktop for MacBook-M1 ????'; echo " (FYI: Right now in $(basename -- $0))";
    ### linux? (Confirmed on MacOS):
    while read -r -t 1; do read -r -t 1; done ### "read -r -t0" will return true if stdin-stream is NOT empty
    read -p "Press <ENTER> .. .. to continue >> "

    echo ''; echo -n '!! Are you sure RE: _LATEST_ Docker-Desktop for MacBook-M1 ???? (in $0)'; echo " (FYI: Right now in $(basename -- $0))";
    ### linux? (Confirmed on MacOS):
    while read -r -t 1; do read -r -t 1; done ### "read -r -t0" will return true if stdin-stream is NOT empty
    read -p "Press <ENTER> .. .. to continue >> "
fi

if [ "$2" == "kill1st" ]; then
    killall "Docker Desktop";
    if [ $? -eq 0 ]; then sleep 15; fi; 
    ### Even after killing Docker, the kernel takes a long time to actually finish the processes!!!
fi

### ----------------------------------------------------
DOCKER_STARTED="false"
for i in {1..10}; do
    docker image ls -q >& /dev/null
    if [ $? -ne 0 ]; then
        DOCKER_STARTED="false"
        printf "(#%s) Docker is _NOT_ running !!" $i
        echo -n "So, __ATTEMPTING__ to start Docker on MacBook-M1 using cmd: " $i
        echo \
        open -a Docker
        open -a Docker
        sleep $i
    else
        DOCKER_STARTED="true"
        printf "(#%s) Docker is running !!" $i
        exit 0
    fi
done
if [ "${DOCKER_STARTED}" == "false" ]; then
    echo "ERROR: Docker is NOT running. Also, _FAILED_ to start it !!"
    exit 12
fi

### EoScript
