#!/bin/bash

### MILDLY MODIFED from https://gist.github.com/DPatrickBoyd/afb54165df0f51903be3f0edea77f9cb

### !!! ATTENTION !!!
# Change the date under `CUTOFF_DATE` to change how far back you want to delete.
# Install the GitHub CLI tool by following the instructions in the official documentation: https://cli.github.com/manual/installation
# Make sure you auth first to github with 'gh auth login'

CUTOFF_DATE="10" ### days ago.

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------

PROJECT_HOME="$( cd ${SCRIPT_FOLDER}/..; pwd )"
# echo $PROJECT_HOME

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

if [ $(uname -os) == "Darwin" ]; then
    echo "Running on a Mac"
    CUTOFF_DATE=$(date -v "-${CUTOFF_DATE}d" +'%Y-%m-%dT%H:%M:%SZ')
else
    echo "Running on Linux"
    CUTOFF_DATE=$(date --date="${CUTOFF_DATE} days ago" +'%Y-%m-%dT%H:%M:%SZ')
fi
printf "Going to destroy artifacts OLDER-THAN the Cutoff date='${CUTOFF_DATE}'\n\n"
read -p "Continue? >>" ANSWER

### ---------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### ---------------------------------------------------------------------

PAGE=1      ### initial value

while true; do
    printf '%.0s-' {1..80};  printf "\ngh api call --- page ${PAGE}\n .."
    echo \
    gh api --method GET -H \"Accept: application/vnd.github+json\" -H \"X-GitHub-Api-Version: 2022-11-28\" \
          repos/${OWNER}/${REPONAME}/actions/artifacts?per_page=100\&page=$PAGE
    gh api --method GET -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
          repos/${OWNER}/${REPONAME}/actions/artifacts?per_page=100\&page=$PAGE
    sleep 5
    # Retrieve a page of artifacts
    ART_EXIST=$(gh api --method GET -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
          repos/${OWNER}/${REPONAME}/actions/artifacts?per_page=100\&page=$PAGE | jq -r '.artifacts[]')
    # ART_EXIST=$(gh api repos/${OWNER}/${REPONAME}/actions/artifacts?per_page=100\&page=$PAGE | jq -r '.artifacts[]')

    ARTIFACTS=$(gh api --method GET -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
          repos/${OWNER}/${REPONAME}/actions/artifacts?per_page=100\&page=$PAGE | jq -r '.artifacts[] | select(.created_at < "'"$CUTOFF_DATE"'") | .id')
    # ARTIFACTS=$(gh api repos/${OWNER}/${REPONAME}/actions/artifacts?per_page=100\&page=$PAGE | jq -r '.artifacts[] | select(.created_at < "'"$CUTOFF_DATE"'") | .id')

    printf "# of artifacts older than CUTOFF-DATE is ${#ARTIFACTS}\n\n"

    # If there are no more artifacts, exit the loop
    if [[ -z "$ART_EXIST" ]]; then
      break
    fi

    # Loop through the artifacts on this page and delete the old ones
    for ARTIFACT_ID in $ARTIFACTS; do
      ARTIFACT_NAME=$(gh api repos/${OWNER}/${REPONAME}/actions/artifacts/$ARTIFACT_ID | jq -r '.name')
      echo "Deleting artifact $ARTIFACT_NAME (ID: $ARTIFACT_ID)..."
      read -p "Deleting artifact $ARTIFACT_NAME (ID: $ARTIFACT_ID)... >>" ANSWER
      gh api repos/${OWNER}/${REPONAME}/actions/artifacts/$ARTIFACT_ID -X DELETE
    done

    # Increment the page counter
    PAGE=$((PAGE+1))
done

### EoScript
