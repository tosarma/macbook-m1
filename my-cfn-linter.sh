#!/bin/bash -f

echo "Usage: $0 [--include-experimental]  [ --all | <file-pattern> ]    <optional:Project-Name>"

if [ "$1" == "--include-experimental" ]; then
    shift
    EXTRA_CLI_ARGS="--include-experimental"
else
    EXTRA_CLI_ARGS=""
fi

if [ $# -eq 0 ]; then
    FILES_PATT="${branch}*template.json"
else
    if [ "$1" == "--all" ]; then
        FILES_PATT="*template.json"
    else
        FILES_PATT="$1"
    fi
    shift
fi

if [ $# -ne 0 ]; then
    PROJECT_NAME="$1"
    echo ".. .. project name is '${PROJECT_NAME}'"
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Derived Variables

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

.  "${SCRIPT_FOLDER}/settings.sh"


if [ ! -z "${PROJECT_NAME+x}" ]; then
    KNOWN_PROJ_ITEMS=~/LocalDevelopment/etc/cfn-lint-ignore_${PROJECT_NAME}.txt
    echo "👉🏾👉🏾👉🏾👉🏾👉🏾👉🏾👉🏾 will compare with KNOWN warnings stored in ${KNOWN_PROJ_ITEMS}"
fi

EXTRA_CLI_ARGS="${EXTRA_CLI_ARGS} --ignore-checks W3005"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Sanity Checks

if [ ! -d "cdk.out" ]; then
    echo "ERROR: 'cdk.out' folder _NOT_ found in CWD =$(pwd) !!❌❌❌❌❌❌❌❌❌❌"
    exit 27
fi

###---------------------------------------------------------------

### Do section

echo ''; printf '%.0s=' {1..160}; echo ''; printf '%.0s=' {1..160}; echo ''; echo '';

### REF: https://github.com/aws-cloudformation/cfn-lint?tab=readme-ov-file#exit-codes
### `cfn-lint` command's Exit Codes
### cfn-lint will return a non zero exit if there are any issues with your template. The value is dependent on the severity of the issues found. For each level of discovered error cfn-lint will use bitwise OR to determine the final exit code. This will result in these possibilities.
###         0 is no issue was found
###         2 is an error
###         4 is a warning
###         6 is an error and a warning
###         8 is an informational
###         10 is an error and informational
###         12 is an warning and informational
###         14 is an error and a warning and an informational
###         Configuring Exit Codes

echo \
cfn-lint ${EXTRA_CLI_ARGS} --non-zero-exit-code 'error' "cdk.out/${FILES_PATT}"
cfn-lint ${EXTRA_CLI_ARGS} --non-zero-exit-code 'error' "cdk.out/${FILES_PATT}" > "${TMPFILE11}"
EXIT_STATUS=$?
grep -v "^W3005 Obsolete DependsOn on resource" "${TMPFILE11}" > "${TMPFILE22}"
if [ "${KNOWN_PROJ_ITEMS+x}" != "" ]; then
    branch="$(git symbolic-ref --short HEAD)"
    sed -e "s/{GITBRANCH}/${branch}/g" < "${KNOWN_PROJ_ITEMS}"  > ${TMPFILE11}
    ls -la "${TMPFILE11}"
    diff  "${TMPFILE11}"    "${TMPFILE22}"
else
    cat "${TMPFILE22}"
fi
if [ ${EXIT_STATUS} -ne 0 ]; then
    ls -la "${TMPFILE11}" "${TMPFILE22}"
    echo "ERROR: 'CloudFormation-Linter FAILED !!❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌"
    exit 13
else
    echo "SUCCESS: ✅ 'CloudFormation-Linter PASSED !!"
fi

### EoF
