#!/bin/bash -f

echo -n "Destroying all running Containers .. "; sleep 3
docker container kill $(docker container ls -q -a)
docker container rm -f $(docker container ls -q -a)

printf "\nDestroyed!!\n\n"
docker container ls -a
sleep 3

# docker image rm -f $(docker image ls -q -a)
echo -n "Destroying all running IMAGES .. "
docker image rm    --force $( docker image ls -q -a )
docker image prune --force --all
printf "\nDestroyed!!\n\n"

echo -n "Destroying all CACHED Build-stuff .. "
docker builder prune --force --all

echo -n "Destroying all running VOLUMES .. "
docker volume prune --force --all
printf "\nDestroyed!!\n\n"

echo -n "system-cleaning .. "
docker system prune --force --all
printf "\n done!!\n\n"

### EoF
