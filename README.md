# Running GENERIC python-based cli-utilities

1. Just in case you have an active `venv` on current-shell -- as a precaution -- open a new bash-shell!!!
    * This is important, so that you do NOT mess up your existing .venv !!!!!
2. Create a new `Pipfile`  ([See Example](#example-pipfile)
3. Run the following cmimands
    * Note: set the value of the bash-variable called `PYTHON_VERSION` in above cli-command
    * It should match the constant defined within `./constants_cdk.py`
7. 👉🏾👉🏾👉🏾 Finally, Verify there is a NEW file named `Pipfile.lock`

run each of the following commands --  AS IS !

```bash
PYTHON_VERSION="3.12"

\rm -rf ~/.cache/ ~/.local/ ~/.venv/ .cache/ .local/ .venv/ __pycache__/

pip install pipenv
pipenv lock --dev --python ${PYTHON_VERSION} --clear
pipenv sync --dev
```

<BR/><BR/><BR/><BR/>
<HR/><HR/><HR/><HR/>

# periodic activities to refresh

Important: Run the following commands at the TOPMOST folder of this project !!!!!!!!

```bash
pipenv sync --dev

TIER="dev"

export PYTHONPATH=${PWD}:${PYTHONPATH}
export PATH=${PWD}:$PATH
pipenv run python3 "${ScriptPath}" ${ScriptCLIArgs[@]}
```

<BR/><BR/><BR/><BR/>
<HR/><HR/><HR/><HR/>

# Wipe & re-install from Pipfile.Lock-locKK-locKK-locKK !!!


```bash
pipenv install --deploy --ignore-pipfile
                    ### --ignore-pipfile ==> Use `Pipfile.lock` and do -NOT- use `Pipfile`.




### !!! Stop using `venv` and plain `pip`
### python -m venv .venv
### source .venv/bin/activate
### pip install -r requirements.txt
```

<BR/><BR/><BR/><BR/>
<HR/><HR/><HR/><HR/>

# EXAMPLE Pipfile

```python
[[source]]
url = "https://pypi.org/simple"
verify_ssl = true
name = "pypi"

[packages]
attrs = "*"
# certifi
# chardet
dictdiffer = "*"
# greenlet
idna = "*"
importlib-metadata = "*"
jmespath = "*"
jsonschema = "*"

### must install both the binary and the non-binary psycopg
# psycopg==3.1.8  ### It's either this -OR- the binary-version.  -NOT- both!!!
# psycopg-binary==3.1.18

# psycopg2==2.9.9 ### It's either this -OR- the binary-version.  -NOT- both!!!
psycopg2-binary = "*"
# psycopg2-binary==2.9.9

python-dateutil = "*"
pytz = "*"

requests = "*"
# s3transfer
six = "*"
typing-extensions = "*"
urllib3 = "*"
# wget
# zipp
# jsonpickle

aws-xray-sdk = "*"
"aws-lambda-powertools[tracer]" = "*"

SQLAlchemy = "2.0.28"
# SQLAlchemy (2.0.36) ### pip-ERROR: poetry 1.5.1 requires urllib3<2.0.0,>=1.26.0, but you have urllib3 2.2.3 which is incompatible.

[dev-packages]

[requires]
python_version = "3.12"
```

/End