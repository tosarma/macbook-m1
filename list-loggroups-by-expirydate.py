### 1. List of all LOG-GROUPS in entire AWS-Account .. .. and then .. ..
### 2. Filter OUT those that are NEWER than `cutoff_date`
### 3. Count total # of log-groups (pre and post-filtered)
### 4. Count total # of log-groups (post-filtered) that are empty.
### 5. Count total # of log-groups that do ---NOT--- have a Retention-Expiry-Date
### 6. Count total # of log-groups that MATCH EACH regular-expression noted in `loggrp_patterns`

import sys
import boto3
import os
import pathlib
import json
import time
import regex
from datetime import datetime, timedelta

if len(sys.argv) != 3:
    print("Usage: python script.py <AWS_PROFILE> <AWS_ENV>")
    sys.exit(1)

aws_profile = sys.argv[1]
env         = sys.argv[2]

TMPDIR    = '/tmp'

### ----------------------------------------------------------------------
### Mnually configurable constants.

loggrps_older_than_in_days = 2 ### filter for all log-groups OLDER thah these many adays

appl_name = 'MyApp'

cache_no_older_than = 7     ### maximum _ days old before invoking SDK-APIs to refresh the json_output_filepath

logrgrp_patterns = [
    # regex.regex.Regex("/aws/lambda/.*"),
    regex.regex.Regex("/aws-glue/jobs/NCCR-DP.*"),
    regex.regex.Regex("/aws/lambda/.*BucketNotificationsHan.*"),
    regex.regex.Regex("/aws/lambda/.*CustomS3AutoDeleteObj.*"),
    regex.regex.Regex("/aws/lambda/nccr-.*-ident.*-AWS679f53fac002430cb0da.*"),
    regex.regex.Regex("/aws/lambda/nccr-.*-congito-AWS679f53fac002430cb0da.*"),
    regex.regex.Regex("/aws/lambda/nccr-.*-codebuild-CustomCDKBucketDeployment8693BB-.*"),
    regex.regex.Regex("/aws/codebuild/auto-testing-.*"),
    regex.regex.Regex("API-Gateway-Execution-Logs.*"),
    regex.regex.Regex("/aws-glue/jobs/nccrCommonStacknonprodgluesecurityconfiguration.*"),
]

### ----------------------------------------------------------------------
### Section: AWS SDK initialization (for API/SDK calls)

session = boto3.Session(profile_name=aws_profile)
# session = boto3.Session(profile_name=aws_profile, region_name=aws_region)
client = session.client('sts')
account_id = client.get_caller_identity()["Account"]
default_region = session.region_name
print(f"\nAWS Account ID: {account_id}")
print(f"Default AWS Region: {default_region}\n")

### ----------------------------------------------------------------------
### Section: Derived variables and constants

json_output_filepath = f"{TMPDIR}/{aws_profile}-{session.region_name}-all-loggroups.json"
json_output_filepath = pathlib.Path(json_output_filepath) ### convert a string into a Path object.

# Calculate the cutoff date for log group creation time
cutoff_date = datetime.now() - timedelta(days=loggrps_older_than_in_days)

### ----------------------------------------------------------------------
### Logic to --Cache-- the output of AWS-SDK API-calls (into temporary files)

if not json_output_filepath.exists():
    print(f"Cache is missing!! a.k.a. File '{json_output_filepath}' is missing!!")
    re_run_aws_sdk_call = True
else:
    # Check if the file was last modified over a week ago
    one_week_in_seconds = cache_no_older_than * 24 * 60 * 60  # 7 days
    file_modified_time = json_output_filepath.stat().st_mtime
    current_time = time.time()

    if current_time - file_modified_time > one_week_in_seconds:
        re_run_aws_sdk_call = True
        print(f"The CACHE/file '{json_output_filepath}' is too old by at least {cache_no_older_than} days !!! ")
    else:
        re_run_aws_sdk_call = False
        print(f"The CACHE/file '{json_output_filepath}' is still fresh enough.\n", '_'*80,"\n\n")

### ----------------------------------------------------------------------
### As necessary invoke AWS SDK API calls.

if re_run_aws_sdk_call:
    print("Invoking the massive AWS-SDK API for describe-log-groups and repeatedly retrieving all loggrp...")
    client = session.client('logs')
    all_log_groups = []
    next_token = None

    while True:
        if next_token:
            response = client.describe_log_groups(nextToken=next_token)
        else:
            response = client.describe_log_groups()

        all_log_groups.extend(response.get('logGroups', []))
        next_token = response.get('nextToken')
        # print(f"nextToken='{next_token}'")

        if not next_token:
            break

    # Prepare the final complete_response with all log groups
    complete_response = {'logGroups': all_log_groups}

    # Write the complete complete_response as JSON to the file
    with open(str(json_output_filepath), "w") as f:
        json.dump(complete_response, f, indent=4)

    print(f"Retrieved {len(all_log_groups)} log groups in total.")
else:
    # print(f"File {json_output_filepath} is present.\nSo .. using this cached AWS-SDK complete_response.. ..\n")
    # Use the cached complete_response
    with open(str(json_output_filepath)) as f:
        complete_response = json.load(f)

### ----------------------------------------------------------------------
### produce output for human consumption
total_rows = 0
total_filtered_rows = 0
total_never_expiring_logggrps = 0
total_empty_loggrps = 0
other_count = 0

no_expiry_loggrp_stats = {}
for patt in logrgrp_patterns:
    no_expiry_loggrp_stats[patt] = 0

empty_loggrp_stats = {}
for patt in logrgrp_patterns:
    empty_loggrp_stats[patt] = 0

nonempty_loggrp_stats = {}
for patt in logrgrp_patterns:
    nonempty_loggrp_stats[patt] = 0

# Iterate over the log groups & determine log-group's creationTime', 'storedBytes' attribute and the retention setting of that log-group
for log_group in complete_response['logGroups']:
    total_rows += 1
    loggrp_name = log_group['logGroupName']
    creation_time = log_group['creationTime']
    loggrpSizeInBytes = log_group.get('storedBytes')
    creation_datetime = datetime.fromtimestamp(creation_time / 1000)  # Convert milliseconds to datetime
    # print(f"{loggrp_name}: {loggrpSizeInBytes} {creation_datetime.astimezone()}")

    if loggrpSizeInBytes is None or loggrpSizeInBytes == 0:
        for patt in logrgrp_patterns:
            if patt.match(loggrp_name):
                empty_loggrp_stats[patt] += 1
        total_empty_loggrps += 1

    # Check if the log group is older than the cutoff date
    if creation_datetime < cutoff_date:
        # print(f"Stored Bytes: {loggrpSizeInBytes}")
        retention_days = log_group.get('retentionInDays')
        total_filtered_rows += 1
        for patt in logrgrp_patterns:
            if patt.match(loggrp_name):
                nonempty_loggrp_stats[patt] += 1

        if retention_days is None:
            retention_setting = 'NeverExpire'
            total_never_expiring_logggrps += 1
            found_pattern = False
            for patt in logrgrp_patterns:
                if patt.match(loggrp_name):
                    no_expiry_loggrp_stats[patt] += 1
                    found_pattern = True
                    # print(f"NEVER-EXPIRES!\t{patt.pattern}\t\t{loggrp_name}")
            if not found_pattern:
                print(f"NEVER-EXPIRES!\t\t\t{loggrp_name}")
                other_count += 1
        else:
            retention_setting = f'{retention_days}-days'
            # print(f"{loggrp_name}: {retention_setting} {loggrpSizeInBytes} {creation_datetime.astimezone()}")

    # print(f"{loggrp_name}: {loggrpSizeInBytes} {creation_datetime.astimezone()}")
    # print(f"{loggrp_name}: {retention_setting} {loggrpSizeInBytes} {creation_datetime.astimezone()}")
print("\nNOT-Empty LogGroups:\n", '_'*80, "\n")
for patt in nonempty_loggrp_stats:
    print(f"{patt.pattern}:\t\t{nonempty_loggrp_stats[patt]}")

print("\nNEVER_EXPIRING LogGroups:\n", '_'*80, "\n")
for patt in no_expiry_loggrp_stats:
    print(f"{patt.pattern}:\t\t{no_expiry_loggrp_stats[patt]}")

print("\nEMPTY LogGroups:\n", '_'*80, "\n")
for patt in empty_loggrp_stats:
    print(f"{patt.pattern}:\t\t{empty_loggrp_stats[patt]}")

print("\nSUMMARY COUNTS:\n", '_'*80, "\n")
print(f"# of LogGroups -WITHOUT- any Expiry in Retention: {total_never_expiring_logggrps}")
print(f"# of LogGroups -WITHOUT- any Expiry in Retention (Not one of the RegExp): {other_count}")
print(f"# of LogGroups older than {loggrps_older_than_in_days} days: {total_filtered_rows}")
print(f"# of Empty LogGroups: {total_empty_loggrps}")
print(f"All LogGroups: {total_rows}")

### EoScript
