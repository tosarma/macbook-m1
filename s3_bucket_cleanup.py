import sys
from typing import Optional
import time
import boto3
from datetime import datetime, timezone, timedelta
from botocore.exceptions import ClientError

from generic_aws_cli_script import ( GenericAWSCLIScript )
import json

APP_NAME = "FACTrial"
THIS_SCRIPT_DATA = "cdk-bucket-objs"
DEBUG = 5

CHUNK_SIZE = 100

"""
Delete objects and their versions from CDK assets bucket that are older than specified days

Args:
    bucket_name: Name of the CDK assets bucket
    older_than: Delete objects that are older than this many days; If Not specified, -ALL- objects will be wiped out.
"""
class DeleteOldCdkAssetsInsideCdkBucket(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        bucket_name :str,
        older_than: Optional[int],
        aws_profile :str,
        tier :str,
        purpose :str = THIS_SCRIPT_DATA,
        debug :bool = False,
    ) -> None:
        super().__init__(
            appl_name=appl_name,
            aws_profile=aws_profile,
            tier=tier,
            purpose=purpose,
            debug=debug,
        )

        if older_than < 0:
            raise ValueError("`older_than` must be a non-negative integer")

        cutoff_date = datetime.now(timezone.utc) - timedelta(days=older_than)
        if older_than:
            print(f"Deleting objects older than {older_than} days (before {cutoff_date})")
        else:
            print(f"Deleting ALL objects")
        print( f"cutoff_date='{cutoff_date}' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        self.s3_client = self.session.client('s3')

        ### -----------------
        s3_obj_ver_list :list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 's3',
            api_method_name = "list_object_versions",
            response_key = 'Versions',
            json_output_filepath = self.json_output_filepath,
            additional_params={
                "Bucket": bucket_name,
                "MaxKeys": 10000, ### By default, all s3-apis return up to 1,000 keys/items
            },
            # additional_params={ "?????StackStatusFilter": ALL_ACTIVE_STACK_STATUSES },
            # additional_param_name="StackStatusFilter",
            # additional_param_value=ALL_ACTIVE_STACK_STATUSES,
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 3: print(s3_obj_ver_list)

        ### Sort objects by LastModified timestamp (oldest first)
        s3_obj_ver_list = sorted(
            s3_obj_ver_list,
            key = lambda x: x.get('LastModified', datetime.min)
        )

        old_versions_2b_deleted = []
        for s3obj in s3_obj_ver_list:
            last_modified = s3obj['LastModified']
            if isinstance(last_modified, str):
                last_modified = datetime.fromisoformat(last_modified.replace('Z', '+00:00'))
            if self.debug > 4:
                print( f"Key={s3obj['Key']}, LastModified={last_modified}, bool={last_modified < cutoff_date}")
                # time.sleep(1)
            if older_than == None or last_modified < cutoff_date:
                old_versions_2b_deleted.append({
                    "Key": s3obj['Key'],
                    "VersionId": s3obj['VersionId'],
                    # "ETag": s3obj['ETag'].strip('"'), ### Wierd stuff in api-response.
                })

        if self.debug > 3 and old_versions_2b_deleted:
            ### print each entry of old_versions_2b_deleted, as a single-line json
            for entry in old_versions_2b_deleted:
                print(json.dumps(entry, default=str))

        deleted_count = self._delete_multiple_s3_items( bucket_name, old_versions_2b_deleted )

        print(f"Successfully deleted {deleted_count} (out of {len(s3_obj_ver_list)}) OLD-versions, given `older_than` param set to {older_than}")

        ### -----------------
        ### Repeat all of the above .. this time for response_key = `DeleteMarkers` instead of `Versions`
        ### DeleteMarkers should NEVER be filtered by Date. Just wipe them out!
        s3obj_delmrkr_list :list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 's3',
            api_method_name = "list_object_versions",
            response_key = 'DeleteMarkers',
            json_output_filepath = self.json_output_filepath,
            additional_params={ "Bucket": bucket_name },
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        del_mrks_api_input = []
        for s3obj in s3obj_delmrkr_list:
            del_mrks_api_input.append({
                "Key": s3obj['Key'],
                "VersionId": s3obj['VersionId'],
            })
        deleted_count = self._delete_multiple_s3_items( bucket_name, del_mrks_api_input )

        print(f"Successfully deleted {deleted_count} (out of {len(s3obj_delmrkr_list)}) DELETE-Markers, given `older_than` param set to {older_than}")

        ### -----------------
        ### Now list all REGULR objects in S2, and delete them.
        s3obj_list :list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 's3',
            api_method_name = "list_objects_v2",
            response_key = 'Contents',
            json_output_filepath = self.json_output_filepath,
            additional_params={
                "Bucket": bucket_name,
                "MaxKeys": 10000, ### By default, all s3-apis return up to 1,000 keys/items
            },
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        s3obj_list = sorted(
            s3obj_list,
            key = lambda x: x.get('LastModified', datetime.min)
        )
        if self.debug > 3 and s3obj_list:
            for entry in s3obj_list:
                print(json.dumps(entry, default=str))
        too_old_objs = []
        for s3obj in s3obj_list:
            last_modified = s3obj['LastModified']
            if isinstance(last_modified, str):
                last_modified = datetime.fromisoformat(last_modified.replace('Z', '+00:00'))
            if older_than == None or last_modified < cutoff_date:
                too_old_objs.append({ "Key": s3obj['Key'] })
        print( len(too_old_objs) )
        deleted_count = self._delete_multiple_s3_items( bucket_name, too_old_objs )

        print(f"Successfully deleted {deleted_count} (out of {len(s3obj_list)}) regular objects, given `older_than` param set to {older_than}")

    ### ------------------------------------------------------------------------------
    def _delete_multiple_s3_items( self,
        bucket_name :str,
        s3_item_list :list,
        chunk_size :int = CHUNK_SIZE,
    ) -> int:
        if s3_item_list == None or len(s3_item_list) <= 0:
            return 0

        deleted_count = 0
        ### Delete up to 1000 objects at a time (S3 limit)
        for chunk in [s3_item_list[i:i + chunk_size] for i in range(0, len(s3_item_list), chunk_size)]:
            print( '.', end="", flush=True )
            response = self.s3_client.delete_objects(
                Bucket=bucket_name,
                Delete={
                    "Objects": chunk,
                    "Quiet": False,
                }
            )
            # deleted_count += len(chunk)
            ### Verify deletions
            if 'Deleted' in response:
                deleted_count += len(response['Deleted'])
                if self.debug > 2:
                    for deleted in response['Deleted']:
                        print(f"Deleted {deleted['Key']} (version {deleted['VersionId']})")
            if 'Errors' in response:
                for error in response['Errors']:
                    print(f"Error deleting marker {error['Key']}: {error['Message']}")
                sys.exit(21)

        return deleted_count

#         # List all object versions
#         paginator = self.s3_client.get_paginator('list_object_versions')

#         deleted_count = 0
#         for page in paginator.paginate(Bucket=bucket_name):
#             # Handle non-delete-marker versions
#             if 'Versions' in page:
#                     old_versions_2b_deleted = [
#                         {'Key': obj['Key'], 'VersionId': obj['VersionId']}
#                         for obj in page['Versions']
#                         if obj['LastModified'] < cutoff_date
#                     ]

### ===========================================================================================================
### ...........................................................................................................
### ===========================================================================================================

def cleanup_s3_bucket( bucketname :str ) -> None:

    s3 = boto3.resource('s3')
    bucket = s3.Bucket( bucketname )
    bucket.object_versions.delete()


def delete_s3_bucket( bucketname :str ) -> None:

    s3 = boto3.resource('s3')
    bucket = s3.Bucket( bucketname )
    # if you want to delete the now-empty bucket as well, uncomment this line:
    bucket.delete()

### ===========================================================================================================
### ...........................................................................................................
### ===========================================================================================================

if __name__ == "__main__":
    if len (sys.argv) < 5:
        print( f"Usage: python {sys.argv[0]} <AWS_PROFILE> <TIER> <CDK-Bucketname> <choose-obj-older-than>")
        print( f"Example: python3 {sys.argv[0]} DEVINT dev  cdk-hnb659fds-assets-127516845550-us-east-1  30 " )
        sys.exit(1)
    aws_profile = sys.argv[1]
    tier = sys.argv[2]
    bucket_name=sys.argv[3]
    older_than=int(sys.argv[4])

    # BUCKETNAME = 'fact-frontend-int-frontendcloudfrontloggingbucket4-qsnvimpoocce'
    # cleanup_s3_bucket( bucketname = sys.argv[1] )
    # delete_s3_bucket( bucketname = sys.argv[1] )
    # delete_s3_bucket( bucketname = BUCKETNAME )
    DeleteOldCdkAssetsInsideCdkBucket(
        appl_name = APP_NAME,
        bucket_name = bucket_name,
        older_than = older_than,
        aws_profile=aws_profile,
        tier=tier,
        debug = DEBUG,
    )
