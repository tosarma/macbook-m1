#!/bin/bash -f

### This script READs files under ${PROJECT_HOME}/.github/workflows/
### The files which are READ (as input) are listed below.
### This script does _NOT_ generate any output files.

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

PROJECT_HOME="$( cd ${SCRIPT_FOLDER}/..; pwd )"
# echo $PROJECT_HOME

FLDR="${PROJECT_HOME}/.github/workflows"
FILES=( "variables-fixed.env" "variables.env" )
EXPRESSION_FILE="variables-expressions.env"

###---------------------------------------------------------------

TMPROOTDIR="/tmp"
TMPDIR="${TMPROOTDIR}/DevOps/Github/${SCRIPT_NAME}"
TMPFILE11="${TMPDIR}/tmp1.txt"
TMPFILE22="${TMPDIR}/tmp2.txt"
TMPFILE_FINAL="${TMPDIR}/tmp99.txt"

mkdir -p ${TMPROOTDIR}
mkdir -p ${TMPDIR}
rm -rf "${TMPFILE11}" "${TMPFILE22}" "${TMPFILE_FINAL}"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

echo '' > ${TMPFILE11}  ### Ensure file is empty and exists.
for FILE in ${FILES[@]}; do
    if [ -f "${FLDR}/${FILE}" ]; then
        echo ''; echo "converting ${FILE}.. "; sleep 1
        cat ${FLDR}/${FILE} | sed -e '/^[ 	]*#.*/d' | sed -e '/^[ 	]*$/d'  | sed -e 's/:[ 	]*/="/' | sed -e 's/[ 	]*$/"/'>> ${TMPFILE11}
    else
        echo "File: ${FLDR}/${FILE} does not exist."
        exit 9
    fi
done
echo ''

###---------------------------------------------------------------

sed -e 's/^/export /' < ${TMPFILE11} > ${TMPFILE22}

. ${TMPFILE22}

envsubst < "${FLDR}/${EXPRESSION_FILE}" > "${TMPFILE_FINAL}"

### !!! ATTENTION !!! Does _NOT_ work.  Cuz, in 1st loop/iteration all UNDEFINED vars are replaced with BLANKS!!!!!
# TMPFILE33="${FLDR}/${EXPRESSION_FILE}"
# echo 'starting loop'
# while [ $? -eq 0 ]; do
#     ### export $(cat ${TMPFILE11} | xargs)  ### <--- This does _NOT_ work when values have BLANKS in it (typical SHELL issue).
#     echo "Sleeping for 1 second .."; sleep 1
#     envsubst < "${TMPFILE33}" > "${TMPFILE_FINAL}"
#     ls -la ${TMPFILE_FINAL}
#     cp -p "${TMPFILE_FINAL}" "${TMPFILE33}"
#     grep '${' ${TMPFILE33}
# done

###---------------------------------------------------------------
###---------------------------------------------------------------

printf "%.s-" {1..100}; echo ''; echo "Running gh cli commands .."; sleep 2

for FILE in ${FILES[@]}; do
    # echo ''; echo "${FILE}.. "
    echo \
    gh variable set --env "${ENV}" --repo "${OWNER}/${REPONAME}"    --env-file "${FLDR}/${FILE}"
    sleep 1
    gh variable set --env "${ENV}" --repo "${OWNER}/${REPONAME}"    --env-file "${FLDR}/${FILE}"
    gh variable set                --repo "${OWNER}/${REPONAME}"    --env-file "${FLDR}/${FILE}"
    ### Note: specify EITHER --org OR --repo, but NOT both.
done

echo "${EXPRESSION_FILE} (with expressions substituted by envsubst cmd)"; sleep 2
gh variable set --env "${ENV}" --repo "${OWNER}/${REPONAME}"     --env-file ${TMPFILE_FINAL}
gh variable set                --repo "${OWNER}/${REPONAME}"     --env-file ${TMPFILE_FINAL}
    ### Note: specify EITHER --org OR --repo, but NOT both.

###---------------------------------------------------------------
###---------------------------------------------------------------

### EoScript
