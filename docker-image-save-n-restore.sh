#!/bin/bash -f

### ----------------------------------------------------
if [ $# -ne 1 ]; then
    echo "USAGE: $0 (save|restore)"
    exit 1
fi

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### Section: Static Settings

AMAZON_LINUX_X86_VER="5.0"
AMAZON_LINUX_AARCH64_VER="3.0"

IMAGES=(
    public.ecr.aws/codebuild/amazonlinux2-x86_64-standard:${AMAZON_LINUX_X86_VER}
    public.ecr.aws/codebuild/amazonlinux2-aarch64-standard:${AMAZON_LINUX_AARCH64_VER}
    public.ecr.aws/codebuild/local-builds:aarch64
    public.ecr.aws/codebuild/local-builds:latest
)

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### Section: derived settings

if [ "$1" == "save" ]; then
    CMD="save"
else
    CMD="restore"
fi

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### 'do' section

for IMAGE in ${IMAGES[@]}; do
    # echo "${CMD} IMAGE: ${IMAGE} .."
    SAVED_IMAGE_FILENAME=$( echo "${IMAGE}.tar" | tr '[/\\:]' '_' )
    if [ "$1" == "save" ]; then
        echo \
        docker save --output "${SAVED_IMAGE_FILENAME}" "${IMAGE}"
    fi
    if [ "$1" == "restore" ]; then
        echo \
        docker import        "${SAVED_IMAGE_FILENAME}" "${IMAGE}"
    fi
done

### EoF
