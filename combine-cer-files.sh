#!/bin/bash -f

if [ $# -ne 0 ]; then
    printf "\n!! ERROR !! NO  CLI-arg allowed !\n"
    # printf "Usage:   $0  \n\n"
    exit 1
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

CERTS_FOLDER="${SCRIPT_FOLDER}/../bento-frontend/etc"
CERTS_FOLDER=$(cd ${CERTS_FOLDER}; pwd)
echo "CERTS_FOLDER = '${CERTS_FOLDER}'"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

TMPROOTDIR="/tmp"
TMPDIR="${TMPROOTDIR}/DevOps/Github/${SCRIPT_NAME}"
TMPFILE11="${TMPDIR}/tmp1.txt"

mkdir -p ${TMPROOTDIR}
mkdir -p ${TMPDIR}
rm -rf "${TMPFILE11}"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

printf "%.0s-" {1..80}
printf "\nCombining multiple *.cer files into one\n"

echo '' > "${TMPFILE11}"                  ### Empty out the file (if it exists)

set +o noglob
ls -la ${CERTS_FOLDER}/*.cer;

FILES_IN_PROPER_SEQ=(	sni.cloudflaressl.com.cer		nodejs.org.cer
		Cloudflare-ECC-CA-3.cer 		Baltimore-CyberTrust-Root.cer
		NIH-DPKI-NS-SSLCA-1A.cer		NIH-DPKI-ROOT-1A.cer
		cacert-popular.pem
    )

for f in ${FILES_IN_PROPER_SEQ[@]}; do
    CERTFILE="${CERTS_FOLDER}/${f}"
	if [ ! -f "${CERTFILE}" ]; then
        echo "ERROR: File '${CERTFILE}' does not exist !"
        exit 9
    fi
    echo "appending ${CERTFILE} ..";
    cat "${CERTFILE}" >> "${TMPFILE11}"
    echo '' >> "${TMPFILE11}"
done

printf "%.0s-" {1..80}; printf "\n\n"

ls -la "${TMPFILE11}"

### EoScript
