#!/bin/false

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

###------ reusable FUNCTION: wait for ANY Stack-Creation to be done -------
waitForCompleteCreationOfStack() {
   ARG_STACKNAME="$1"
   if [ -z ${AWSPROFILEREGION+x} ]; then
       echo "AWSPROFILEREGION variable is NOT defined: It should be = --profile X --region Y"
   fi
   echo -n "waitForCompleteCreationOfStack(): Waiting for complete-creation of ${ARG_STACKNAME} .. .. "
   STARTTIME=$(date +%s)
   while [ 1 -eq 1 ]; do
       sleep 10;
       aws cloudformation describe-stacks --stack-name ${ARG_STACKNAME} ${AWSPROFILEREGION} >& /dev/null
       if [ $? -ne 0 ]; then
           echo -n "."
       else
           STACKSTATUS=$( aws cloudformation describe-stacks --stack-name ${ARG_STACKNAME} ${AWSPROFILEREGION} --output json  | jq '.Stacks[].StackStatus' --raw-output )
           # echo -n "STACKSTATUS=${STACKSTATUS}"
           if [ "${STACKSTATUS}" == "CREATE_COMPLETE" ] || [ "${STACKSTATUS}" == "ROLLBACK_COMPLETE" ] || [ "${STACKSTATUS}" == "UPDATE_ROLLBACK_COMPLETE" ]; then
               echo ' [Breaking waiting-loop] CFT is successfully created completely!'
               break
           else
               if [ "${STACKSTATUS}" == "UPDATE_COMPLETE" ] || [ "${STACKSTATUS}" == "UPDATE_ROLLBACK_COMPLETE" ]; then
                   echo ''; echo ''; echo ' [Breaking waiting-loop] CFT is in UNEXPECTED STATE !!'; echo ''; echo ''
                   read -p "! Ignore the issue.. and CONTINUE anyway (Yy) ? [Default: No]>> " RESPONSE
                   if [ "${RESPONSE}" == "Y" ] || [ "${RESPONSE}" == "y" ]; then
                       return 81
                   else
                       exit 81
                   fi
               else
                   if [ "${STACKSTATUS}" == "CREATE_FAILED" ]; then
                       echo ''; echo ''; echo ' [Breaking waiting-loop]  .. CFT  FAILED !!'; echo ''; echo ''
                       exit 85
                   else
                       echo -n '▶'
                   fi
               fi
           fi
       fi
   done
   ENDTIME=$(date +%s)
   echo " .. done in $(( (ENDTIME - STARTTIME) / 60 )) minutes"
}
###------ End of Function -----

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

###------ reusable FUNCTION: wait for Stack-DELETION to be done -------
waitForCompleteDeletionOfStack() {
   ARG_STACKNAME="$1"
   if [ -z ${AWSPROFILEREGION+x} ]; then
       echo "AWSPROFILEREGION variable is NOT defined: It should be = --profile X --region Y"
   fi
   echo -n "waitForCompleteDeletionOfStack(): Waiting for complete-DELETION of ${ARG_STACKNAME} .. .. "
   STARTTIME=$(date +%s)
   while [ 1 -eq 1 ]; do
       sleep 10;
       aws cloudformation describe-stacks --stack-name ${ARG_STACKNAME} ${AWSPROFILEREGION} >& /dev/null
       if [ $? -ne 0 ]; then
           break
       else
           echo -n "."
       fi
   done
   ENDTIME=$(date +%s)
   echo " .. done in $(( (ENDTIME - STARTTIME) / 60 )) minutes"
}
###------ End of Function -----

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

###------ reusable FUNCTION: wait for a SPECIFIC Resource in a Stack to be CREATED -------
waitForStackResourceCreation() {
   ARG_STACKNAME="$1"
   ARG_RESOURCEID="$2"
   echo \
   aws cloudformation describe-stack-resource --stack-name "${ARG_STACKNAME}" --logical-resource-id "${ARG_RESOURCEID}" \
               ${AWSREGIONPROFILE} --output json
   # aws eks describe-cluster --name "${ClusterName}" ${AWSREGIONPROFILE}
   # aws ec2 describe-launch-templates --launch-template-name "${LAUNCHTEMPLATE}" ${AWSREGIONPROFILE}

   while true; do
       aws cloudformation describe-stack-resource --stack-name "${ARG_STACKNAME}" --logical-resource-id "${ARG_RESOURCEID}" \
               ${AWSREGIONPROFILE} --output json >& ${TMPFILE11}
       if [ $? -eq 254 ]; then
           echo -n '.'; sleep 30
       else
           ### Stack is _NOW_ visible on AWS CloudFormation-Console.
           STATUSofResource=$( jq '.StackResourceDetail.ResourceStatus' < "${TMPFILE11}" --raw-output )
           # echo -n " STATUSofResource=${STATUSofResource} "
           if [ $? -ne 0 ] || [ "${STATUSofResource}" == "CREATE_FAILED" ] || [ "${STATUSofResource}" == "DELETE_COMPLETE" ] || [ "${STATUSofResource}" == "DELETE_IN_PROGRESS" ]; then
               echo "!! INTERNAL ERROR !! See '${TMPFILE11}';  unable to detect STATUS-of-Stack via '.StackResourceDetail.ResourceStatus' "
               exit 199
           fi
           ### Even for a NULL-Stack, since Client's Pipeline will create and UPDATE the stack MULTIPLE-times before 100% completion .. we _MUST_ check for "UPDATE-COMPLETE" also.
           if [ "${STATUSofResource}" == "CREATE_COMPLETE" ] || [ "${STATUSofResource}" == "UPDATE_COMPLETE" ]; then
               echo "Breaking loop .."
               break;
           fi
           echo -n '▶'; sleep 5
       fi
   done
}
###------ End of Function -----

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### EoScript

