#!/bin/bash -f

### Load settings / configuration - specifc to each Project/Github Org.
### Then, Iterate over each ENVIRONMENT.. ..
###     DESTROY all the GitHub-Workflows VARIABLES -- for each ENVIRONMENT.
###     DESTROY all the GitHub           SECRETs   -- for each ENVIRONMENT.
###     Following which, DESTROY _EACH_ GitHub-Workflows ENVIRONMENT itself.
### Then, DESTROY the GitHub-Workflows PROJECT.

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

if [ "$1" == "--all" ]; then
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''
    sleep 60
    WIPE_ALL="true"
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 2
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 2
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

for e in "${ENVIRONMENTS[@]}"; do

    if [ "${WIPE_ALL}" != "true" ] && [ "${e}" != "${ENV}" ]; then
        echo ''; echo "Skipping the GitHub-ENVIRONMENT '${e}'"
        continue
    fi

    ### -------- Cache (Java & NodeJS) --------
    gh cache delete --all

    ### -------- ALL variables (whether fixed/constant, or expression) --------
    echo ""; echo "Deleting FIXED-value VARIABLES"; sleep 2
    VARIABLES=( $( gh variable list --env "${ENV}" --repo "${OWNER}/${REPONAME}" | tail +1 | cut -f1 ) )

    for VARNAME in "${VARIABLES[@]}"; do
        echo -n \
        gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
        gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
            ### Note: specify EITHER --org OR --repo, but NOT both.
    done

    # ### -------- variables-fixed.env --------
    # ### Note: Specify EITHER --org OR --repo, but NOT both.

    # echo ""; echo "Deleting FIXED-value VARIABLES"; sleep 2
    # VARIABLES=( PROJECT FILES_VERSION URL_SRC AUTH_ENABLED REACT_APP_AUTH PUBLIC_ACCESS
    #             GRAPHQL_SCHEMA GRAPHQL_ES_SCHEMA GRAPHQL_PUBLIC_SCHEMA GRAPHQL_PUBLIC_ES_SCHEMA
    #             NODE_LEVEL_ACCESS NODE_LABEL REDIS_ENABLE REDIS_USE_CLUSTER REDIS_HOST REDIS_FILTER_ENABLE
    #             ES_JAVA_OPTS network_bind_host network_host discovery_type plugins_security_disabled
    #         )

    # for VARNAME in "${VARIABLES[@]}"; do
    #     echo -n \
    #     gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
    #     gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
    #         ### Note: specify EITHER --org OR --repo, but NOT both.
    # done

    # ### -------- variables.env --------
    # echo ''; echo "Deleting project-specific VARIABLES"; sleep 2
    # VARIABLES=( BENTO_API_VERSION BENTO_DATA_MODEL BACKEND_BRANCH FRONTEND_BRANCH FILES_BRANCH
    #             BUILD_MODE FRONTEND_SOURCE_FOLDER BACKEND_SOURCE_FOLDER
    #             BACKEND_HOST FILES_HOST
    #             NEO4J_USER NEO4J_PASS NEO4J_HOST
    #             ES_HOST ES_PORT
    #         )

    # for VARNAME in "${VARIABLES[@]}"; do
    #     echo -n \
    #     gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
    #     gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
    #         ### Note: specify EITHER --org OR --repo, but NOT both.
    # done

    # ### -------- variables-EXPRESSIONS.env --------
    # echo ''; echo "Deleting DERIVED (expression-based) VARIABLES"; sleep 2
    # VARIABLES=( BACKEND_URL
    #             NEO4J_PASSWORD NEO4J_URI NEO4J_URL AUTH_ENDPOINT
    #             REACT_APP_BACKEND_API REACT_APP_BACKEND_PUBLIC_API REACT_APP_FILE_SERVICE_API
    #             REACT_APP_ABOUT_CONTENT_URL REACT_APP_BE_VERSION REACT_APP_FE_VERSION REACT_APP_AUTH_SERVICE_API REACT_APP_USER_SERVICE_API
    #         )

    # for VARNAME in "${VARIABLES[@]}"; do
    #     echo -n \
    #     gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
    #     gh variable delete --env "${e}" --repo "${OWNER}/${REPONAME}" 	"${VARNAME}"
    #         ### Note: specify EITHER --org OR --repo, but NOT both.
    # done

    ### -------- SECRETS --------
    echo ''; echo "Deleting SECRETS"; sleep 2
    SECRETS=(
            "NEO4J_PASSWORD"
        )

    for SECRET in ${SECRETS[@]}; do
        echo -n \
        gh secret delete --env "${e}" --app actions - "${SECRET}"
        gh secret delete --env "${e}" --app actions - "${SECRET}"
            ### Note: specify EITHER --org OR --repo, but NOT both.
    done


    ###------------------------ ENVIRONMENTS -------------------------
    ###---------------------------------------------------------------
    echo ''; echo "Deleting the GitHub-ENVIRONMENT '${e}': repos/${OWNER}/${REPONAME}/environments/${e}"
    gh api \
        --method DELETE \
        -H "Accept: application/vnd.github+json"  \
        -H "X-GitHub-Api-Version: 2022-11-28"     \
        "repos/${OWNER}/${REPONAME}/environments/${e}"
        #__ "repos/cbiit/iodc-bento-sandbox/environments/${e}"
    ### REF: https://docs.github.com/en/rest/deployments/environments?apiVersion=2022-11-28#delete-an-environment
    ### REF: ALL APIs: https://docs.github.com/en/rest/quickstart?apiVersion=2022-11-28

done ### FOR loop above (looping over ENVIRONMENTS shell-variable)

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

echo "Deleting the GitHub-Workflows PROJECT"
gh project delete --owner sarma-nci-nih-essex  "${PROJECTID}"   ### No ORG needed as CLI-arg

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

# EoScript
