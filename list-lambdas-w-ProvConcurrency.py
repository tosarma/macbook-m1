### 1. List of all Lambdas in entire AWS-Account .. .. and then .. ..
### 2. Filter OUT those that are .. do NOT belong to the App or allowed-TIERS.
### 3. Count total # of Lambdas (pre and post-filtered)
### 5. Dump the Lambdas with Provisioned-Concurrency

import sys
import boto3
import os
import pathlib
import json
import time
import regex
from datetime import datetime, timedelta
import traceback

from aws_api_invoker import (
    InvokeAWSApi,
    MyException,
)

if len(sys.argv) != 4:
    print(f"Usage: python {sys.argv[0]} <CDK_APP_NAME> <AWS_PROFILE> <TIER>")
    print(f"Example: python {sys.argv[0]} FACT  DEVINT   dev")
    sys.exit(1)

cdk_app_name = sys.argv[1]
aws_profile  = sys.argv[2]
tier         = sys.argv[3]

### ----------------------------------------------------------------------
### Mnually configurable constants.

loggrps_older_than_in_days = 2 ### filter for all log-groups OLDER thah these many adays

appl_name = 'FACT'

cache_no_older_than = 7     ### maximum _ days old before invoking SDK-APIs to refresh the json_output_filepath_roles

TAG_NAMED_TIER = 'TIER'
TAG_NAMED_ENV = 'ENVIRONMENT'
all_tiers = [
    "dev",
    "test",
    "int",
    "uat",
    "stage",
    "prod",
]
all_tiers_regex = [
    regex.regex.Regex("dev"),
    regex.regex.Regex("test"),
    regex.regex.Regex("int"),
    regex.regex.Regex("uat"),
    regex.regex.Regex("stage"),
    regex.regex.Regex("prod"),
    # regex.regex.Regex("nccr-[a-z0-9A-Z-]+"),
]
lambdaname_extract_regex = regex.regex.Regex(f"{cdk_app_name}-[a-z0-9A-Z-]+-[a-zA-Z]+-(.*)")

lambdas_to_ignore = [
    "BucketNotificationsH",
    "LogRetention",
]

DEBUG = False

TMPDIR    = '/tmp'

### ----------------------------------------------------------------------
### Section: Derived variables and constants

json_output_filepath_roles = f"{TMPDIR}/all-lambdas-{aws_profile}-{tier}.json"
json_output_filepath_roles = pathlib.Path(json_output_filepath_roles) ### convert a string into a Path object.

### ----------------------------------------------------------------------
awsapi_invoker = InvokeAWSApi(aws_profile=aws_profile, debug=DEBUG)

session = awsapi_invoker.sanity_check_awsprofile()

client = session.client('iam')

### ----------------------------------------------------------------------
### iam-P O L I C I E S

print("Loading Cache for Full-Comprehensive-Info re: All-Lambdas.. ..", end="")

complete_response_all_lambdas = awsapi_invoker.get_all_lambdas_full_details(
    json_output_filepath=json_output_filepath_roles,
    cache_no_older_than=cache_no_older_than,
)
print(".. Done")

total_rows = 0
total_filtered_rows = 0
other_count = 0

lambdas_w_prov_concurrency = {}
for tiername in all_tiers:
    lambdas_w_prov_concurrency[tiername] = 0

# Iterate over the IAM-POLICIES
for alambda in complete_response_all_lambdas:
    total_rows += 1
    lambda_name = alambda['FunctionName']
    last_modified_datetime = alambda['LastModified']
    policy_arn = alambda['FunctionArn']
    mem = alambda['MemorySize']
    timeout = alambda['Timeout']
    cpu_arch = alambda['Architectures']
    # layers = alambda['Layers'] ### [ { "Arn": "arn:aws:lambda:us-east-1:580247275435:layer:LambdaInsightsExtension:12", "CodeSize": 4351068 } .. .. ]

    if DEBUG: print(f"{lambda_name}:\t{last_modified_datetime}\t{policy_arn}")
    if DEBUG > 4: print(alambda)
    tier = None
    if 'Tags' in alambda:
        if TAG_NAMED_TIER in alambda['Tags']:
            tier = alambda['Tags'][TAG_NAMED_TIER]
        elif TAG_NAMED_ENV in alambda['Tags']:
            tier = alambda['Tags'][TAG_NAMED_ENV]
    if not tier:
        if DEBUG: print(f"\n\n!! WARNING !! {lambda_name} does NOT have a Tag defining the {TAG_NAMED_TIER}/{TAG_NAMED_ENV} !!\n\n")

    bFound = False
    if not "Tags" in alambda:
        if DEBUG: print(f"\n\n!! WARNING ⚠️⚠️ !! Lambda '{lambda_name}' does NOT have a Tags !!\n\n")
    else:
        patt :regex = None
        for patt in all_tiers_regex:
            if tier and patt.match(tier):
                total_filtered_rows += 1
                bFound = True
                break

    # if not bFound:
    #     continue

    # filter
    # if not is_proj_iam_Policy:
    #     continue
    #         is_proj_iam_Policy = True

    if match := lambdaname_extract_regex.match(lambda_name):
        simple_lambda_name = match.group(1)
        if DEBUG: print(f"Extracted simple-lambda_name '{simple_lambda_name}'")
    else:
        if DEBUG: print(f"Skipping '{lambda_name}' .. ", end="")
        other_count += 1
        # continue

    if lambda_name.startswith(appl_name):
        print(f"{lambda_name}:\t\t{mem}MB\t{timeout}s!")
    if "ProvisionedConcurrency" in alambda and alambda['ProvisionedConcurrency'] != None:
        lambdas_w_prov_concurrency[tier] += 1
        print(f"PROVISIONED-CONCURRENCY for {lambda_name} Found!")
        print(json.dumps(alambda['ProvisionedConcurrency'],indent=4))

    # print(f"{iamPolicy_name}: {retention_setting} {loggrpSizeInBytes} {last_modified_datetime.astimezone()}")


print("\nTIER-specific Lambdas:\n", '_'*80, "\n")
for tiername in all_tiers:
    print(f"{tiername}:\t\t{lambdas_w_prov_concurrency[tiername]}")

print("\nSUMMARY COUNTS:\n", '_'*80, "\n")
print(f"# of Lambdas (Not one of the RegExp): {total_rows}")
print(f"# of Lambdas (NOT FACTrial): {other_count}")
print(f"# of TIER-Specific Lambdas (based on Tags): {total_filtered_rows}\n\n")

# for lambda_name, policies in lambda_specific_policies_inline.items():
#     print('.'*20, end=""); print( lambda_name, end="" ); print('.'*20);
#             print(f"\t{policy}")

### EoScript
