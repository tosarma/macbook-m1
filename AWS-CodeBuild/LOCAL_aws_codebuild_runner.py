LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import os
import sys
import stat
import time
import pathlib
import argparse     ### https://docs.python.org/3/library/argparse.html

import docker       ### https://docker-py.readthedocs.io/en/stable/

ALL_FILE_MODES = {
    33188: '-rw-r--r--',
    33252: '-rwxr--r--',
    33261: '-rwxr-xr-x',
    33279: '-rwxrwxrwx',
}

def linux_cmd_ls( file ):
    file_path = pathlib.Path( file )
    file_info = file_path.stat()
    rwx = ALL_FILE_MODES.get( file_info.st_mode, f'unknown-file-mode: {file_info.st_mode}' )
    return f'{file_path.name}: Mode: {rwx}, Size: {file_info.st_size} bytes, Last Modified: {time.ctime(file_info.st_mtime)}'

### -----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------------------------------------------

class MyScript:

    @property
    def argv(self):
        return self.__argv

    @property
    def parsed_args(self):
        return self.__parsed_args

    def __init__(self, _argv):
        self.__argv = _argv

        self.main()

    ### -------------------------------------------------------------------------------------------
    def basicChecks(self):

        if os.environ.get('BUILDPLATFORM') is None:
            print( "!! Attention !! BUILDPLATFORM is NOT defined. setting it to defaults." )
            os.environ['BUILDPLATFORM'] = 'linux/aarch64'
            time.sleep(5)

        if os.environ.get('DOCKER_DEFAULT_PLATFORM') is None:
            os.environ['DOCKER_DEFAULT_PLATFORM'] = os.environ['BUILDPLATFORM']

        if os.environ.get('TARGETPLATFORM') is None:
            os.environ['TARGETPLATFORM'] = os.environ['BUILDPLATFORM']

        for key, value in os.environ.items():
            if 'PLATFORM' in key:
                print(f'{key}: {value}')

    ### -------------------------------------------------------------------------------------------
    def deriveVariables(self):

        self.SCRIPTFOLDER = pathlib.Path(__file__).parent.absolute()
        self.SCRIPTNAME = pathlib.Path(__file__).name
        self.CWD = pathlib.Path.cwd()

        if self.parsed_args.platform == "AMAZONLINUX2":
            ### ARM/AARCH64: AWS_CODEBUILD_RUNTIME_CONTAINER_IMG=f"public.ecr.aws/aws/codebuild/amazonlinux2-aarch64-standard:{self.AMAZON_LINUX_VER}";
            if os.environ['BUILDPLATFORM'] == "linux/amd64" or os.environ['BUILDPLATFORM'] == "linux/x86_64":
                ### https://gallery.ecr.aws/codebuild/amazonlinux2-x86_64-standard
                # AMAZON_LINUX_VER="4.0";       ### Amazon Linux 2 - older-version.
                self.AMAZON_LINUX_VER="5.0";         ### Amazon Linux 2 - latest;  Incl. Amazon-Linux-2023.
                self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG=f"public.ecr.aws/codebuild/amazonlinux2-x86_64-standard:{self.AMAZON_LINUX_VER}"
            if os.environ['BUILDPLATFORM'] == "linux/aarch64":
                ### https://gallery.ecr.aws/codebuild/amazonlinux2-aarch64-standard
                self.AMAZON_LINUX_VER="3.0";         ### Amazon Linux 2 - latest;  Incl. Amazon-Linux-2023.
                self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG=f"public.ecr.aws/codebuild/amazonlinux2-aarch64-standard:{self.AMAZON_LINUX_VER}"

        if self.parsed_args.platform == "UBUNTU":
            if os.environ['BUILDPLATFORM'] == "linux/amd64" or os.environ['BUILDPLATFORM'] == "linux/x86_64":
                # UBUNTU_LINUX_VER="4.0";         ### ubuntu = aws/codebuild/standard:5.0 for Ubuntu 20.04
                self.UBUNTU_LINUX_VER="5.0";         ### ubuntu = aws/codebuild/standard:5.0 for Ubuntu 22.04
                self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG=f"aws/codebuild/standard:{self.UBUNTU_LINUX_VER}"
            else:
                print( "!! ERROR !! UBUNTU O/S only supported for amd64 a.k.a. x86_64 chip-platform only !!" )
                sys.exit(17)

        ### https://gallery.ecr.aws/codebuild/local-builds
        if os.environ['BUILDPLATFORM'] == "linux/amd64" or os.environ['BUILDPLATFORM'] == "linux/x86_64":
            self.IMAGE_VER="latest"
        if os.environ['BUILDPLATFORM'] == "linux/aarch64":
            self.IMAGE_VER="aarch64"

        self.CODEBUILD_AGENT_IMAGE=f"public.ecr.aws/codebuild/local-builds:{self.IMAGE_VER}"
        self.CODEBUILD_AGENT_DEFAULT_IMAGE="public.ecr.aws/codebuild/local-builds:latest"    ### for "amd64", this & above _WILL_ be exactly identical.

        # print( '-'*80 )

        ### Locate the "LOCAL-buildspec.yaml" or as a fall-back the "buildspec.yaml" file.
        self.BUILDSPEC_YAML="buildspec.yaml"
        self.BUILDSPEC_YML ="buildspec.yml"
        if   not   pathlib.Path(self.BUILDSPEC_YAML).exists():
            if pathlib.Path(self.BUILDSPEC_YML).exists():
                self.BUILDSPEC_YAML=self.BUILDSPEC_YML
            else:
                if pathlib.Path(f"LOCAL-{self.BUILDSPEC_YAML}"):
                    print( "MacBook-M1 customized __LOCAL-MacBook-m1__ buildspec.yaml Detected !!" )
                    self.BUILDSPEC_YAML=f"LOCAL-{self.BUILDSPEC_YAML}"
                else:
                    print( f"ERROR: {self.BUILDSPEC_YAML} does NOT exist !!" )
                    sys.exit(19)

        print( linux_cmd_ls(self.BUILDSPEC_YAML) )

    ### -------------------------------------------------------------------------------------------
    def sanityChecks(self):
        if self.parsed_args.platform != "AMAZONLINUX2" and self.parsed_args.platform != "UBUNTU":
            print ( f"ERROR: CLI-arg: {self.parsed_args.platform} = INVALID!!" )
            sys.exit(29)

        ### Make sure Docker Daemon is running
        import shutil
        if shutil.which('docker') is None:
            print( "ERROR: Docker Daemon is NOT running!!" )
            sys.exit(32)

    ### -------------------------------------------------------------------------------------------
    def do_docker_setup(self):

        # print( '-'*80 ); print( '-'*80 );
        print( f"starting at {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}" )
        self.STARTED_AT = time.time()

        docker_client = docker.from_env()
        try:
            docker_client.ping()        ### https://docker-py.readthedocs.io/en/stable/client.html#docker.client.DockerClient.ping
        except docker.errors.APIError:
            print( "ERROR: Docker Daemon is NOT running!!" )
            sys.exit(60)

        try:
            print( f"docker image ls -q {self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG} .. .. ", end="", flush=True )
            docker_client.images.get( self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG )    ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.get
            print( "Done!")
            ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.get
        except docker.errors.ImageNotFound:
            try:
                print( f"docker image pull {self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG} .. .. ", end="", flush=True )
                docker_client.images.pull( self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG )   ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.pull
                print( "Done!")
                ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.pull
            except docker.errors.APIError:
                print( f"\n\n!! ERROR: docker pull {self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG} failed !!" )
                print( "Please manually-BUILD that (missing Docker-image) from Git-SOURCE .. .. and __RE-RUN__ this command." )
                print( "\tTo do that, please run the script: " )
                print( "\tpython3 "+ self.SCRIPTFOLDER_FULLPATH + "/LOCAL-create-aws-codebuild-standard-image.py\n" )
                # user_input = input("Press <ENTER> .. .. to Build that from Git-SOURCE LOCALLY>> ")
                sys.exit(66)

        # print( '-'*80 )

        ### https://gallery.ecr.aws/codebuild/local-builds
        # echo Running .. \
        # docker image ls -q "{CODEBUILD_AGENT_IMAGE}"
        def pull_docker_image( docker_client, docker_image ):
            try:
                print( f"docker image ls -q {docker_image} .. .. ", end="", flush=True )
                docker_client.images.get( docker_image )    ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.get
                print( "Done!")
            except docker.errors.ImageNotFound:
                try:
                    print( f"docker image pull {docker_image} .. .. ", end="", flush=True )
                    docker_client.images.pull( docker_image )   ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.pull
                    print( "Done!")
                except docker.errors.ImageNotFound:
                    print( f"!! FATAL ERROR !! docker pull {docker_image} failed !!" )
                    sys.exit(68)

        pull_docker_image( docker_client=docker_client, docker_image=self.CODEBUILD_AGENT_IMAGE )
        pull_docker_image( docker_client=docker_client, docker_image=self.CODEBUILD_AGENT_DEFAULT_IMAGE )

        # print( '-'*80 )

    ### -------------------------------------------------------------------------------------------
    def do(self):

        self.CODEBUILD_LOCAL_SCRIPT="/tmp/codebuild_build.sh"
        if pathlib.Path(self.CODEBUILD_LOCAL_SCRIPT):
            import requests
            url = "https://raw.githubusercontent.com/aws/aws-codebuild-docker-images/master/local_builds/codebuild_build.sh"
            response = requests.get(url)
            # Write the content to a file

            with open( self.CODEBUILD_LOCAL_SCRIPT, 'w' ) as file:
                file.write(response.text)
            st = os.stat( self.CODEBUILD_LOCAL_SCRIPT )
            os.chmod( path=self.CODEBUILD_LOCAL_SCRIPT, mode= st.st_mode | stat.S_IEXEC )

            print( linux_cmd_ls(self.CODEBUILD_LOCAL_SCRIPT) )

        # print( '-'*80 ); print( '-'*80 );
        print( f"ENDED at {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}" )
        self.ENDED_AT = time.time()
        print( f"TOTAL TIME: {self.ENDED_AT - self.STARTED_AT} seconds" )

        print( "\n\nPlease run the following command:\n")
        print( self.CODEBUILD_LOCAL_SCRIPT +' -i '+ self.AWS_CODEBUILD_RUNTIME_CONTAINER_IMG +' -l '+ self.CODEBUILD_AGENT_IMAGE
                + ' -a /tmp/aws-codebuild-results  -b '+ self.BUILDSPEC_YAML )
                # " -s "+ INSIDE_CONTAINER_SRC_CODE_ROOTFLDR

    ### -------------------------------------------------------------------------------------------
    def main(self):
        parser = argparse.ArgumentParser(       ### https://docs.python.org/3/library/argparse.html#argumentparser-objects
            description='Python re-write of AWS-CodeBuild/LOCAL-aws-codebuild-runner.sh',
            prog=pathlib.Path(__file__).stem,
            # usage=
        )

        ### ----------------------------------
        ### The following does __NOT__ allow   "--???"   on the CLI.
        ### It's called a POSITIONAL-Arg.
        ### Instead it takes ONLY allowed values (as listed in `choices`).
        parser.add_argument(                    ### https://docs.python.org/3/library/argparse.html#the-add-argument-method
            'platform', help='must ONLY be one of the allowed values!',
            # required=True, ### ERROR: TypeError: 'required' is an invalid argument for positionals
            choices=["AMAZONLINUX2","UBUNTU"]            ### https://docs.python.org/3/library/argparse.html#choices
        )

        ### ----------------------------------
        self.__parsed_args = parser.parse_args( self.argv ) ### https://docs.python.org/3/library/argparse.html#the-parse-args-method

        return self  ### always a good practice, to help with chaining.

### -----------------------------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------------------------------------------

if __name__ == '__main__':
    my_script = MyScript(_argv=sys.argv[1:])  ### Note: the "1:"

    my_script.basicChecks()
    my_script.deriveVariables()
    my_script.sanityChecks()
    my_script.do_docker_setup()
    my_script.do()

### EoF
