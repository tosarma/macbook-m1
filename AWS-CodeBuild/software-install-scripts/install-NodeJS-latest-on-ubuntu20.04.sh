#!/bin/bash -f

# Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



### ATTENTION: For Ubuntu 20.04 based Code-Build ENVIRONMENT.
### Use this to install Node.JS latest version on Ubuntu 20.04 based Code-Build ENVIRONMENT.

VERSION_DESIRED="18"

printf "%.0s#" {1..80}; echo ''; printf "%.0s#" {1..80}; echo ''; printf "%.0s#" {1..80}; echo ''
uname -a

### Ubuntu does NOT like "sudo" 🤷🏾‍♂️
apt-get update
apt-get upgrade     -y

echo "in Ubuntu 20.04 .. DEBUGGING Node.JS versions - PRE"

which node
which npm
ls -la /usr/local/bin/node
ls -la /usr/bin/node
ls -la /usr/local/bin/npm
ls -la /usr/bin/npm
### OUTPUT of above commands .. ..
### /usr/local/bin/node
### /usr/local/bin/npm
### -rwxr-xr-x 1 root root 75438256 Nov 15 17:14 /usr/local/bin/node
### ls: cannot access '/usr/bin/node': No such file or directory
### lrwxrwxrwx 1 root root 38 Nov 15 17:14 /usr/local/bin/npm -> ../lib/node_modules/npm/bin/npm-cli.js
### ls: cannot access '/usr/bin/npm': No such file or directory
ls -a /usr/local/bin
echo $PATH
### OUTPUT of above commands .. ..
## . .. aws aws-iam-authenticator aws_completer containerd containerd-shim containerd-shim-runc-v2 corepack ctr dind docker docker-compose docker-init docker-proxy dockerd dockerd-entrypoint.sh dotnet-install.sh ecs-cli eksctl grunt kubectl n node npm npx rbenv-install rbenv-uninstall ruby-build runc sbt stunnel stunnel3 webpack
### /usr/local/bin/sbt/bin:/root/.phpenv/shims:/root/.phpenv/bin:/root/.goenv/shims:/root/.goenv/bin:/go/bin:/root/.phpenv/shims:/root/.phpenv/bin:/root/.pyenv/shims:/root/.pyenv/bin:/root/.rbenv/shims:/usr/local/rbenv/bin:/usr/local/rbenv/shims:/root/.dotnet/:/root/.dotnet/tools/
###             :/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/tools:/codebuild/user/bin


/usr/bin/node --version
/usr/local/bin/node --version

apt-get install -y curl wget

### per NODE.JS official site -- https://github.com/nodesource/distributions#installation-instructions
apt-get install -y npm
apt-get update
apt-get install -y ca-certificates curl gnupg
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR="18"
NODE_MINOR="17" ### 18.17.x
NODE_MICRO="1"  ### 18.17.1
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
apt-get update
apt-cache policy nodejs
apt-get install --yes nodejs=${NODE_MAJOR}.${NODE_MINOR}.${NODE_MICRO}-1nodesource1
  ### Following per: https://joshtronic.com/how-to-install-nodejs-18-on-ubuntu-2004-lts/  as well as https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04
  ### This script, located at https://deb.nodesource.com/setup_X, used to install Node.js is deprecated now and will eventually be made inactive.
  ### Please visit the NodeSource distributions Github and follow the instructions to migrate your repo. https://github.com/nodesource/distributions
  # - curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
  # - curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
  # - apt-get install -y nodejs

### per NODE.JS official site -- https://github.com/nodesource/distributions#installation-instructions
### Important:  Ubuntu-v20.04 OS by default installs the following (belonging to Node.JS Ver-14.x)
###             The above apt-get nodejs installs it in a DIFFERENT folder "/usr/bin" instead.
rm /usr/local/bin/node
rm /usr/local/bin/npm
rm /usr/local/bin/n

echo "in Ubuntu 20.04 .. DEBUGGING Node.JS versions - POST"

which node
which npm
echo $PATH

# /usr/bin/node --version
# /usr/local/bin/node --version
node --version
npm --version

### EoScript
