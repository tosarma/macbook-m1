#!/bin/bash -f

# Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



### ATTENTION: For Ubuntu 20.04 based Code-Build ENVIRONMENT.
### Use this to install Node.JS latest version on Ubuntu 20.04 based Code-Build ENVIRONMENT.

for i in {1..2}; do
    printf "%.0s-" {1..160}; echo ''
done
echo ''; date; echo ''
BEGIN="$(date)"

### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------

### Official Google's instructions for installing Chrome.
### Download the latest version of Chrome per https://cloud.google.com/looker/docs/best-practices/how-to-install-chromium-for-amazon-linux
pushd /tmp



### Pick one - yum vs. deb vs. apt-get


# wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
# yum install -y ./google-chrome-stable_current_x86_64.rpm


wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb




popd

### !!! Attention !!! Verify that this script is SAFE.  It was recommended by above Google site.
bash ../infrastructure/intoli.com-install-google-chrome.sh

which google-chrome-stable

### --------------------------
### Following NO LONGER works!  Stopped working around 2020.
### Per AWS OFFICIAL instructions at https://repost.aws/questions/QUxs9gIDEiTNKLr8TX-l3e0g/unable-to-get-headless-chrome-to-work-on-aws-linux-2#AN2olxolwOQDyxw-wYykIeAA
# sudo amazon-linux-extras install epel -y  ### Enable Extra Packages for Enterprise Linux
# sudo yum install -y chromium

### --------------------------
### Following NO LONGER works!  Stopped working around 2020.
###___ did NOT work on Ubuntu 22.04 --> apt-get install -y epel-release
###___ did NOT work on Ubuntu 22.04 --> apt-get install -y chromium
# apt-get update
# apt-get upgrade     -y
# apt-get -y install google-chrome
# apt-get -y install toilet figlet
# apt-get -y install wget libgtk-3-0 libdbus-glib-1-2 libxt6
# apt-get install xvfb -y
# mkdir -p /tmp/.X11-unix
# chmod 1777 /tmp/.X11-unix
# chown root /tmp/.X11-unix/

### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------




### Ubuntu does NOT like "sudo" 🤷🏾‍♂️


### PRE-REQUISITE!!! Install dependencies required for running Chrome:
### CENTOS: yum install -y libX11 libXcomposite libXcursor libXdamage libXext libXi libXtst libXrandr libXScrnSaver libXxf86vm redhat-lsb
apt-get install -y libXcursor libXdamage libXss libXrandr \
    cups-libs dbus-glib libXinerama cairo cairo-gobject pango
        #### Above alternative is from: https://aws.amazon.com/blogs/devops/how-to-run-headless-front-end-tests-with-aws-cloud9-and-aws-codebuild/

echo ''; echo ''; echo "started at: ${BEGIN}"; echo -n "Ended at:"; date; echo ''

### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------

### FIX for: Failed to connect to the bus: Failed to connect to socket /var/run/dbus/system_bus_socket: No such file or directory
### Reference: https://github.com/nodejs/help/issues/3220#issuecomment-912569242
apt-get install -y dbus
### dbus-daemon[8296]: Failed to start message bus: Failed to bind socket "/var/run/dbus/system_bus_socket": No such file or directory
mkdir -p "/var/run/dbus"
chown root "/var/run/dbus"
chmod ugo+rwx "/var/run/dbus"
chgrp dbus "/var/run/dbus"
dbus-daemon --system &

### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------

### Verify that Chrome is installed correctly:
echo '----------------- 1 -----------------'
google-chrome --headless --disable-gpu --disable-dev-shm-usage --disable-software-rasterizer --remote-debugging-port=9222 --no-sandbox https://google.com
echo '----------------- 3 -----------------'
google-chrome '--headless'  '--no-sandbox' '--disable-dev-shm-usage' '--disable-extensions'
echo '----------------- 4 -----------------'
google-chrome '--headless'  '--no-sandbox' '--disable-dev-shm-usage' '--disable-extensions' '--start-maximized' '--start-fullsccreen'
echo '----------------- 6 -----------------'
### WARNING: below-command does __NOT__ return, due to "--remote-debugging-port=.."
google-chrome '--headless'  '--no-sandbox' '--disable-dev-shm-usage' '--disable-extensions' '--start-maximized' '--start-fullsccreen' '--remote-debugging-port=9222'
echo '----------------- 9 -----------------'

### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------

for i in {1..2}; do
    printf "%.0s-" {1..160}; echo ''
done
echo ''; date; echo ''
BEGIN="$(date)"

### Per https://www.npmjs.com/package/chromedriver
npm install chromedriver --chromedriver-force-download

echo ''; echo ''; echo "started at: ${BEGIN}"; echo -n "Ended at:"; date; echo ''

### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------

echo "Update nightwatch.conf.js as follows:"
cat <<EOF
    chrome: { //// REF: https://nightwatchjs.org/guide/browser-drivers/chromedriver.html
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        'chromeOptions': {
            // More info on Chromedriver: https://sites.google.com/a/chromium.org/chromedriver/
            w3c: true,  // w3c:false tells Chromedriver to run using the legacy JSONWire protocol (not required in Chrome 78)
            args: [
                '--no-sandbox',
                '--headless',
                '-silent-launch',
                '--disable-extensions',
                '--disable-dev-shm-usage',
                // '--remote-debugging-port=9222',
                '--start-maximized',
                '--start-fullsccreen',
                // '--silent',  ?????? no such thing ??
                // '--disable-gpu',   ????????? no such  thing ??
                // '--disable-blink-features=AutomationControlled',
            ]
        },
        "prefs": {
          "credentials_enable_service": true,
          "profile.password_manager_enabled": true,
        }
      },
    },
EOF


### --------------------------------------------------------------------
### ====================================================================
### --------------------------------------------------------------------

### EoScript
