#!/bin/bash -f

# Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### This script will __RUN__ your CodeBuild-Project locally on MacBook-M1.
### To that end, this script will AUTOMATICALLY download all relevant Docker-images + AUTOMATICALLY create _NEW_ __LOCAL-ONLY__ Docker-images.
### FYI: The work of automatically creating _NEW_ __LOCAL-ONLY__ Docker-images is done by the script: ./MacBook-M1-LOCAL/AWS-CodeBuild/LOCAL-aws-codebuild-standard.sh

### ----------------------------------------------------
if [ $# -ne 1 ]; then
    echo "USAGE: $0 (AmazonLinux2|Ubuntu)"
    echo "      FYi: the cli-argument is conveniently Case-INSensitive!"
    exit 1
fi

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### BASIC-SETTINGS section.  This is most important.

OS_DISTRO="$1";
OS_DISTRO="$( echo ${OS_DISTRO} | tr '[a-z]' '[A-Z]' )";

if [ -z "${BUILDPLATFORM+x}" ]; then
    echo '';  echo '!!!!!!!!!!!!!!!!! BUILDPLATFORM is _NOT_ set !!!!!!!!!!!!!!!!!'; echo '';
    export BUILDPLATFORM="linux/aarch64"
    # export BUILDPLATFORM="linux/amd64"
    echo "Setting BUILDPLATFORM to '${BUILDPLATFORM}'"
    echo 'sleeping 5s'; sleep 5
fi

# if [ -z "${DOCKER_DEFAULT_PLATFORM+x}" ]; then
    export DOCKER_DEFAULT_PLATFORM="${BUILDPLATFORM}"
# fi
# if [ -z "${TARGETPLATFORM+x}" ]; then
    export TARGETPLATFORM="${DOCKER_DEFAULT_PLATFORM}"
# fi
env | grep PLATFORM



### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### Derive Bash-VARIABLES and Derive ENV-vars.

SCRIPTFOLDER=$(dirname -- "$0")
SCRIPTNAME=$(basename -- "$0")
if [ -e "$(pwd)/${SCRIPTFOLDER}" ]; then
    echo "Detected Relative-Path for script".
    SCRIPTFOLDER_FULLPATH="$(pwd)/${SCRIPTFOLDER}"
else
    echo "Detected Absolute-Path for script".
    SCRIPTFOLDER_FULLPATH="${SCRIPTFOLDER}"
fi
# echo "${SCRIPTFOLDER_FULLPATH}"
CWD=$(pwd)
# echo "Current Working directory = ${CWD}"

pushd . > /dev/null

### ==================================

if [ $OS_DISTRO == "AMAZONLINUX2" ]; then
    ### ARM/AARCH64: AWS_CODEBUILD_RUNTIME_CONTAINER_IMG="public.ecr.aws/aws/codebuild/amazonlinux2-aarch64-standard:${AMAZON_LINUX_VER}";
    if [ "${BUILDPLATFORM}" == "linux/amd64" ] || [ "${BUILDPLATFORM}" == "linux/x86_64" ]; then
        ### https://gallery.ecr.aws/codebuild/amazonlinux2-x86_64-standard
        # AMAZON_LINUX_VER="4.0";       ### Amazon Linux 2 - older-version.
        AMAZON_LINUX_VER="5.0";         ### Amazon Linux 2 - latest;  Incl. Amazon-Linux-2023.
        AWS_CODEBUILD_RUNTIME_CONTAINER_IMG="public.ecr.aws/codebuild/amazonlinux2-x86_64-standard:${AMAZON_LINUX_VER}";
    fi
    if [ "${BUILDPLATFORM}" == "linux/aarch64" ]; then
        ### https://gallery.ecr.aws/codebuild/amazonlinux2-aarch64-standard
        AMAZON_LINUX_VER="3.0";         ### Amazon Linux 2 - latest;  Incl. Amazon-Linux-2023.
        AWS_CODEBUILD_RUNTIME_CONTAINER_IMG="public.ecr.aws/codebuild/amazonlinux2-aarch64-standard:${AMAZON_LINUX_VER}";
    fi
fi
if [ $OS_DISTRO == "UBUNTU" ]; then
    if [ "${BUILDPLATFORM}" == "linux/amd64" ] || [ "${BUILDPLATFORM}" == "linux/x86_64" ]; then
        # UBUNTU_LINUX_VER="4.0";         ### ubuntu = aws/codebuild/standard:5.0 for Ubuntu 20.04
        UBUNTU_LINUX_VER="5.0";         ### ubuntu = aws/codebuild/standard:5.0 for Ubuntu 22.04
        AWS_CODEBUILD_RUNTIME_CONTAINER_IMG="aws/codebuild/standard:${UBUNTU_LINUX_VER}";
    else
        echo "!! ERROR !! UBUNTU O/S only supported for amd64 a.k.a. x86_64 chip-platform only !!"
        exit 67
    fi
fi

### -----------------------
### https://gallery.ecr.aws/codebuild/local-builds
if [ "${BUILDPLATFORM}" == "linux/amd64" ] || [ "${BUILDPLATFORM}" == "linux/x86_64" ]; then
    IMAGE_VER="latest"
fi
if [ "${BUILDPLATFORM}" == "linux/aarch64" ]; then
    IMAGE_VER="aarch64"
fi

CODEBUILD_AGENT_IMAGE="public.ecr.aws/codebuild/local-builds:${IMAGE_VER}"
CODEBUILD_AGENT_DEFAULT_IMAGE="public.ecr.aws/codebuild/local-builds:latest"    ### for "amd64", this & above _WILL_ be exactly identical.

### ==============================================================================================
### ##############################################################################################
### ==============================================================================================

### "Sanity-checks" section.

if [ "${OS_DISTRO}" != "AMAZONLINUX2" ] && [ "${OS_DISTRO}" != "UBUNTU" ]; then
    echo "ERROR: OS_DISTRO: ${OS_DISTRO} = INVALID!!"
    echo "USAGE: $0  (AWS|UBUNTU)"
    exit 2
fi

### Make sure Docker Daemon is running
command -v docker-start-MacBook-M1.sh
if [ $? -eq 0 ]; then docker-start-MacBook-M1.sh; fi

### ==============================================================================================

### "do" section.

for i in {1..2}; do printf "%.0s-" {1..160}; echo ''; done
echo -n "Starting $0 at"; date; echo ''
BEGIN="$(date)"

### Download/Re-creaete the FOUNDATIONAL Docker-Images (to run LOCALLY on MacBook-M1) from OFFICIAL AWS CodeBuild.

# echo Running .. \
# docker image ls -q "${AWS_CODEBUILD_RUNTIME_CONTAINER_IMG}"
OUTPUT=$( docker image ls -q "${AWS_CODEBUILD_RUNTIME_CONTAINER_IMG}" | wc -l )
if [ $OUTPUT -eq 0 ]; then
    docker pull "${AWS_CODEBUILD_RUNTIME_CONTAINER_IMG}"
    if [ $? -ne 0 ]; then
        echo "ERROR: docker pull ${AWS_CODEBUILD_RUNTIME_CONTAINER_IMG} failed !!"
        read -p "Press <ENTER> .. .. to Build that from Git-SOURCE LOCALLY>> "

        set -x
        ${SCRIPTFOLDER_FULLPATH}/LOCAL-create-aws-codebuild-standard-image.sh || exit 16
        set +x
    fi
fi

printf "%.0s-" {1..160}; echo ''

### ----------------------------------------------------
### https://gallery.ecr.aws/codebuild/local-builds
# echo Running .. \
# docker image ls -q "${CODEBUILD_AGENT_IMAGE}"
OUTPUT=$( docker image ls -q "${CODEBUILD_AGENT_IMAGE}" | wc -l )
if [ $OUTPUT -eq 0 ]; then
    set -x
    docker pull "${CODEBUILD_AGENT_IMAGE}"
    docker pull "${CODEBUILD_AGENT_DEFAULT_IMAGE}"  ### assuming these 2 lines are NOT identical.
    ### why pull the image associated with the 'latest' tag?
    ### See GitHub issue titled -> "containerd-integration: cannot interact with images that don't have the default platform"
    ### https://github.com/moby/moby/issues/44578
    set +x
fi

printf "%.0s-" {1..160}; echo ''

### ==============================================================================================
### ##############################################################################################
### ==============================================================================================
pwd

### Locate the "LOCAL-buildspec.yaml" or as a fall-back the "buildspec.yaml" file.
BUILDSPEC_YAML="buildspec.yaml"
BUILDSPEC_YML ="buildspec.yml"
if [ ! -f "./${BUILDSPEC_YAML}" ]; then
    if [ -f "./${BUILDSPEC_YML}" ]; then
        BUILDSPEC_YAML="${BUILDSPEC_YML}"
    else
        if [ -f "./LOCAL-${BUILDSPEC_YAML}" ]; then
            echo "MacBook-M1 customized __LOCAL-MacBook-m1__ buildspec.yaml Detected !!"
            BUILDSPEC_YAML="LOCAL-${BUILDSPEC_YAML}"
        else
            echo "ERROR: ${BUILDSPEC_YAML} does NOT exist !!"
            exit 31
        fi
    fi
fi
ls -la "${BUILDSPEC_YAML}"
# if [ ! -L "${BUILDSPEC_YAML}" ]; then ln -s "??????????????????????????????"/buildspec.yaml ; fi;


printf "%.0s-" {1..160}; echo ''

### ----------------------------------------------------
pwd

CODEBUILD_LOCAL_SCRIPT="/tmp/codebuild_build.sh"
if [  ! -f "${CODEBUILD_LOCAL_SCRIPT}" ]; then
    curl --output ${CODEBUILD_LOCAL_SCRIPT} \
        https://raw.githubusercontent.com/aws/aws-codebuild-docker-images/master/local_builds/codebuild_build.sh
    chmod +x ${CODEBUILD_LOCAL_SCRIPT}
fi
ls -la ${CODEBUILD_LOCAL_SCRIPT}


set -x
"${CODEBUILD_LOCAL_SCRIPT}" -i ${AWS_CODEBUILD_RUNTIME_CONTAINER_IMG} -l "${CODEBUILD_AGENT_IMAGE}"     \
        -a /tmp/aws-codebuild-results  -b "${BUILDSPEC_YAML}"
        # -s "${INSIDE_CONTAINER_SRC_CODE_ROOTFLDR}"
set +x

### ----------------------------------------------------
echo ''; echo ''; echo "started at: ${BEGIN}"; echo -n "Ended at:"; date; echo ''

### EoScript
