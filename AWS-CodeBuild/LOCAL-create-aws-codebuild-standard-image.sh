#!/bin/bash

# Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### This script will automatically create _NEW_ __LOCAL-ONLY__ Docker-image.
### This docker-image will use Ubuntu 20.04 or 22.04 (as appropriate defined below).
### This docker-image is what is used to run CodeBuild-FRAMEWORK locally on MacBook-M1.

### SOURCE: https://github.com/aws/aws-codebuild-docker-images#how-to-build-docker-images

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### BASIC-SETTINGS section.  This is most important.

SCRATCH_SPACE=~/LocalDevelopment/

GITREPONAME="aws-codebuild-docker-images"     ### AWS official GitHub Repo -- for Laptop-LOCAL-CodeBuild Images.
GITREPOURL="https://github.com/aws/${GITREPONAME}.git"
GITCLONEFLDR="LOCAL-${GITREPONAME}"

# UBUNTU_LINUX_VER="7.0"; # ubuntu = aws/codebuild/standard:7.0
UBUNTU_LINUX_VER="5.0"; # ubuntu = aws/codebuild/standard:5.0

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

### Derive Bash-VARIABLES and Derive ENV-vars.

SCRIPTFOLDER=$(dirname -- "$0")
SCRIPTNAME=$(basename -- "$0")
if [ -e "$(pwd)/${SCRIPTFOLDER}" ]; then
    # echo "Detected Relative-Path for script".
    SCRIPTFOLDER_FULLPATH="$(pwd)/${SCRIPTFOLDER}"
else
    # echo "Detected Absolute-Path for script".
    SCRIPTFOLDER_FULLPATH="${SCRIPTFOLDER}"
fi

### --------------------
if [ -z "${BUILDPLATFORM+x}" ]; then
    echo '';  echo '!!!!!!!!!!!!!!!!! BUILDPLATFORM is _NOT_ set !!!!!!!!!!!!!!!!!'; echo '';
    exit 83
fi
export DOCKER_DEFAULT_PLATFORM="${BUILDPLATFORM}"
export TARGETPLATFORM="${DOCKER_DEFAULT_PLATFORM}"
env | grep PLATFORM

### ==============================================================================================
### ##############################################################################################
### ==============================================================================================

### "Sanity-checks" section.

if [ ! -d "${SCRATCH_SPACE}" ]; then
    echo ''; echo "!! ERROR !! Folder '${SCRATCH_SPACE}' does not exist !!"
    echo ''; echo "Either create that folder .. or .. Pls correct the value of variable 'SCRATCH_SPACE' in '${SCRIPTNAME}' script."
    exit 1
fi

### ==============================================================================================
### ##############################################################################################
### ==============================================================================================

### "do" section.

cd "${SCRATCH_SPACE}"
pwd

if [ ! -d "${GITCLONEFLDR}" ]; then
    git clone "${GITREPOURL}" --depth 1 || exit 12
    mv -i "${GITREPONAME}" "${GITCLONEFLDR}"
else
    echo "git-repo already cloned locally!"
fi
cd "${GITCLONEFLDR}"
git pull || exit 14
cd ubuntu/standard/${UBUNTU_LINUX_VER}
pwd

### ------------------------------
printf "%.0s#" {1..80}; echo 'Starting at'; date

docker build -t aws/codebuild/standard:${UBUNTU_LINUX_VER}   .

echo -n $?; echo " = return-status of above command"
printf "%.0s#" {1..80}; echo 'ENDED at'; date

docker run -it --entrypoint sh aws/codebuild/standard:${UBUNTU_LINUX_VER} -c uname -a

echo -n $?; echo " = return-status of above command"
printf "%.0s#" {1..80}; echo ''; echo ''

exit 0

### EoScript
