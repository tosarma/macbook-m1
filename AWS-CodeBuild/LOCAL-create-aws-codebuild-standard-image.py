LICENSE = """
Copyright 2023 Udaybhaskar Sarma Seetamraju ToSarma@gmail.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

### This script will automatically create _NEW_ __LOCAL-ONLY__ Docker-image.
### This docker-image will use Ubuntu 20.04 or 22.04 (as appropriate defined below).
### This docker-image is what is used to run CodeBuild-FRAMEWORK locally on MacBook-M1.

### SOURCE: https://github.com/aws/aws-codebuild-docker-images#how-to-build-docker-images

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

import os
import sys
import pathlib
import time
import docker

import git      ### pip3 install GitPython     https://gitpython.readthedocs.io/en/stable/intro.html#installing-gitpython

### BASIC-SETTINGS section.  This is most important.

DEBUG = False

SCRATCH_SPACE=pathlib.Path.home() / "LocalDevelopment"

GITREPONAME=  "aws-codebuild-docker-images"     ### AWS official GitHub Repo -- for Laptop-LOCAL-CodeBuild Images.
GITREPOURL = f"https://github.com/aws/{GITREPONAME}.git"
GITCLONEFLDRNAME= f"LOCAL-{GITREPONAME}"
GITCLONEFLDRPATH= SCRATCH_SPACE / GITCLONEFLDRNAME
print( f"GITCLONEFLDRPATH='{GITCLONEFLDRPATH}'" )

# UBUNTU_LINUX_VER="7.0"; # ubuntu = aws/codebuild/standard:7.0
UBUNTU_LINUX_VER="5.0"; # ubuntu = aws/codebuild/standard:5.0

### -------------------------------------------------------------------------------------------
### ###########################################################################################
### -------------------------------------------------------------------------------------------

def clone_git_repo( new_repo_name: str, repo_url: str, empty_working_dir: str ) -> git.Repo:

    if DEBUG: print(__file__,": clone_git_repo(): about to git clone, where PWD/CWD =", os.getcwd() )
    if DEBUG: print( f"Creating Git-Repo named: {new_repo_name} .. at: {empty_working_dir} .." )
    aws_git_repo = git.Repo.init( empty_working_dir, mkdir=True, bare=False, initial_branch="main" )    ### https://gitpython.readthedocs.io/en/stable/reference.html#git.repo.base.Repo.init

    os.chdir( empty_working_dir )

    if DEBUG: print( f"Setting REMOTE-ORIGIN to: {repo_url} .." )
    aws_git_repo.git.remote( "add", "origin", repo_url )

    if DEBUG: print( "p-u-l-l-ing from REMOTE-ORIGIN: .." )
    aws_git_repo.git.pull( repo_url, "main")

    branch_name = "main";
    if DEBUG: print( f"p-u-s-h-ing BRANCH: {branch_name} .." )
    aws_git_repo.git.push( "--set-upstream", "origin", branch_name )

    return aws_git_repo

### -------------------------------------------------------------------------------------------
### ###########################################################################################
### -------------------------------------------------------------------------------------------

### Derive Bash-VARIABLES and Derive ENV-vars.

SCRIPTFOLDER = pathlib.Path(__file__).parent.absolute()
SCRIPTNAME = pathlib.Path(__file__).name
CWD = pathlib.Path.cwd()

if os.environ.get('BUILDPLATFORM') is None:
    print( "!! Attention !! BUILDPLATFORM is NOT defined. setting it to defaults." )
    os.environ['BUILDPLATFORM'] = 'linux/aarch64'
    sys.exit(19)

os.environ['DOCKER_DEFAULT_PLATFORM'] = os.environ['BUILDPLATFORM']
os.environ['TARGETPLATFORM'] = os.environ['BUILDPLATFORM']

for key, value in os.environ.items():
    if 'PLATFORM' in key:
        print(f'{key}: {value}')

### ==============================================================================================
### ##############################################################################################
### ==============================================================================================

### "Sanity-checks" section.

if not SCRATCH_SPACE.exists() or not SCRATCH_SPACE.is_dir():
    print( f"\n\n!! ERROR !! Folder '{SCRATCH_SPACE}' does not exist !!\n" )
    print( f"\n\tEither create that folder .. or\n\t .. Pls correct the value of variable 'SCRATCH_SPACE' in '{SCRIPTNAME}' script." )
    sys.exit( 1 )

### ==============================================================================================
### ##############################################################################################
### ==============================================================================================

### "do" section.

print( f"starting at {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}" )
STARTED_AT = time.time()
### ------------------------------
### Clone the AWS official repo, if it hasnt been cloned already (by this script).

os.chdir( GITCLONEFLDRPATH )
print( pathlib.Path.cwd() )

if not GITCLONEFLDRPATH.exists():
    pathlib.Path.mkdir( GITCLONEFLDRPATH, parents=True, exist_ok=False )
    clone_git_repo( new_repo_name=GITREPONAME, repo_url=GITREPOURL, empty_working_dir=GITCLONEFLDRPATH)
    # git clone "${GITREPOURL}" "${GITCLONEFLDRNAME}" --depth 1 || exit 12
else:
    print( "✅ git-repo already cloned locally!" )

# git pull || exit 14
aws_git_repo = git.Repo( path=GITCLONEFLDRPATH, search_parent_directories=False )
# aws_git_repo.init( path=GITCLONEFLDRPATH, mkdir=False )
### https://gitpython.readthedocs.io/en/stable/reference.html#git.remote.Remote.pull
aws_git_repo.remote().pull( refspec="refs/heads/master:refs/heads/origin", progress=git.UpdateProgress(), kill_after_timeout=5, allow_unsafe_protocols=False, )

### ------------------------------
### Make sure Docker Daemon is running
import shutil
if shutil.which('docker') is None:
    print( "ERROR: Docker Daemon is NOT running!!" )
    sys.exit(32)

docker_client = docker.from_env()
try:
    docker_client.ping()     ### https://docker-py.readthedocs.io/en/stable/client.html#docker.client.DockerClient.ping
except docker.errors.APIError:
    print( "ERROR: Docker Daemon is NOT running!!" )
    sys.exit(60)

### ------------------------------
os.chdir( GITCLONEFLDRPATH )
os.chdir( f"ubuntu/standard/{UBUNTU_LINUX_VER}" )
print( pathlib.Path.cwd() )

try:
    docker_client.images.build(     ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.build
        path = ".",
        tag=f"aws/codebuild/standard:{UBUNTU_LINUX_VER}",
        platform=os.environ['BUILDPLATFORM'],
        quiet=False,
    )
    ### https://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.build
    ### implement: -> docker build -t aws/codebuild/standard:${UBUNTU_LINUX_VER}   .
except docker.errors.BuildError:
    print( "\n\n!! ERROR !! `Build` using Docker's Python-API is NOT working." )
    print( "\tTo workaround, MANUALLY run the following commands:\n" )
    print( "\tset the ENVIRONMENT-VARIABLE --> BUILDPLATFORM=linux/amd64" )
    print( "\tset the ENVIRONMENT-VARIABLE --> TARGETPLATFORM=linux/amd64" )
    print( "\tset the ENVIRONMENT-VARIABLE --> DOCKER_DEFAULT_PLATFORM=linux/amd64" )
    print( f"\tcd {pathlib.Path.cwd()}" )
    print( f"\tdocker build -t aws/codebuild/standard:{UBUNTU_LINUX_VER}   .\n")
    time.sleep(60)
    sys.exit(60)

print( f"ENDED at {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}" )
ENDED_AT = time.time()
print( f"TOTAL TIME: {ENDED_AT - STARTED_AT} seconds" )

print( f"✅ Docker-image aws/codebuild/standard:{UBUNTU_LINUX_VER} built successfully!\n\tTo _TEST_ it (sanity-check), run the command:" )
print( f"\tdocker run -it --entrypoint sh aws/codebuild/standard:{UBUNTU_LINUX_VER} -c uname -a" )

sys.exit(0)

### EoScript
