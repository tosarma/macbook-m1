from typing import List, Dict, Sequence
import boto3
import sys

### ####################################################################################################

def find_orphan_enis_for_sg(
    security_group_id :str,
) -> Sequence:
    ec2 = boto3.client('ec2')
    return find_orphan_enis_for_sg( security_group_id, ec2 )

### ####################################################################################################

def find_orphan_enis_for_sg(
    security_group_id :str,
    boto3_client :boto3.client,
) -> Sequence:

    # Describe network interfaces associated with the security group
    response = boto3_client.describe_network_interfaces( Filters=[ {
        'Name': 'group-id',
        'Values': [security_group_id]
    } ] )

    orphan_enis = []

    for eni in response['NetworkInterfaces']:
        # Check if the ENI is not attached to an instance
        if 'Attachment' not in eni or eni['Attachment']['Status'] == 'detached':
            orphan_enis.append({
                'NetworkInterfaceId': eni['NetworkInterfaceId'],
                'SubnetId': eni['SubnetId'],
                'VpcId': eni['VpcId'],
                'AvailabilityZone': eni['AvailabilityZone'],
                'Description': eni.get('Description', 'No description'),
                'Status': eni['Status']
            })

    return orphan_enis


### ####################################################################################################

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print( f"Usage: python {sys.argv[0]} <security_group_id>" )
        sys.exit(1)

    security_group_id = sys.argv[1]

    orphan_enis = find_orphan_enis_for_sg(security_group_id)

    if orphan_enis:
        print(f"Found {len(orphan_enis)} potential orphan ENI(s):")
        for eni in orphan_enis:
            print(f"ENI ID: {eni['NetworkInterfaceId']}")
            print(f"Subnet ID: {eni['SubnetId']}")
            print(f"VPC ID: {eni['VpcId']}")
            print(f"Availability Zone: {eni['AvailabilityZone']}")
            print(f"Description: {eni['Description']}")
            print(f"Status: {eni['Status']}")
            print("---")
    else:
        print("No potential orphan ENIs found.")

### EoScript
