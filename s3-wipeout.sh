#!/bin/bash -f

### aws s3 ls | cut -d ' ' -f 3
BUCKETS=(
    cdk-hnb659fds-assets-285217901196-us-east-1
    cdk-hnb659fds-assets-285217901196-us-west-2
    emfact-backend-dev-state-apisessionresults23278a2-1c0glzx2e1b6n
    emfact-backend-dev-state-apisessionresults23278a2-1c0o7i38axfyp
    emfact-backend-dev-state-apisessionresults23278a2-jj8uh6wn8qki
    emfact-backend-pipeline-emfactbackendpipelinedev-1ppnk7ehrjhrj
    emfact-backend-pipeline-emfactbackendpipelinedev-24szs7zemntz
    emfact-dev-frontend-frontendcloudfrontloggingbuck-5srvr98qekzv
    emfact-dev-frontend-monitormycanaryartifactsbucke-106knk5sc3cg0
    emfact-frontend-cloudfro-frontendcloudfrontloggin-1tf77e6okdznd
    emfact-frontend-cloudfro-frontendmycanaryartifact-1u8f14lljs4e6
    emfact-frontend-cloudfro-frontends3bucket3b67c49e-pmzo3buxzedg
    emfact-frontend-cloudfro-frontends3loggingbucketf-ipvbm76ec7g9
    emfact-frontend-dev-fron-frontendmycanaryartifact-1fyudggse91mb
    emfact-frontend-dev-fron-frontends3bucket3b67c49e-1lykps3pqw1iu
    emfact-frontend-dev-fron-frontends3bucket3b67c49e-cp4pqcftgwll
    emfact-frontend-dev-fron-frontends3loggingbucketf-1av0rc56bds2s
    emfact-frontend-dev-fron-frontends3loggingbucketf-5lpx2zecspnm
    emfact-frontend-dev-fron-frontends3loggingbucketf-jglyuihdzr34
    emfact-frontend-dev-frontendcloudfrontloggingbuck-11ooj1hehs237
    emfact-frontend-dev-frontendcloudfrontloggingbuck-1746cyphwc27k
    emfact-frontend-dev-frontendcloudfrontloggingbuck-19bi5vtkkvizc
    emfact-frontend-dev-frontendcloudfrontloggingbuck-1aeyws2l3gs06
    emfact-frontend-dev-frontendcloudfrontloggingbuck-1h1iiobka2xs3
    emfact-frontend-dev-frontendcloudfrontloggingbuck-eahmtw7nhgbc
    emfact-frontend-dev-frontendcloudfrontloggingbuck-eguftjvaetvk
    emfact-frontend-dev-frontendcloudfrontloggingbuck-tylhb9lxqq7x
    emfact-frontend-dev-frontendmycanaryartifactsbuck-1txrc9ubh0hk
    emfact-frontend-dev-frontendmycanaryartifactsbuck-ngzv7wpdnanj
    emfact-frontend-dev-frontends3bucket3b67c49e-1iw1fo1qgdhgo
    emfact-frontend-dev-frontends3bucket3b67c49e-1nmqlpmqbetzz
    emfact-frontend-dev-frontends3bucket3b67c49e-dn7v0ezk7xm0
    emfact-frontend-dev-frontends3bucket3b67c49e-pq5fiiujlw8t
    emfact-frontend-dev-frontends3bucket3b67c49e-rwf5nyksxhj6
    emfact-frontend-dev-frontends3loggingbucketfc6d67-1d8471b5hb7mh
    emfact-frontend-dev-frontends3loggingbucketfc6d67-1t6f9wd3a35eu
    emfact-frontend-dev-frontends3loggingbucketfc6d67-1va5rnxkkmpzk
    emfact-frontend-dev-frontends3loggingbucketfc6d67-2cpyzs2sitju
    emfact-frontend-dev-frontends3loggingbucketfc6d67-6oajl2mdyv83
    emfact-frontend-dev-frontends3loggingbucketfc6d67-rq2xi3ubqqdo
    emfact-frontend-dev-haib-frontendmycanaryartifact-kya2bp31xrow
    emfact-frontend-dev-haib-frontends3bucket3b67c49e-17jne2av713ip
    emfact-frontend-dev-haib-frontends3bucket3b67c49e-l027vkf7urzs
    emfact-frontend-dev-haib-frontends3bucket3b67c49e-o36xdg29gpj1
    emfact-frontend-dev-haib-frontends3bucket3b67c49e-qeivhxr4rlgq
    emfact-frontend-dev-haib-frontends3loggingbucketf-1g3x941egfho3
    emfact-frontend-dev-haib-frontends3loggingbucketf-4wuurllch0wf
    emfact-frontend-dev-haib-frontends3loggingbucketf-dmsttb2oz9ew
    emfact-frontend-dev-haib-frontends3loggingbucketf-lffv7fqdhz5r
    emfact-frontend-dev-haib-frontends3loggingbucketf-mp0813p134to
    emfact-frontend-login-frontendcloudfrontloggingbu-5ijtfbjzm07c
    emfact-frontend-login-frontendmycanaryartifactsbu-3mw1paaweth4
    emfact-frontend-login-frontends3bucket3b67c49e-1w8vcychnsshl
    emfact-frontend-login-frontends3loggingbucketfc6d-15sgs26waaf3d
    emfact-frontend-login2-frontendcloudfrontloggingb-1l3039caj009l
    emfact-frontend-login2-frontendmycanaryartifactsb-xuvg7rfvze27
    emfact-frontend-login2-frontends3bucket3b67c49e-njdxd8hshkfk
    emfact-frontend-login2-frontends3loggingbucketfc6-xe33w07xx28u
    emfact-frontend-pipeline-emfactfrontendpipelinede-8cyzq6jzv95w
    emfact-pipeline-dev-emfactpipelinedevpipelinearti-162ztno7iepe9
    emfact-pipeline-dev-emfactpipelinedevpipelinearti-1arzopom9v1yg
    emfact-pipeline-dev-emfactpipelinedevpipelinearti-43dh4hq4c2ci
    emfact-pipeline-dev-emfactpipelinedevpipelinearti-j4qgpfdahb0j
    emfact-pipeline-dev-emfactpipelinedevpipelinearti-zhfq5crzh31x
    emfact-pipeline-dev-pipelineartifactsbucketaea9a0-1ejz2f3eehdkd
    emfact-pipeline-dev-pipelineartifactsbucketaea9a0-yosw325gatrj
)

for b in ${BUCKETS[@]}; do
	echo \
	python3 s3_bucket_cleanup.py $b
	python3 s3_bucket_cleanup.py $b
done


### EoF

