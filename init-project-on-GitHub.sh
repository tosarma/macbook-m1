#!/bin/bash -f

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

if [ "$1" == "--all" ]; then
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''; echo "!! ATTENTION !! WIPING OUT ALL Environments!!!"
    echo ''
    sleep 60
    WIPE_ALL="true"
fi

###---------------------------------------------------------------

PROJECT_HOME="$( cd ${SCRIPT_FOLDER}/..; pwd )"
# echo $PROJECT_HOME

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

printf "%.s-" {1..100}; echo ''; echo "Creating the GitHub-Workflows PROJECT"; sleep 2
echo \
gh project create --owner "${OWNER}" --title "${PROJECTNAME}"
gh project create --owner "${OWNER}" --title "${PROJECTNAME}"

###---------------------------------------------------------------

printf "%.s-" {1..100}; echo ''; echo "Creating the GitHub-ENVIRONMENT '${ENV}'"; sleep 2
echo \
gh api --method PUT -H "Accept: application/vnd.github+json" repos/${OWNER}/${REPONAME}/environments/${ENV}
gh api --method PUT -H "Accept: application/vnd.github+json" repos/${OWNER}/${REPONAME}/environments/${ENV}
### REF: https://docs.github.com/en/rest/quickstart?apiVersion=2022-11-28

###---------------------------------------------------------------


printf "%.s-" {1..100}; echo ''; echo "Loading all PROJECT-Specific variables"; sleep 2
"${SCRIPT_FOLDER}/load-github-actions-variables.sh"

sleep 2
printf "%.s-" {1..100}; echo ''; echo "Loading all PROJECT-Specific SECRETS"; sleep 2
"${SCRIPT_FOLDER}/create-github-actions-secrets.sh"

###---------------------------------------------------------------

# EoScript
