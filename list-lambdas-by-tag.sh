#!/bin/bash -f

### Takes No CLI-args.
### Filters for all lambdas that meet the values in the following VARIABLES
###   see also: ./list-lambdas-details.sh

ENV="sarma"
APP_NAME="FACT"
NO_OLDER_THAN="7"     ### if aws-cli was run recently (< than these many # of -DAYS-)

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Derived Variables

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

.  "${SCRIPT_FOLDER}/settings.sh"


### ---------------------------------------------------------------

### Section: Derived Variables

### ---------------------------------------------------------------

### Section: --TEMP-- Variables

# Get the list of all Lambda functions
FN_LIST="/tmp/fn-list.txt"
FN_LIST_JSON="/tmp/all-rsrcs-lambdas.json"

### ---------------------------------------------------------------

if [ ! -f ${FN_LIST_JSON} ]; then
    RE_RUN_CMD="y"
else
    if [[ $(find "${FN_LIST_JSON}" -mtime +${NO_OLDER_THAN} -print) ]]; then
        RE_RUN_CMD="y"
        echo "Skipping command -> 'aws resourcegroupstaggingapi', since I ran it in the last ${NO_OLDER_THAN} days"
    fi
fi
if [ "${RE_RUN_CMD}" == "y" ]; then
    echo "About to invoke the massive AWS-API 'resourcegroupstaggingapi' .. .."
    sleep 4
    aws resourcegroupstaggingapi get-resources --resource-type-filters "lambda" >  ${FN_LIST_JSON}
fi
sleep 1

jq  ".ResourceTagMappingList[] | select(.Tags[].Key == \"application\" and .Tags[].Value == \"${APP_NAME}\")  | select(.Tags[].Key == \"branch\" and .Tags[].Value == \"${ENV}\")  | .ResourceARN " --raw-output ${FN_LIST_JSON}

exit $?

### EoScript
