#!/bin/bash -f

### A generic script that works on ANY *.config.sh anywhere;
### This will read the contents of that `*.config.sh` file for variables like CFT_FILENAME and CFT_PARAMETERS .. ..
###     ... and use that to dynamically-generate + run the `aws cloudformation create-stack` command.
### This script is generic enough to be run from ANYWHERE, while also providing a RELATIVE/ABSOLUTE path to the CLI-arg-filename

set -e

SCRIPTFOLDER=$(dirname -- "$0")
SCRIPTNAME=$(basename -- "$0")
if [ -e "$(pwd)/${SCRIPTFOLDER}" ]; then
   echo "Detected Relative-Path for script".
   SCRIPTFOLDER_FULLPATH="$(pwd)/${SCRIPTFOLDER}"
else
   echo "Detected Absolute-Path for script".
   SCRIPTFOLDER_FULLPATH="${SCRIPTFOLDER}"
fi
# echo "${SCRIPTFOLDER_FULLPATH}"
CWD=$(pwd)
# echo "Current Working directory = ${CWD}"

pushd . > /dev/null

### -----------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------

MYSRC=${SCRIPTFOLDER_FULLPATH}
COMMONSETTINGS="${MYSRC}/settings.sh"
if [ ! -e ${COMMONSETTINGS} ]; then
   echo "ERROR: ${COMMONSETTINGS} is missing"
   exit 9
fi



ls -la ${COMMONSETTINGS}
. ${COMMONSETTINGS}




MYSTACKS=()



###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

### Loads "Bash functions" like "waitForCompleteCreationOfStack()"
. ${MYSRC}/checkStackStatus.sh

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

if [ -e "/$1" ]; then
   CUSTOMIZEDSETTINGS="$1"     ### Looks like "$1" is ABSOLUTE-PATH !!
else if [ -e "${SCRIPTFOLDER_FULLPATH}/$1" ]; then
   CUSTOMIZEDSETTINGS="${SCRIPTFOLDER_FULLPATH}/$1"    ### Relative-PATH from script's folder!
else
   CUSTOMIZEDSETTINGS="${CWD}/$1"  ### Maybe "$1" is in "cwd" folder, where the script was invoked from.
fi
fi
if [ ! -e "${CUSTOMIZEDSETTINGS}" ]; then
   if [ -e "${CUSTOMIZEDSETTINGS}.config.sh" ]; then
       CUSTOMIZEDSETTINGS="${CUSTOMIZEDSETTINGS}.config.sh"
   else
       echo "ERROR: ${CUSTOMIZEDSETTINGS} is missing !! Searched everywhere !!"
       exit 19
   fi
fi

###--------------------------------------------------------

CUSTOMIZEDSETTINGS_ABSOLUTEFOLDERPATH=$( dirname ${CUSTOMIZEDSETTINGS} )
CUSTOMIZEDSETTINGS_FILENAME=$( basename ${CUSTOMIZEDSETTINGS} )

###--------------------------------------------------------

cd "${CUSTOMIZEDSETTINGS_ABSOLUTEFOLDERPATH}"
pwd

ls -la "${CUSTOMIZEDSETTINGS_FILENAME}"
. "${CUSTOMIZEDSETTINGS_FILENAME}"


###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------
### Set default values if missing.

if [ -z ${CFT_FILENAME+x} ]; then
   CFT_FILENAME=./${StackName}-CFT.yaml
else
   echo "CFT_FILENAME is already set to ${CFT_FILENAME}"
fi
###----------------------------
if [ -z ${CAPABILITIES+x} ]; then
   CAPABILITIES=CAPABILITY_NAMED_IAM   ### ATTENTION:  Note: this is a UNIQUE capability (with _NAMED_ within its name)
else
   echo "CAPABILITIES is already set to ${CAPABILITIES}"
fi

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

MYSTACKS+=( ${StackName}-${ENV} )
# CFT_FILENAME=./${StackName}.yaml

###----------------------------
echo    \
aws cloudformation describe-stacks --stack-name ${StackName}-${ENV} ${AWSPROFILEREGION}
set +e
aws cloudformation describe-stacks --stack-name ${StackName}-${ENV} ${AWSPROFILEREGION}  >& /dev/null
       ### describe-stacks returns "254" (non-zero) value if NO SUCH STACK.
       ### describe-stacks returns "0" if STACK EXISTS.
EXIT_CODE=$?
set -e
if [ ${EXIT_CODE} -ne 0 ]; then
   echo "[✅] No such stack exists."
   echo \
   aws cloudformation create-stack \
       --stack-name "${StackName}-${ENV}"   \
       --disable-rollback       \
       --template-body file://${CFT_FILENAME}  \
       --capabilities ${CAPABILITIES[@]}           \
       --parameters ${PARAMETERS[@]}           \
       ${AWSPROFILEREGION}                  \
       --tags ${TAGS[@]}
   aws cloudformation create-stack \
       --stack-name "${StackName}-${ENV}"   \
       --disable-rollback       \
       --template-body file://${CFT_FILENAME}  \
       --capabilities ${CAPABILITIES[@]}           \
       --parameters ${PARAMETERS[@]}           \
       ${AWSPROFILEREGION}                  \
       --tags ${TAGS[@]}
   ###----------------------------
else
   ###----------------------------
   echo "[♺] UPDATE STACK."
   echo \
   aws cloudformation update-stack \
       --stack-name "${StackName}-${ENV}"   \
       --template-body file://${CFT_FILENAME}  \
       --capabilities ${CAPABILITIES[@]}           \
       --parameters ${PARAMETERS[@]}           \
       ${AWSPROFILEREGION}                  \
       --tags ${TAGS[@]}
   aws cloudformation update-stack \
       --stack-name "${StackName}-${ENV}"   \
       --template-body file://${CFT_FILENAME}  \
       --capabilities ${CAPABILITIES[@]}           \
       --parameters ${PARAMETERS[@]}           \
       ${AWSPROFILEREGION}                  \
       --tags ${TAGS[@]}
fi

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

DEL_SCRIPT=${TMPDIR}/del-IAMStacks.sh
printf "\e[2m\n"
       ### Terminal BOLD ITALIC DIM HIGHLIGHT STRIKETHRU STRIKETHROUGH FONTS: https://askubuntu.com/a/985386
       ### Attention: STRIKETHRU is the ONLY ONE that does NOT work :-(

echo '#!/bin/bash -f' > ${DEL_SCRIPT}

for STK in "${MYSTACKS[@]}"
do
   echo aws cloudformation delete-stack --stack-name ${STK} ${AWSPROFILEREGION} >> ${DEL_SCRIPT}
done

echo ''
echo ${DEL_SCRIPT}
cat ${DEL_SCRIPT}
printf "\e[22m\n"   ### See MATCHING printf above.

###--------------------------------------------------------
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###--------------------------------------------------------

### Whether there's a 2nd CLI-Argument to this script .. telling us we should wait for above Stack to complete !?
echo ''
if [ "$2" == "--waitForCompleteCreationOfStack" ]; then
   waitForCompleteCreationOfStack "${StackName}-${ENV}"
fi

### EoScript


