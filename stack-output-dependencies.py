import sys
import json
import regex
import time

from generic_aws_cli_script import ( GenericAWSCLIScript )

APP_NAME = "FACT-backend"
THIS_SCRIPT_DATA = "stacks"
DEBUG = False

ALL_STACK_STATUSES=[
    'CREATE_IN_PROGRESS', 'CREATE_FAILED', 'CREATE_COMPLETE', 'ROLLBACK_IN_PROGRESS', 'ROLLBACK_FAILED', 'ROLLBACK_COMPLETE',
    'DELETE_IN_PROGRESS', 'DELETE_FAILED',
    # 'DELETE_COMPLETE',
    'UPDATE_IN_PROGRESS', 'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_COMPLETE', 'UPDATE_FAILED', 'UPDATE_ROLLBACK_IN_PROGRESS', 'UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE', 'REVIEW_IN_PROGRESS', 'IMPORT_IN_PROGRESS', 'IMPORT_COMPLETE', 'IMPORT_ROLLBACK_IN_PROGRESS', 'IMPORT_ROLLBACK_FAILED', 'IMPORT_ROLLBACK_COMPLETE',
]

class AnalyzeStacks(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        stack_name :str,
        aws_profile :str,
        tier :str,
        purpose :str = THIS_SCRIPT_DATA,
        debug :bool = False,
    ) -> None:
        super().__init__(
            appl_name=appl_name,
            aws_profile=aws_profile,
            tier=tier,
            purpose=purpose,
            debug=debug,
        )

        # get list of stacks named f"{self.app_name}-{self.tier}-{}"
        if self.tier.startswith(self.appl_name):
            self.stack_name = f"{self.tier}-{stack_name}"
        else:
            self.stack_name = f"{self.appl_name}-{self.tier}-{stack_name}"
        if self.debug: print(self.stack_name)
        # stack_list = self.get_stack_list()

        stkname_regex = f"{self.appl_name}*"
        # stkname_regex = f"{self.stack_name}" ### exact match
        # stkname_regex = f"{self.stack_name}.{0,20}"

        ec2_client = self.session.client("ec2")

        all_stks_list = self.awsapi_invoker.get_all_stacks_full_details(
            app_name = appl_name,
            json_output_filepath = self.json_output_filepath,
            cache_no_older_than = self.cache_no_older_than,
        )
        # all_stks_list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
        #     aws_client_type = 'cloudformation',
        #     api_method_name = "list_stacks",
        #     response_key = 'StackSummaries',
        #     json_output_filepath = self.json_output_filepath,
        #     additional_params={ "StackStatusFilter": ALL_STACK_STATUSES },
        #     # additional_param_name="StackStatusFilter",
        #     # additional_param_value=ALL_ACTIVE_STACK_STATUSES,
        #     cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        # )
        if self.debug > 2: print(all_stks_list)

        for stk in all_stks_list:
            stknm = stk['StackName']
            print( '.', end="", flush=True)
            if self.debug > 1: print(str(stkname_regex) +'/'+ stknm, end=" // ", flush=True)
            if self.debug > 1: print(stknm +'/'+ stk['StackStatus'], end=".. ", flush=True)

            stk_found = regex.match( pattern=stkname_regex, string=stknm ) and stknm == self.stack_name
            # stk_found = regex.match( pattern=stkname_regex, string=stknm )
            # stk_found =  stk['StackName'].startswith(self.stack_name)
            if stk_found:
                if self.debug: print(f"Analyzing stack '{stknm}'", end=" .. ", flush=True)
                stk_template = stk['TemplateBody']
                if not 'Outputs' in stk:
                    print(f"✅-NO- outputs in stack: '{stknm}'; ")
                    continue
                stk_outputs = stk['Outputs']
                ### Check each output's usage in other stacks
                for output in stk_outputs:
                    output_name = output['OutputKey']
                    # output_value = output['OutputValue']
                    if self.debug: print(f"\nChecking usage of output: {output_name}")
                    # Look for references in other stacks
                    for other_stack in all_stks_list:
                        other_stknm = other_stack['StackName']
                        if self.debug > 3: print(f"**OTHER** stack '{other_stknm}'", end=" .. ", flush=True)
                        if not 'TemplateBody' in other_stack:
                            if self.debug > 4: print(f"⚠️⚠️⚠️-NO- template-body found in stack: {other_stknm}") ### We are NOT loading template-bodies for stack of NO interest.
                            continue
                        template_body = other_stack['TemplateBody']
                        # Convert template to string if it's a dict
                        if isinstance(template_body, dict):
                            template_body = json.dumps(template_body)
                        # Check for Fn::ImportValue or !ImportValue
                        if  f"Fn::ImportValue': '{stknm}:{output_name}'" in template_body or \
                            f'Fn::ImportValue": "{stknm}:{output_name}"' in template_body or \
                            f"Fn::ImportValue': '{output_name}'" in template_body or \
                            f'Fn::ImportValue": "{output_name}"' in template_body or \
                            f"!ImportValue {output_name}" in template_body:
                            print(f"\n👉🏾👉🏾 Stack {stknm}'s Export {output_name} .. is used in OTHER-stack: {other_stknm}")

    # # Get outputs from source stack
    # try:
    #     source_response = cfn.describe_stacks(StackName=source_stack_name)
    #     if not source_response['Stacks'][0].get('Outputs'):
    #         print(f"No outputs found in stack {source_stack_name}")
    #         return

    #     source_outputs = source_response['Stacks'][0]['Outputs']
    # except Exception as e:
    #     print(f"Error getting source stack outputs: {str(e)}")
    #     return

    # # Get all stacks
    # try:
    #     all_stacks = []
    #     paginator = cfn.get_paginator('list_stacks')
    #     for page in paginator.paginate():
    #         all_stacks.extend([
    #             stack for stack in page['StackSummaries']
    #             if stack['StackStatus'] != 'DELETE_COMPLETE'
    #             and stack['StackName'] != source_stack_name
    #         ])
    # except Exception as e:
    #     print(f"Error listing stacks: {str(e)}")
    #     return

### ####################################################################################################

# if invoked via cli
if __name__ == "__main__":
    if len(sys.argv) >= 4:
        aws_profile = sys.argv[1]
        tier = sys.argv[2]
        stack_name = sys.argv[3]
        scr = AnalyzeStacks(
            appl_name = APP_NAME,
            stack_name = stack_name,
            aws_profile=aws_profile,
            tier=tier,
            purpose = THIS_SCRIPT_DATA,
            debug=DEBUG,
        )
    else:
        print( f"Usage:   python {sys.argv[0]} <AWS_PROFILE> <TIER> <SIMPLE__STACK_NAME_SUFFIX>" )
        print( f"EXAMPLE: python {sys.argv[0]} DEVINT   dev    Stateful\n")
        sys.exit(2)
