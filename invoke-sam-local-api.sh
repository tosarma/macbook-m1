#!/bin/bash -f

### Support invoking APIs in 3 scenarios:
###   (i)  SAM LOCAL (on `localhost`)
###   (ii)  Direct invocation of RestApi-Method
###   (iii)  invocation (via CloudFront) of RestApi-Method
###
### NOTE !!! If using CloudFront in-FRONT-of APIGW, then switch the values for:-
###             (1)     `DeveloperTier_APIGW_EndPt`
###             (2)     `API_URL`
###             (3)     UN-comment curl-cli command for `OPTIONS`

if [ $# -ne 3 ]; then
        echo "Usage: $0 <TARGET> <Tier> <LambdaName|apigw-MethodUrl>"
        echo "          TARGET: cloudfront | direct-apigw | sam-local"
        echo "Example: $0 cloudfront nccr-war-token meta/stats "
        exit 1
fi

Target="$1"
Tier="$2"
LambdaName="$3"

AWS_PROFILE="NCCR-nonprod"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### LOAD settings

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

.  "${SCRIPT_FOLDER}/settings.sh"

pwd

### ---------------------------------------------------------------

### Constants

echo " Section: constants .."

### Note: This is pure 100% Query-String .. that is, whatever is AFTER the "?" character.
#__ URLQueryString="?file_name="${urlResponse_FileOutput:t}
#__ URLQueryString="?research_aims=cancer"
URLQueryString=""  ### Empty string implies NO QUery-String is passed.

HTTP_METHOD="GET"

### --------------------------
PayLoad="{\"somekey\":\"somevalue\",\"Tier\":\"${Tier}\"}"
# PayLoad="<html><body><h1>Hello nccr-team</h1></body></html>"

### --------------------------
CognitoIdPIdToken="eyJraWQiOiI2b2RPOEVCK0ZuKzc3aGQxQWF4eW9oYllRXC96MDV3a1JkeGdCdnlLZUs4ND0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoicHNIUnd0N211OGFCLXZKTmJ2N1hsdyIsInN1YiI6Ijg0ZWI1ZGNmLTkxYzktNGY5OS05ZWRmLTQ1ZWU3NTZkMTUyNSIsImNvZ25pdG86Z3JvdXBzIjpbInVzLWVhc3QtMV9mdVJXS2REZDVfYXV0aC5uaWguZ292LXN0YWdlIiwibmNjckV4cGxvcmVHcm91cC1kZXYiXSwiZW1haWxfdmVyaWZpZWQiOnRydWUsImN1c3RvbTpVc2VyUHJpbmNpcGFsTmFtZSI6InNlZXRhbXJhanV1c0BuaWguZ292IiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfZnVSV0tkRGQ1IiwiY29nbml0bzp1c2VybmFtZSI6ImF1dGgubmloLmdvdi1zdGFnZV9zZWV0YW1yYWp1dXNAbmloLmdvdiIsImN1c3RvbTp1c2VyX2F1dGhuX3NvdXJjZSI6Ik5JSCIsInByZWZlcnJlZF91c2VybmFtZSI6InNlZXRhbXJhanV1c0BuaWguZ292IiwiZ2l2ZW5fbmFtZSI6IlNhcm1hIiwiY3VzdG9tOnNhbUFjY291bnROYW1lIjoic2VldGFtcmFqdXVzIiwib3JpZ2luX2p0aSI6IjlhOTMzZjkxLWQyYzMtNDhiNS05NDkyLTU3OTdiODk1MGUzNyIsImF1ZCI6IjQ0NGIxMmlvdWxsbm04b291b3Vvb3RrdjI3IiwiY3VzdG9tOm9yZ19uYW1lIjoiTklIIiwiaWRlbnRpdGllcyI6W3sidXNlcklkIjoic2VldGFtcmFqdXVzQG5paC5nb3YiLCJwcm92aWRlck5hbWUiOiJhdXRoLm5paC5nb3Ytc3RhZ2UiLCJwcm92aWRlclR5cGUiOiJTQU1MIiwiaXNzdWVyIjoiaHR0cHM6XC9cL2F1dGhkZXYubmloLmdvdlwvU0FNTDJcL0lEUCIsInByaW1hcnkiOiJ0cnVlIiwiZGF0ZUNyZWF0ZWQiOiIxNjk2Mjc2MDA5MDUxIn1dLCJjdXN0b206bmlobG9naW5fbWFpbCI6InNhcm1hLnNlZXRhbXJhanVAbmloLmdvdiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNzM4OTYxMjg1LCJleHAiOjE3Mzg5NjMwODYsImlhdCI6MTczODk2MTI4NiwiZmFtaWx5X25hbWUiOiJTZWV0YW1yYWp1IiwianRpIjoiMTIxZjFiYzUtNjY4MS00MzI0LTk5YmEtMDdkYzM1NWQ2ODVhIiwiZW1haWwiOiJzYXJtYS5zZWV0YW1yYWp1QG5paC5nb3YifQ.H2OPdgiWhHJcKf4c2fIb5Zn-4VrYeYQACmRA-XFixNG3ZSvdbTKfGn2eHIC6csJX48aJb0L6PVNY9VfSFRhh6Li-_4tTI5UxZ6WVlRPhdyYcrHyaYCYAVqFX1KSf6z6MAQxvRzyyfBX6d_k4GTW2Wy0S11eNWpie8gLRxOI_heDXeUQCYqgVot7w2ZxJBvReKu5aa9wZx-9mZQl0Zp2Jz1M0Dv9rqtARGoRrTRLCebqkeMzrb_6fiF2zVezWiRlY3OMB3fLR3C3uRxbUItCASvtXewHGdDv_ZJz-50bDuxcZ32y-K1VFMFOdafU--upLSuKNy_qufSQCT0TrSOmquQ"

### ---------------------------------------------------------------

### Section: Derived Variables
echo " Section: Derived Variables .. "

### Just in case LambdaName/Method has '/' or other characters in it.. .. get rid of them when creating NAME of temp-files.
urlResponse_FileOutput="${Tier}_${LambdaName}-response"
urlResponse_FileOutput=$( echo ${urlResponse_FileOutput} | tr '/:' '_' )
urlResponse_FileOutput="/tmp/${urlResponse_FileOutput}"
echo " urlResponse_FileOutput= ${urlResponse_FileOutput} "

APIGW_FQDN="qcyriwfh3g.execute-api.us-east-1.amazonaws.com" ### as Deployed on AWS!!!
CloudFront_FQDN="d10aplkz4ihzzv.cloudfront.net" ### as Deployed on AWS, with CloudFront in front of APIGW!!!

if [ "${Target}" == "direct-apigw" ]; then
        DeveloperTier_APIGW_EndPt="https://${APIGW_FQDN}/${Tier}" ### as Deployed on AWS!!!
else if [ "${Target}" == "cloudfront" ]; then
        DeveloperTier_APIGW_EndPt="https://${CloudFront_FQDN}" ### as Deployed on AWS, with CloudFront in front of APIGW!!!
     else if [ "${Target}" == "sam-local" ]; then
             SAMLOCAL_APIGW_EndPt="http://127.0.0.1:4000"        ### running on LocalHost
          else
             echo "ERROR: Unrecognized CLI-agument Target (1st cli-arg) = '${Target}' ❌❌"
             exit 1
          fi
     fi
fi
echo "  DeveloperTier_APIGW_EndPt='${DeveloperTier_APIGW_EndPt}'"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Section: Sanity Checks & Validation
echo " Section: Sanity Checks & Validation .. "

while read -r -t 2; do read -r -t 1; done ### Flush stdin
read -t 5 -p "SAM Local? [y/N] >>" ANS
if [ -z ${ANS+x} ] || [ "${ANS}" == "" ] || [ "${ANS}" == "y" ] || [ "${ANS}" == "Y" ]; then
        ### When invoking what's DEPLOYED onto AWS
        # API_URL="${DeveloperTier_APIGW_EndPt}"  ### if invoking via CloudFront
        #__ API_URL="${DeveloperTier_APIGW_EndPt}/api/v1/${Tier}_${LambdaName}"  ### Invoking the direct APIGW's stage-specific-METHOD
        #__ API_URL="${DeveloperTier_APIGW_EndPt}/api/v1/${LambdaName}"  ### Invoking the direct APIGW's stage-specific-METHOD
        API_URL="${DeveloperTier_APIGW_EndPt}/api/v1/${LambdaName}"  ### Invoking the direct APIGW's stage-specific-METHOD

else
        ### SAM-LOCAL a.k.a. LocalHost
        #___ API_URL="${SAMLOCAL_APIGW_EndPt}/api/v1/${Tier}_${LambdaName}"
        API_URL="${SAMLOCAL_APIGW_EndPt}/api/v1/${LambdaName}"

fi
echo "API_URL = ${API_URL}"
sleep 2

### --------------------------
        # -H "Origin: http://localhost:8080"                  \

if [ ${Target} == "cloudfront" ]; then
        echo "For CloudFront-facade to RestApi's method .. doing a CORS check .. .."
        set -x
        curl -X OPTIONS    \
        	-H "Authorization: Bearer $CognitoIdPIdToken"       \
                -H "Origin: ${DeveloperTier_APIGW_EndPt}"           \
                -H "Access-Control-Request-Method: ${HTTP_METHOD} " \
                -H "Access-Control-Request-Headers: X-Requested-With"  \
                -H "Content-Type: application/json"                 \
                --output ${urlResponse_FileOutput}                  \
                --dump-header ${urlResponse_FileOutput}.headers     \
                ${API_URL}${URLQueryString}
        EXIT_CODE=$?
        set +x

        if [ ${EXIT_CODE} -ne 0 ]; then
                echo "ERROR: CORS curl-request's EXIT_CODE = '${EXIT_CODE}' ❌❌"
                exit ${EXIT_CODE}
        fi

        echo ''; echo ''; read -p "Continue bash-script? Note: CORS-repsonse saved into files .. >>" ANS
fi


if [ "${HTTP_METHOD}" == "GET" ]; then
        set -x
        curl -X ${HTTP_METHOD}      \
                -H "Authorization: Bearer $CognitoIdPIdToken"       \
                -H "Origin: ${APIGW_FQDN}"                          \
                --output ${urlResponse_FileOutput}                  \
                --dump-header ${urlResponse_FileOutput}.headers     \
                ${API_URL}${URLQueryString}
        EXIT_CODE=$?
        set +x
else
        set -x
        curl -X ${HTTP_METHOD}      \
                -H "Authorization: Bearer $CognitoIdPIdToken"       \
                -H "Origin: ${APIGW_FQDN}"                          \
                -H "Content-Type: application/json"                 \
                --data "${PayLoad}"                                 \
                --output ${urlResponse_FileOutput}                  \
                --dump-header ${urlResponse_FileOutput}.headers     \
                ${API_URL}${URLQueryString}
        EXIT_CODE=$?
        set +x
fi

printf "%.0s_" {1..100} ; echo ""
printf "Curl-CLI's EXIT_CODE = '${EXIT_CODE}'\n\n"
cat ${urlResponse_FileOutput}.headers
cat ${urlResponse_FileOutput}
echo ""; printf "%.0s_" {1..100} ; echo ""

### EoInfo
