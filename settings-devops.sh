#!/bin/false

### --------------------------------------------------------

### DEVOPS Variables

USER="seangchan-ryu-nih"
USER="sarma-nci-nih-essex"

ORG="CBIIT"

OWNER="IODC"
OWNER="NCI-DCCPS"

PROJECTNAME="nccr-dp"
PROJECTID="nccr"
REPONAME="iodc-bento-sandbox"
REPONAME="nccr-dataplatform"

###-----------------

branch="$(git symbolic-ref --short HEAD)"
if [ $? -ne 0 ]; then
    echo "ERROR: 'git command failed !!❌❌❌❌❌❌❌❌❌❌❌"
    exit 13
fi

GIT_BRANCHID_PATTERN="[0-9][0-9]*"

### EoScript
