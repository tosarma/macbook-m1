#!/bin/bash -f




Not working.


###      See also: ./list-lambdas-filter-VPC-by-env.py PYTHON-SCRIPT




### Takes No CLI-args.
### Filters for all lambdas that meet the values in the following VARIABLES
###      See also: ./list-lambdas-by-tag.sh

ENV="sarma"
APP_NAME="FACT"
NO_OLDER_THAN="7"     ### if aws-cli was run recently (< than these many # of -DAYS-)

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Derived Variables

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

.  "${SCRIPT_FOLDER}/settings.sh"


### ---------------------------------------------------------------

### Section: Derived Variables

### ---------------------------------------------------------------

### Section: --TEMP-- Variables

# Get the list of all Lambda functions
FN_LIST="/tmp/fn-list"
FN_LIST_JSON="/tmp/all-lambdas.json"

### ---------------------------------------------------------------

if [ ! -f ${FN_LIST_JSON} ]; then
    RE_RUN_CMD="y"
else
    if [[ $(find "${FN_LIST_JSON}" -mtime +${NO_OLDER_THAN} -print) ]]; then
        RE_RUN_CMD="y"
        echo "Skipping command -> 'aws list-functions' .. since I ran it in the last ${NO_OLDER_THAN} days"
    fi
fi
if [ "${RE_RUN_CMD}" == "y" ]; then
    echo "About to invoke the massive AWS-API 'list-functions' .. .."
    sleep 2
    aws lambda list-functions --output json  ${AWSPROFILEREGION} > ${FN_LIST_JSON}
    # aws lambda list-functions --query "Functions[].FunctionName"
fi
sleep 1

macbook-m1/list-lambdas-by-tag.sh > ${TMPFILE11}

jq  ".Functions[] | [.FunctionName, .VpcConfig.VpcId] | @tsv" --raw-output ${TMPFILE11}

# ### -- -- aws lambda list-functions --query "Functions[].FunctionName" --output text  ${AWSPROFILEREGION} > ${FN_LIST}
# jq  ".Functions[].FunctionName" --raw-output ${FN_LIST_JSON} > ${FN_LIST}
# wc -l ${FN_LIST}

# # For each function, get the tags and check if they match the desired tags
for function_name in $(cat ${TMPFILE11}); do
#     tags=$(aws lambda list-tags --resource arn:aws:lambda:${AWS_REGION}:${AWS_ACCOUNT_ID}:function:$function_name --query "Tags" --output text ${AWSPROFILEREGION} )
#     if [[ $tags == *"application=FACT"* ]] && [[ $tags == *"ENVIRONMENT=dev"* ]]; then
#         echo "Found function: $function_name"
#     else
#         echo -n '.'
#     fi
done

### EoScript
