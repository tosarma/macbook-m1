#!/bin/bash -f

# (1) retrieves a list of closed GitHub issues and ..
# (2) checks if the corresponding Git branches exist in the remote repository.
# (3) If a branch exists, it will ECHO the git-command to DESTROY that git-branch.

###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"

###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

###---------------------------------------------------------------

IAM_ROLE_PATTERN="stateless-privateapiprivaterestapiCloudWat"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------


# Set the status of the Agile tickets to be retrieved
AGILE_TICKET_STATUS="closed"

# Define the GitHub issue fields to be included in the JSON output
GH_ISSUE_FIELDS="assignees,author,body,closed,closedAt,comments,createdAt,id,labels,milestone,number,projectCards,projectItems,reactionGroups,state,title,updatedAt,url"

# Retrieve the list of closed GitHub issues and save it to a JSON file
gh issue list --limit 1000 --state ${AGILE_TICKET_STATUS} --json ${GH_ISSUE_FIELDS} | jq . > /tmp/issues.json

# Extract the branch IDs from the JSON file and sort them
BRANCH_IDs=$( jq '.[].number' /tmp/issues.json | sort -n )

# Iterate over each branch ID
for brid in ${BRANCH_IDs[@]}; do
    sleep 1
    echo -n "${brid} .. "
    # Check if the Git branch exists in the remote repository
    exists_in_remote=$( git ls-remote --heads origin "${PROJECTID}-${brid}" )

    # If the branch exists, delete it
    if [[ "${exists_in_remote}" != "" ]]; then
        echo ; echo \
        git branch --force --delete "${PROJECTID}-${brid}"
    fi
done

### EoF

