### The working version of the shell-script `macbook-m1/list-lambdas-filter-VPC-by-env.sh`
### Filter all lambdas, for those in a specific ENV, and then identify Lambdas that BELONG / DO-NOT belong to a VPC.
### This works great.

import sys
import boto3
import os
import pathlib
import json
import time

if len(sys.argv) < 3:
    print("Usage: python script.py <AWS_PROFILE> <ENV> ?vpc?")
    sys.exit(1)

aws_profile = sys.argv[1]
env         = sys.argv[2]
if len(sys.argv) > 3:
    s = sys.argv[3].lower()
    if s == "vpc":
        must_be_in_vpc = True
    else:
        print("Usage: python script.py <AWS_PROFILE> <ENV> ?vpc?")
        print("last cli-arg must be the exact string 'vpc' ONLY !")
        sys.exit(1)
else:
    must_be_in_vpc = False

### ----------------------------------------------------------------------
### Mnually configurable constants.

appl_name = 'FACT'
TMPDIR    = '/tmp'

no_older_than = 7     ### maximum _ days old

### ----------------------------------------------------------------------
### AWS Configuration

session = boto3.Session(profile_name=aws_profile)
# session = boto3.Session(profile_name=aws_profile, region_name=aws_region)
client = session.client('sts')
account_id = client.get_caller_identity()["Account"]
default_region = session.region_name
print(f"\nAWS Account ID: {account_id}")
print(f"Default AWS Region: {default_region}\n")

one_week_in_seconds = no_older_than * 24 * 60 * 60
current_time = time.time()

### ----------------------------------------------------------------------
### Section: Derived variables and constants

all_rsrcs_json_output_filepath = f"{TMPDIR}/{aws_profile}-{session.region_name}-all-rsrcs-lambda.json"
all_lambdas_json_output_filepath = f"{TMPDIR}/{aws_profile}-{session.region_name}-all-lambdas.json"
all_rsrcs_json_output_filepath = pathlib.Path(all_rsrcs_json_output_filepath) ### convert a string into a Path object.
all_lambdas_json_output_filepath = pathlib.Path(all_lambdas_json_output_filepath) ### convert a string into a Path object.

### ----------------------------------------------------------------------
### Logic to --Cache-- the output of AWS-SDK API-calls (into temporary files)

if not all_rsrcs_json_output_filepath.exists():
    print(f"Cache is missing!! a.k.a. File '{all_rsrcs_json_output_filepath}' is missing!!")
    re_run_rsrcs_awssdk_call = True
else:
    # Check if the file was last modified over a week ago
    file_modified_time = all_rsrcs_json_output_filepath.stat().st_mtime
    if (current_time - file_modified_time) > one_week_in_seconds:
        re_run_rsrcs_awssdk_call = True
        print(f"The CACHE/file '{all_rsrcs_json_output_filepath}' is too old by at least {no_older_than} days !!! ")
    else:
        re_run_rsrcs_awssdk_call = False
        print(f"The CACHE/file '{all_rsrcs_json_output_filepath}' is still fresh enough.\n")

### ----------------------------------------------------------------------
### Logic to --Cache-- the output of AWS-SDK API-calls (into temporary files)

if not all_lambdas_json_output_filepath.exists():
    print(f"Cache is missing!! a.k.a. File '{all_lambdas_json_output_filepath}' is missing!!")
    re_run_lambda_awssdk_call = True
else:
    # Check if the file was last modified over a week ago
    file_modified_time = all_lambdas_json_output_filepath.stat().st_mtime
    if (current_time - file_modified_time) > one_week_in_seconds:
        re_run_lambda_awssdk_call = True
        print(f"The CACHE/file '{all_lambdas_json_output_filepath}' is too old by at least {no_older_than} days !!! ")
    else:
        re_run_lambda_awssdk_call = False
        print(f"The CACHE/file '{all_lambdas_json_output_filepath}' is still fresh enough.\n")

### ----------------------------------------------------------------------
### As necessary invoke AWS SDK API calls.

if re_run_rsrcs_awssdk_call: ### resourceGroupsTaggingApi call

    print("Invoking the massive AWS-SDK API for resourceGroupsTaggingApi .. ..\n")
    client = session.client('resourcegroupstaggingapi')

    response_rsrcs_api = client.get_resources(
        TagFilters=[
            { 'Key': 'branch', 'Values': [env] },
            { 'Key': 'application', 'Values': [appl_name] },
        ],
        ResourceTypeFilters=['lambda']
    )

    # Write the response_rsrcs_api as a json to a file named f"{TMPDIR}/all-rsrcs-lambda.json"
    with open(str(all_rsrcs_json_output_filepath), "w") as f:
        json.dump(response_rsrcs_api, f, indent=4)
else:
    # print(f"File {all_rsrcs_json_output_filepath} is present.\nSo .. using this cached AWS-SDK response_rsrcs_api.. ..\n")
    with open(str(all_rsrcs_json_output_filepath)) as f:
        response_rsrcs_api = json.load(f)

### ------------

if re_run_lambda_awssdk_call: ### lambda list_functions call

    print("Invoking the massive AWS-SDK API for lambda list_functions .. ..\n")
    client = session.client('lambda')
    response_lambdaapi = client.list_functions()
    # Write the response_lambdaapi as a json to a file named f"{TMPDIR}/all-lambdas.json"
    with open(str(all_lambdas_json_output_filepath), "w") as f:
        json.dump(response_lambdaapi, f, indent=4)

else:
    # print(f"File {all_lambdas_json_output_filepath} is present.\nSo .. using this cached AWS-SDK response_lambdaapi.. ..\n")
    with open(str(all_lambdas_json_output_filepath)) as f:
        response_lambdaapi = json.load(f)

### ----------------------------------------------------------------------
### produce output for human consumption

for resource in response_rsrcs_api['ResourceTagMappingList']:
    # print(f"Lambda function: {resource['ResourceARN']}")
    lambda_name :str = resource['ResourceARN'].split(':')[-1]
    for function in response_lambdaapi["Functions"]:
        if function["FunctionName"] == lambda_name:
            vpc_config = function.get("VpcConfig", None)
            if vpc_config:
                if must_be_in_vpc:
                    print(lambda_name)
                # print(f"VpcConfig for {lambda_name}:")
                # print(json.dumps(vpc_config, indent=2))
            else:
                # print(f"No VpcConfig found for {lambda_name}")
                if not must_be_in_vpc:
                    print(lambda_name)
            break
    else:
        print(f"!! INTERNAL ERROR !! Lambda function '{lambda_name}' not found in 'response_rsrcs_api' !!")
        sys.exit(11)

### EoScript
