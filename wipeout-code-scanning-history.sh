#!/bin/bash -f

### Using "gh api" commands, DESTROY all the various FINDINGS/ITEMS listed under "Code Scanning" webpage.

### ACCESS-PRIVILEGE ESCALATION REQUIRED:  Quote ..
###     gh: This API operation needs the "delete_repo" scope. To request it, run:  gh auth refresh -h github.com -s delete_repo

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------

PROJECT_HOME="$( cd ${SCRIPT_FOLDER}/..; pwd )"
# echo $PROJECT_HOME

###---------------------------------------------------------------

TMPROOTDIR="/tmp"
TMPDIR="${TMPROOTDIR}/DevOps/Github/${SCRIPT_NAME}"
TMPFILE11="${TMPDIR}/tmp1.txt"
TMPFILE22="${TMPDIR}/tmp2.txt"
ITEMS_FILE="${TMPDIR}/gh_codescan_findings_to_delete.txt"

mkdir -p ${TMPROOTDIR}
mkdir -p ${TMPDIR}
rm -rf "${TMPFILE11}" "${TMPFILE22}" "${ITEMS_FILE}"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

echo \
gh api --method GET -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
        repos/${OWNER}/${REPONAME}/code-scanning/alerts
gh api --method GET -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
        repos/${OWNER}/${REPONAME}/code-scanning/alerts > "${TMPFILE11}"

ls -la "${TMPFILE11}"
if [ ! -s "${TMPFILE11}" ]; then
    echo "No Code-Scanner findings found for ${OWNER}/${REPONAME};  Quietly quitting."
    exit 0
else
    printf "\n# of Workflow-RUNS found = $( jq '. | length' ${TMPFILE11})\n\n"
fi

jq '.[].number' "${TMPFILE11}"    > "${ITEMS_FILE}"
ls -la  "${ITEMS_FILE}"
read -p "Start deleting? >> " ANSWER;

### Derived from https://github.com/orgs/community/discussions/26256#discussioncomment-5854220
for analysisID in $(cat "${ITEMS_FILE}" ); do
    echo "delete Code-Scanning analysisID=${analysisID}";
    echo \
    gh api --method DELETE -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
        repos/${OWNER}/${REPONAME}/code-scanning/analyses/${analysisID} ;
    read -p "Continue? >> " ANSWER;
    gh api --method DELETE -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" \
        repos/${OWNER}/${REPONAME}/code-scanning/analyses/${analysisID} ;
    #__ curl -L -X DELETE -H "Accept: application/vnd.github+json" -H "Authorization: Bearer $GH_TOKEN" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/${OWNER}/${REPO}/actions/runs/${analysisID} ;
done


### EoScript
