#/bin/bash -f

# Script takes 3 CLI arguments: tier, stackName and AWSPROFILE
# use aws cli to download the cloudformation template for the stack
# use "diff" command to compare the template to the file cdk.out/${stackName}.template.json

AWSREGION="us-east-1"

### ------------------------------------------------------------------

if [ $# -ne 5 ]; then
    echo "Usage: $0 <CDK_APPNAME>  <componentName> <tier> <stackName> <AWSPROFILE>"
    echo "Example: $0 FACT backend dev   abcd1234     DEVINT"
    echo "Example: $0 FACT devops  dev PostDeployment DEVINT"
    echo "      Note: To get correct stack-name, run 'cdk list ..' command"
    exit 1
fi

CDK_APP_NAME="$1"
tier=$3
if [ "$2" == "" ]; then
    if [ "$4" == "" ]; then
        stackName="${CDK_APP_NAME}-${tier}"
    else
        stackName="${CDK_APP_NAME}-${tier}-$4"
    fi
else
    if [ "$4" == "" ]; then
        stackName="${CDK_APP_NAME}-$2-${tier}"
    else
        stackName="${CDK_APP_NAME}-$2-${tier}-$4"
    fi
fi
AWSPROFILE=$5
echo "tier = '${tier}'"
echo "stackName = '${stackName}'"
echo "AWSPROFILE = '${AWSPROFILE}'"

### ------------------------------------------------------------------

downloadedFilePath="cdk.out/DOWNLOADED-${stackName}.template.json"
\rm -f "${downloadedFilePath}"
echo "Downloading deployed-stack to -> '${downloadedFilePath}'"

echo "Downloading the cloudformation template for the stack"
set -x
aws cloudformation get-template \
    --stack-name ${stackName}   \
    --query 'TemplateBody'      \
    --profile ${AWSPROFILE}     \
    --region ${AWSREGION}       \
    > "${downloadedFilePath}"
set +x
ls -la "${downloadedFilePath}"

### ------------------------------------------------------------------
CDK_SYNTH_TEMPLATE="cdk.out/${stackName}.template.json"

echo diff -w "${downloadedFilePath}" "${CDK_SYNTH_TEMPLATE}"
if [ -f "${downloadedFilePath}" ] && [ -f "${CDK_SYNTH_TEMPLATE}" ]; then
    ls -la "${CDK_SYNTH_TEMPLATE}"
    WC=$( diff -w "${downloadedFilePath}" "${CDK_SYNTH_TEMPLATE}" | wc -l )
    if [ $WC -gt 10 ]; then
        echo "TOO DIFFERENT !!!!!!!!!!!!!!!!!!!!! ❌❌"
    else
        read -p "Press enter to start 'diff' command >>"
        diff -w "${downloadedFilePath}" "${CDK_SYNTH_TEMPLATE}"
    fi
else
        echo "cdk.out file is missing !!!!!!!!!!!!!!!!!!!!! ❌❌"
fi


# EoF
