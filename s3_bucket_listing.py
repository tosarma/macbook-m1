import sys
from typing import Optional
import time
import boto3
from datetime import datetime, timezone, timedelta
from botocore.exceptions import ClientError

from generic_aws_cli_script import ( GenericAWSCLIScript )
import json

APP_NAME = "FACTrial"
DEBUG = True

CHUNK_SIZE = 100

"""
List --ALL-- objects and their versions from any bucket -- that are older than specified days

Args:
    bucket_name: Name of the CDK assets bucket
    older_than: Delete objects that are older than this many days; If Not specified, -ALL- objects will be wiped out.
"""
class ListAllObjectsInsideBucket(GenericAWSCLIScript):

    def __init__(self,
        appl_name :str,
        bucket_name :str,
        older_than: Optional[int],
        aws_profile :str,
        tier :str,
        debug :bool = False,
    ) -> None:

        purpose :str = f"bucket-{bucket_name}"

        super().__init__(
            appl_name=appl_name,
            aws_profile=aws_profile,
            tier=tier,
            purpose=purpose,
            debug=debug,
        )

        if older_than < 0:
            raise ValueError("`older_than` must be a non-negative integer")

        cutoff_date = datetime.now(timezone.utc) - timedelta(days=older_than)
        if older_than:
            print(f"Listing objects older than {older_than} days (before {cutoff_date})")
        else:
            print(f"Listing ALL objects")

        self.s3_client = self.session.client('s3')
        self.json_output_filepath_delmrkr = self.gen_name_of_json_outp_file( purpose, suffix="-delmrkr" )
        self.json_output_filepath_listobjv2 = self.gen_name_of_json_outp_file( purpose, suffix="-listobjv2" )

        ### -----------------
        s3_obj_ver_list :list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 's3',
            api_method_name = "list_object_versions",
            response_key = 'Versions',
            json_output_filepath = self.json_output_filepath,
            additional_params={
                "Bucket": bucket_name,
                "MaxKeys": 10000, ### By default, all s3-apis return up to 1,000 keys/items
            },
            # additional_params={ "?????StackStatusFilter": ALL_ACTIVE_STACK_STATUSES },
            # additional_param_name="StackStatusFilter",
            # additional_param_value=ALL_ACTIVE_STACK_STATUSES,
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        if self.debug > 3: print(s3_obj_ver_list)

        ### Sort objects by LastModified timestamp (oldest first)
        s3_obj_ver_list = sorted(
            s3_obj_ver_list,
            key = lambda x: x.get('LastModified', datetime.min)
        )

        obj_list = []
        for s3obj in s3_obj_ver_list:
            last_modified = s3obj['LastModified']
            if isinstance(last_modified, str):
                last_modified = datetime.fromisoformat(last_modified.replace('Z', '+00:00'))
            if self.debug > 4:
                print( f"Key={s3obj['Key']}, LastModified={last_modified}, bool={last_modified < cutoff_date}")
                # time.sleep(1)
            if older_than == None or last_modified < cutoff_date:
                obj_list.append([
                    s3obj['Key'],
                    s3obj['VersionId'],
                    s3obj['LastModified'],
                    s3obj['Size'],
                    s3obj['StorageClass'],
                    # "ETag": s3obj['ETag'].strip('"'), ### Wierd stuff in api-response.
                ])

        ### print each entry of old_versions_2b_deleted, as a single-line json
        for entry in obj_list:
            print(json.dumps(entry, default=str))

        print(f"{len(obj_list)} objects (out of {len(s3_obj_ver_list)}) are `older_than` param set to {older_than}")

        ### -----------------
        ### Repeat all of the above .. this time for response_key = `DeleteMarkers` instead of `Versions`
        ### DeleteMarkers should NEVER be filtered by Date. Just wipe them out!
        s3obj_delmrkr_list :list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 's3',
            api_method_name = "list_object_versions",
            response_key = 'DeleteMarkers',   ### <----------- different from previous invocation above.
            json_output_filepath = self.json_output_filepath_delmrkr,
            additional_params={ "Bucket": bucket_name },
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        ### print each entry of old_versions_2b_deleted, as a single-line json
        for s3obj in s3obj_delmrkr_list:
            entry = [
                s3obj['Key'],
                s3obj['VersionId'],
                s3obj['LastModified'],
                s3obj['Size'],
                s3obj['StorageClass'],
                # "ETag": s3obj['ETag'].strip('"'), ### Wierd stuff in api-response.
            ]
            print(json.dumps(entry, default=str))

        print(f"{len(s3obj_delmrkr_list)} DEL-MARKER-objects are `older_than` param set to {older_than}")

        ### -----------------
        ### Now list all REGULR objects in S2, and delete them.
        s3obj_list :list = self.awsapi_invoker.invoke_aws_GenericAWSApi_for_complete_response(
            aws_client_type = 's3',
            api_method_name = "list_objects_v2",
            response_key = 'Contents',
            json_output_filepath = self.json_output_filepath_listobjv2,
            additional_params={
                "Bucket": bucket_name,
                "MaxKeys": 10000, ### By default, all s3-apis return up to 1,000 keys/items
            },
            cache_no_older_than = 1, ### Override the value for 'self.cache_no_older_than' .. as stacks frequently change every-day!
        )
        s3obj_list = sorted(
            s3obj_list,
            key = lambda x: x.get('LastModified', datetime.min)
        )
        if self.debug > 3 and s3obj_list:
            for entry in s3obj_list:
                print(json.dumps(entry, default=str))
        too_old_objs = []
        print(json.dumps(s3obj_list[0], indent=4))
        for s3obj in s3obj_list:
            last_modified = s3obj['LastModified']
            if isinstance(last_modified, str):
                last_modified = datetime.fromisoformat(last_modified.replace('Z', '+00:00'))
            if older_than == None or last_modified < cutoff_date:
                too_old_objs.append( [s3obj['Key'],
                                      # s3obj['VersionId'],   ### Not present in `list_objects_v2()` response
                                      s3obj['LastModified'],
                                      s3obj['Size'],
                                      s3obj['StorageClass']
                                    ])

        ### print each entry of old_versions_2b_deleted, as a single-line json
        for entry in too_old_objs:
            print(json.dumps(entry, default=str))

        print(f"{len(too_old_objs)} objects (out of {len(s3obj_list)}) are `older_than` param set to {older_than}")

#         # List all object versions
#         paginator = self.s3_client.get_paginator('list_object_versions')

#         deleted_count = 0
#         for page in paginator.paginate(Bucket=bucket_name):
#             # Handle non-delete-marker versions
#             if 'Versions' in page:
#                     old_versions_2b_deleted = [
#                         {'Key': obj['Key'], 'VersionId': obj['VersionId']}
#                         for obj in page['Versions']
#                         if obj['LastModified'] < cutoff_date
#                     ]

### ===========================================================================================================
### ...........................................................................................................
### ===========================================================================================================

if __name__ == "__main__":
    if len (sys.argv) < 5:
        print( f"Usage: python {sys.argv[0]} <AWS_PROFILE> <TIER> <CDK-Bucketname> <choose-obj-older-than>")
        print( f"Example: python3 {sys.argv[0]} DEVINT dev  cdk-hnb659fds-assets-127516845550-us-east-1  30 " )
        sys.exit(1)
    aws_profile = sys.argv[1]
    tier = sys.argv[2]
    bucket_name=sys.argv[3]
    older_than=int(sys.argv[4])

    # BUCKETNAME = 'fact-frontend-int-frontendcloudfrontloggingbucket4-qsnvimpoocce'
    # cleanup_s3_bucket( bucketname = sys.argv[1] )
    # delete_s3_bucket( bucketname = sys.argv[1] )
    # delete_s3_bucket( bucketname = BUCKETNAME )
    ListAllObjectsInsideBucket(
        appl_name = APP_NAME,
        bucket_name = bucket_name,
        older_than = older_than,
        aws_profile=aws_profile,
        tier=tier,
        debug = DEBUG,
    )
