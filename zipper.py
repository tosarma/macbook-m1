import sys
import zipfile
import os
from pathlib import Path

def create_zip_archive(zip_filename, source_dir):
    # Ensure the source directory exists
    source_path = Path(source_dir)
    if not source_path.exists():
        raise FileNotFoundError(f"Source directory {source_dir} does not exist")

    # Create zip file
    with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
        # Walk through the directory
        for root, _, files in os.walk(source_dir):
            for file in files:
                # Get the full file path
                file_path = os.path.join(root, file)
                # Calculate path relative to source_dir for the archive
                arcname = os.path.relpath(file_path, source_dir)
                # Add file to the zip archive
                zipf.write(file_path, arcname)

if __name__ == "__main__":
    # Define the zip filename and source directory
    # zip_filename = "8ae2b85a89c4326da6831c4dca7ca62d051a284be51414979d338c926d453c0f.zip"
    # source_dir = "/asset-output/python"
    zip_filename = sys.argv[1]
    source_dir = sys.argv[2]

    try:
        create_zip_archive(zip_filename, source_dir)
        print(f"Successfully created {zip_filename}")
    except Exception as e:
        print(f"Error creating zip file: {e}")
