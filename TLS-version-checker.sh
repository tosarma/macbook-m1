#!/usr/bin/env bash

nmap --script ssl-enum-ciphers -p 443 "$1"

### From https://superuser.com/a/224263
### test cipher suites of a website.
### It retrieves a list of supported cipher suites from OpenSSL and tries to connect using each one.
### If the handshake is successful, it prints YES.
### If the handshake isn't successful, it prints NO, followed by the OpenSSL error text.

# OpenSSL requires the port number.
# SERVER=$1
# DELAY=1
# ciphers=$(openssl ciphers 'ALL:eNULL' | sed -e 's/:/ /g')

# echo Obtaining cipher list from $(openssl version).

# for cipher in ${ciphers[@]}
# do
#   echo -n Testing $cipher...
#   result=$(echo -n | openssl s_client -cipher "$cipher" -connect $SERVER 2>&1)
#   if [[ "$result" =~ ":error:" ]] ; then
#     error=$(echo -n $result | cut -d':' -f6)
#     echo NO \($error\)
#   else
#     if [[ "$result" =~ "Cipher is ${cipher}" || "$result" =~ "Cipher    :" ]] ; then
#       echo YES
#     else
#       echo UNKNOWN RESPONSE
#       echo $result
#     fi
#   fi
#   sleep $DELAY
# done