#!/bin/bash -f

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
BIN_FOLDER="$( cd ${SCRIPT_FOLDER}/..; pwd )"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

if [ -z "${GITHUB_ENV+x}" ]; then
    printf "\n!! ERROR !! NEVER run ${SCRIPT_NAME} OUTSIDE of GitHub-Actions.\n"
    printf "\n!! ERROR !!      Env-Variable GITHUB_ENV is NOT defined!.\n"
    exit 9
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### -------------------- variables.env ------------------------
echo "BACKEND_URL=${BACKEND_URL}" >> ${GITHUB_ENV}
echo "NEO4J_PASSWORD=${NEO4J_PASSWORD}" >> ${GITHUB_ENV}
echo "NEO4J_URI=${NEO4J_URI}" >> ${GITHUB_ENV}
echo "NEO4J_URL=${NEO4J_URL}" >> ${GITHUB_ENV}
echo "AUTH_ENDPOINT=${AUTH_ENDPOINT}" >> ${GITHUB_ENV}
echo "REACT_APP_BACKEND_API=${REACT_APP_BACKEND_API}" >> ${GITHUB_ENV}
echo "REACT_APP_BACKEND_PUBLIC_API=${REACT_APP_BACKEND_PUBLIC_API}" >> ${GITHUB_ENV}
echo "REACT_APP_FILE_SERVICE_API=${REACT_APP_FILE_SERVICE_API}" >> ${GITHUB_ENV}
echo "REACT_APP_ABOUT_CONTENT_URL=${REACT_APP_ABOUT_CONTENT_URL}" >> ${GITHUB_ENV}
echo "REACT_APP_BE_VERSION=${REACT_APP_BE_VERSION}" >> ${GITHUB_ENV}
echo "REACT_APP_FE_VERSION=${REACT_APP_FE_VERSION}" >> ${GITHUB_ENV}
echo "REACT_APP_AUTH_SERVICE_API=${REACT_APP_AUTH_SERVICE_API}" >> ${GITHUB_ENV}
echo "REACT_APP_USER_SERVICE_API=${REACT_APP_USER_SERVICE_API}" >> ${GITHUB_ENV}

### -------------------- variables-fixed.env ------------------------
### Any value that is PERMANENTLY a fixed-value (INDEPENDENT of environment), should be moved to `variables-fixed.env` file.

echo "BACKEND_BRANCH=${BACKEND_BRANCH}" >> ${GITHUB_ENV}
echo "FRONTEND_BRANCH=${FRONTEND_BRANCH}" >> ${GITHUB_ENV}
echo "FILES_BRANCH=${FILES_BRANCH}" >> ${GITHUB_ENV}
echo "BUILD_MODE=${BUILD_MODE}" >> ${GITHUB_ENV}

echo "FRONTEND_SOURCE_FOLDER=${FRONTEND_SOURCE_FOLDER}" >> ${GITHUB_ENV}
echo "BACKEND_SOURCE_FOLDER=${BACKEND_SOURCE_FOLDER}" >> ${GITHUB_ENV}

echo "BACKEND_HOST=${BACKEND_HOST}" >> ${GITHUB_ENV}
echo "FILES_HOST=${FILES_HOST}" >> ${GITHUB_ENV}
echo "NEO4J_USER=${NEO4J_USER}" >> ${GITHUB_ENV}
echo "NEO4J_PASS=${NEO4J_PASS}" >> ${GITHUB_ENV}
echo "NEO4J_HOST=${NEO4J_HOST}" >> ${GITHUB_ENV}
echo "ES_HOST=${ES_HOST}" >> ${GITHUB_ENV}
echo "ES_PORT=${ES_PORT}" >> ${GITHUB_ENV}

echo "NODE_ENV=${NODE_ENV}" >> ${GITHUB_ENV}

### -------------------- variables-expression.env ------------------------
### Only those values that are PERMANENTLY FIXED values -- across __ALL__ environments!!
###     If Not, then place such values in "variables.env" file instead.

echo "PROJECT=${PROJECT}" >> ${GITHUB_ENV}
echo "FILES_VERSION=${FILES_VERSION}" >> ${GITHUB_ENV}
echo "URL_SRC=${URL_SRC}" >> ${GITHUB_ENV}
echo "AUTH_ENABLED=${AUTH_ENABLED}" >> ${GITHUB_ENV}

echo "BENTO_API_VERSION=${BENTO_API_VERSION}" >> ${GITHUB_ENV}
echo "BENTO_DATA_MODEL=${BENTO_DATA_MODEL}" >> ${GITHUB_ENV}

echo "REACT_APP_AUTH=${REACT_APP_AUTH}" >> ${GITHUB_ENV}
echo "PUBLIC_ACCESS=${PUBLIC_ACCESS}" >> ${GITHUB_ENV}

echo "GRAPHQL_SCHEMA=${GRAPHQL_SCHEMA}" >> ${GITHUB_ENV}
echo "GRAPHQL_ES_SCHEMA=${GRAPHQL_ES_SCHEMA}" >> ${GITHUB_ENV}
echo "GRAPHQL_PUBLIC_SCHEMA=${GRAPHQL_PUBLIC_SCHEMA}" >> ${GITHUB_ENV}
echo "GRAPHQL_PUBLIC_ES_SCHEMA=${GRAPHQL_PUBLIC_ES_SCHEMA}" >> ${GITHUB_ENV}

echo "NODE_LEVEL_ACCESS=${NODE_LEVEL_ACCESS}" >> ${GITHUB_ENV}
echo "NODE_LABEL=${NODE_LABEL}" >> ${GITHUB_ENV}

echo "REDIS_ENABLE=${REDIS_ENABLE}" >> ${GITHUB_ENV}
echo "REDIS_USE_CLUSTER=${REDIS_USE_CLUSTER}" >> ${GITHUB_ENV}
echo "REDIS_HOST=${REDIS_HOST}" >> ${GITHUB_ENV}
echo "REDIS_FILTER_ENABLE=${REDIS_FILTER_ENABLE}" >> ${GITHUB_ENV}

echo "ES_JAVA_OPTS=${ES_JAVA_OPTS}" >> ${GITHUB_ENV}
echo "network_bind_host=${network_bind_host}" >> ${GITHUB_ENV}
echo "network_host=${network_host}" >> ${GITHUB_ENV}
echo "discovery_type=${discovery_type}" >> ${GITHUB_ENV}
echo "plugins_security_disabled=${plugins_security_disabled}" >> ${GITHUB_ENV}

### EoScript
