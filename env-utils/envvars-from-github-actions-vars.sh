#!/bin/bash -f

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
BIN_FOLDER="$( cd ${SCRIPT_FOLDER}/..; pwd )"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

if [ -z "${GITHUB_ENV+x}" ]; then
    printf "\n!! ERROR !! NEVER run ${SCRIPT_NAME} OUTSIDE of GitHub-Actions.\n"
    printf "\n!! ERROR !!      Env-Variable GITHUB_ENV is NOT defined!.\n"
    exit 9
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${BIN_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${BIN_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

TMPROOTDIR="/tmp"
TMPDIR="${TMPROOTDIR}/DevOps/Github/${SCRIPT_NAME}"
TMPFILE11="${TMPDIR}/tmp1.txt"

mkdir -p ${TMPROOTDIR}
mkdir -p ${TMPDIR}
rm -rf "${TMPFILE11}"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

printf "%.0s-" {1..80}
printf "\nConverting ALL GitHub-Action-VARIABLES into ENV-VARS for use by GitHub-Action-WORKFLOWS\n"

echo gh variable list --env "${ENV}" --repo "${ORG}/${REPONAME}"
VARIABLES=( $( gh variable list --env "${ENV}" --repo "${ORG}/${REPONAME}" | tail +1 | cut -f1 ) )

gh variable list --env "${ENV}" --repo "${ORG}/${REPONAME}" | tail +1 | cut -f1,2 | sed -e 's/\t/="/' | sed -E 's/[[:space:]]*$/"/' > "${TMPFILE11}"
ls -la "${TMPFILE11}"
. "${TMPFILE11}"

for VARNAME in "${VARIABLES[@]}"; do
    eval echo "exporting ${VARNAME}=\${${VARNAME}} .."
    eval echo "${VARNAME}=\${${VARNAME}}" >> ${GITHUB_ENV}
done

printf "%.0s-" {1..80}; printf "\n\n"

### EoScript
