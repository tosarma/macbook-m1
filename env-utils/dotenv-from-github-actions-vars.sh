#!/bin/bash -f

if [ $# -ne 1 ]; then
    printf "\n!! ERROR !! Just one CLI-arg required (found $#)\n"
    printf "Usage:   $0  <Path2 .env-file>\n\n"
    exit 1
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
BIN_FOLDER="$( cd ${SCRIPT_FOLDER}/..; pwd )"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

DOTENV_FILEPATH=${CWD}/$1
echo "DOTENV_FILEPATH=[${DOTENV_FILEPATH}]"
ls -lad "${DOTENV_FILEPATH}"

if [ ! -w "${DOTENV_FILEPATH}" ]; then
    printf "\n!! ERROR !! Non-existent/invalid FILE-PATH $1;\n"
    printf "            Note: It should relative-path to current-working-dir\n\n"
    exit 1
fi

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 1
. "${BIN_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 1
. "${BIN_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

TMPROOTDIR="/tmp"
TMPDIR="${TMPROOTDIR}/DevOps/Github/${SCRIPT_NAME}"
TMPFILE11="${TMPDIR}/tmp1.txt"

mkdir -p ${TMPROOTDIR}
mkdir -p ${TMPDIR}
rm -rf "${TMPFILE11}"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

printf "%.0s-" {1..80}
printf "\nConverting ALL GitHub-Action-VARIABLES into ENV-VARS for use by GitHub-Action-WORKFLOWS\n"

echo gh variable list --env "${ENV}" --repo "${ORG}/${REPONAME}"
VARIABLES=( $( gh variable list --env "${ENV}" --repo "${ORG}/${REPONAME}" | tail +1 | cut -f1 ) )

gh variable list --env "${ENV}" --repo "${ORG}/${REPONAME}" | tail +1 | cut -f1,2 | sed -e 's/\t/="/' | sed -E 's/[[:space:]]*$/"/' > "${TMPFILE11}"
ls -la "${TMPFILE11}"
. "${TMPFILE11}"

echo '' > "${DOTENV_FILEPATH}"                  ### Empty out the file (if it exists)

for VARNAME in "${VARIABLES[@]}"; do
    eval echo "exporting ${VARNAME}=\${${VARNAME}} .."
    eval echo "${VARNAME}=\${${VARNAME}}" >> "${DOTENV_FILEPATH}"
done

printf "%.0s-" {1..80}; printf "\n\n"

### EoScript
