#!/bin/bash -f

SCRIPT_FOLDER="$(dirname ${BASH_SOURCE[0]})"
SCRIPT_NAME="$(basename ${BASH_SOURCE[0]})"
CWD="$(pwd)"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

### Load settings / configuration - specifc to each Project/Github Org.

echo "Loading all PROJECT-Specific settings"; sleep 2
. "${SCRIPT_FOLDER}/settings.sh"

echo "Loading DERIVED PROJECT-Specific settings"; sleep 2
. "${SCRIPT_FOLDER}/settings-derived.sh"

###---------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###---------------------------------------------------------------

LEN=16  ### Random-password of this many characters LONG.

SECRETS=(
        "NEO4J_PASSWORD"
    )

### ------------------------------------------------------------------------

for SECRET in ${SECRETS[@]}; do

    RANDPASS="******"  ### To prevent it from mistakenly being printed, when MOVING/REFACTORING lines.
    echo -n \
    gh secret set "${SECRET}" --env "${ENV}" --app actions --body "${RANDPASS}" .. ..
    sleep 2

    ### ------------------------------------
    RANDPASS="$( openssl rand -base64 64 | tr -d '\n\r' | tr -d '[:punct:]' | cut -c 1-${LEN} )"

    gh secret set "${SECRET}" --env "${ENV}" --app actions --body "${RANDPASS}"
    ### !!! Warning !!! This secret is to be used ACROSS by multiple users + DevSecOps toolchain.
    ###                  So, do _NOT_ add "--user" cli-arg to the above cmd.
    ###                  For "Repo-specific" secrets, add "--repo" cli-arg.. .. but, do run such cmds manually
    ###                  For "USER-specific" secrets, add "--user" cli-arg.. .. but, do run such cmds manually

    ### ------------------------------------
    RANDPASS="******"  ### To prevent it from mistakenly being printed, when MOVING/REFACTORING lines.

done

### EoScript
