### The working version of the shell-script `macbook-m1/list-lambdas-by-tag.sh`
### List of all Lambdas that belong a specific ENV provided via CLI-arg
### This works great.

import sys
import boto3
import os
import pathlib
import json
import time

if len(sys.argv) != 3:
    print("Usage: python script.py <AWS_PROFILE> <ENV>")
    sys.exit(1)

aws_profile = sys.argv[1]
env         = sys.argv[2]

### ----------------------------------------------------------------------
### Mnually configurable constants.

appl_name = 'FACT'
TMPDIR    = '/tmp'
no_older_than = 7     ### maximum _ days old

### ----------------------------------------------------------------------
### AWS API configuration

session = boto3.Session(profile_name=aws_profile)
# session = boto3.Session(profile_name=aws_profile, region_name=aws_region)
client = session.client('sts')
account_id = client.get_caller_identity()["Account"]
default_region = session.region_name
print(f"\nAWS Account ID: {account_id}")
print(f"Default AWS Region: {default_region}\n")

### ----------------------------------------------------------------------
### Section: Derived variables and constants

json_output_filepath = f"{TMPDIR}/{aws_profile}-{session.region_name}-all-rsrcs-lambda.json"
json_output_filepath = pathlib.Path(json_output_filepath) ### convert a string into a Path object.

### ----------------------------------------------------------------------
### Logic to --Cache-- the output of AWS-SDK API-calls (into temporary files)

if not json_output_filepath.exists():
    print(f"Cache is missing!! a.k.a. File '{json_output_filepath}' is missing!!")
    re_run_aws_sdk_call = True
else:
    # Check if the file was last modified over a week ago
    one_week_in_seconds = no_older_than * 24 * 60 * 60  # 7 days
    file_modified_time = json_output_filepath.stat().st_mtime
    current_time = time.time()

    if current_time - file_modified_time > one_week_in_seconds:
        re_run_aws_sdk_call = True
        print(f"The CACHE/file '{json_output_filepath}' is too old by at least {no_older_than} days !!! ")
    else:
        re_run_aws_sdk_call = False
        print(f"The CACHE/file '{json_output_filepath}' is still fresh enough.")

### ----------------------------------------------------------------------
### As necessary invoke AWS SDK API calls.

if re_run_aws_sdk_call:
    print("Invoking the massive AWS-SDK API for resourceGroupsTaggingApi .. ..\n")
    client = session.client('resourcegroupstaggingapi')

    response = client.get_resources(
        TagFilters=[
            { 'Key': 'branch', 'Values': [env] },
            { 'Key': 'application', 'Values': [appl_name] },
        ],
        ResourceTypeFilters=['lambda']
    )

    # Write the response as a json to a file named f"{TMPDIR}/all-rsrcs-lambda.json"
    with open(str(json_output_filepath), "w") as f:
        json.dump(response, f, indent=4)
else:
    # print(f"File {json_output_filepath} is present.\nSo .. using this cached AWS-SDK response.. ..\n")
    with open(str(json_output_filepath)) as f:
        response = json.load(f)

### ----------------------------------------------------------------------
### produce output for human consumption

for resource in response['ResourceTagMappingList']:
    # print(f"Lambda function: {resource['ResourceARN']}")
    print(resource['ResourceARN'].split(':')[-1])

### EoScript
